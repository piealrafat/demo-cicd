/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces;

import org.apache.commons.lang3.StringUtils;

import org.json.JSONObject;

/**
 *
 * @author glgay
 */
public class Widget {
    
    private String  displayName = null;
    private String  url         = null;
    private boolean builtin     = false;
    
    public Widget(JSONObject json, boolean builtin) {
        this.builtin = builtin;
        if (json != null) {
            displayName = json.optString("display_name");
            url         = json.optString("url");
        }
    }
    
    public Widget(JSONObject json) {
        this(json, false);
    }
    
    public boolean isValid() {
        return !StringUtils.isBlank(displayName) && !StringUtils.isBlank(url);
    }
    
    public String getDisplayName() { return displayName; }
    public String getUrl()         { return url; }
    public boolean isBuiltin()     { return builtin; }
    
}
