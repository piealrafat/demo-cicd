/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces;

import java.util.Properties;

import javax.servlet.ServletContext;

import java.nio.charset.StandardCharsets;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.session.Session;

import org.json.JSONObject;
import org.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;

import com.nndata.ces.api.RestAPI;


/**
 *
 * @author glgay
 */
public class UIConfig {
    
    public static final String DEFAULT_HOME      = "/composable";
    public static final String PROPERTIES_PATH   = "/ces.properties";
    public static final String PROPERTIES_KEY    = "com.nndata.ces.properties";
    public static final String USER_NAME         = "com.nndata.ces.user.name";
    public static final String ACTIVE_WIDGETS    = "com.nndata.ces.active_widgets";
    public static final String MAP_STATE         = "com.nndata.ces.map_state";
		public static final String SELECTED_MISSION  = "com.nndata.ces.selected_mission";
    public static final String UI_HOME           = "ui.home.url";
    public static final String USER_TIME_ZONE    = "com.nndata.ces.user.time_zone";
    public static final String TIMEZONE_COOKIE   = "user_time_zone";
    public static final String MODULE_DIR        = "WEB-INF/modules";
    
    private static final Logger log = LoggerFactory.getLogger(UIConfig.class);
    
    private final Properties cesProps;
    private final RestAPI    adminApi;
		private final JSONArray missions;
    
    public UIConfig(ServletContext ctx) {
        cesProps = AppListener.loadProperties();
        adminApi = (ctx == null) ? null : (RestAPI)ctx.getAttribute(AppListener.NV_ADMIN_KEY);
				missions = (ctx == null) ? null : getMissions(ctx);
    }
    
    public String getUiHome() {
        return cesProps.getProperty(UI_HOME, DEFAULT_HOME);
    }
    
    public String getProperty(String key) {
        return cesProps.getProperty(key);
    }
    
    public String getProperty(String key, String defaultValue) {
        return cesProps.getProperty(key, defaultValue);
    }
    
    public String getUserName() {
        String result = "";
        try {
            Subject sbj  = SecurityUtils.getSubject();
            Session sess = sbj.getSession();
            Object  o    = sess.getAttribute(USER_NAME);
            
            if (o != null) {
                result = o.toString();
            }
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public String getTimeZone() {
        String result = "";
        try {
            Subject sbj  = SecurityUtils.getSubject();
            Session sess = sbj.getSession();
            Object  o    = sess.getAttribute(USER_TIME_ZONE);
            
            if (o != null) {
                result = o.toString();
            }
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public JSONObject getUserPrefs() {
        
        JSONObject prefs = null;
        
        try {
            
            String username = getUserName();
            prefs = adminApi.getUserPrefs(username);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        if (prefs == null) {
            prefs = new JSONObject();
        }
        
        return prefs;
    }
    
    public boolean saveUserPrefs(JSONObject prefs) {
        
        boolean worked = false;
        
        try {
            
            String username = getUserName();
            worked = adminApi.saveUserPrefs(username, prefs);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        return worked;
    }
    
    public String getStartPage() {
        String page = null;
        try {
            
            JSONObject prefs = getUserPrefs();
            String tmpPage = prefs.optString("start_page", "");
            
            // Right now, allow only the ones we know about...
            switch (tmpPage) {
                case "map": {
                    page = tmpPage;
                    break;
                }
                case "grid": {
                    page = tmpPage;
                    break;
                }
                case "controller": {
                    page = tmpPage;
                    break;
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return page;
    }
    
    public Widgets getActiveWidgets() {
        Widgets active = new Widgets();
        try {
            
            Subject sbj  = SecurityUtils.getSubject();
            Session sess = sbj.getSession();
            
            synchronized (sess) {
                String arrStr = (String)sess.getAttribute(ACTIVE_WIDGETS);
                if (arrStr == null) {
                    JSONObject prefs = getUserPrefs();
                    JSONArray  saved = prefs.optJSONArray("widgets");
            
                    if (saved != null) {
                        // Wrap them in the same format as the controller...
                        JSONObject liveWidgets = new JSONObject();
                        liveWidgets.put("group",          "active");
                        liveWidgets.put("visualizations", saved);
                        JSONArray arr = new JSONArray();
                        arr.put(liveWidgets);
                        active.addAll(arr, false);
                        
                        sess.setAttribute(ACTIVE_WIDGETS, arr.toString());
                    }
                } else {
                    JSONArray arr = new JSONArray(arrStr);
                    active.addAll(arr, false);
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return active;
    }
    
    public void setActiveWidgets(JSONArray widgets) {
        try {
            
            Subject sbj  = SecurityUtils.getSubject();
            Session sess = sbj.getSession();
            
            synchronized (sess) {
                JSONObject liveWidgets = new JSONObject();
                liveWidgets.put("group",          "active");
                liveWidgets.put("visualizations", widgets);
                JSONArray arr = new JSONArray();
                arr.put(liveWidgets);
                sess.setAttribute(ACTIVE_WIDGETS, arr.toString());
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
    }
    
    public void setMapState(JSONArray center, double zoom) {
        try {
            
            Subject sbj  = SecurityUtils.getSubject();
            Session sess = sbj.getSession();
            
            synchronized (sess) {
                
                JSONObject mapState = new JSONObject();
                mapState.put("center", center);
                mapState.put("zoom", zoom);
                sess.setAttribute(MAP_STATE, mapState.toString());
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
    }
    
    public JSONObject getMapState() {
        
        JSONObject result = new JSONObject("{}");
        
        try {
            
            Subject sbj  = SecurityUtils.getSubject();
            Session sess = sbj.getSession();
            
            synchronized (sess) {
                String sState = (String)sess.getAttribute(MAP_STATE);
                if (StringUtils.isBlank(sState)) {
                   JSONObject prefs = getUserPrefs();
                   JSONObject  saved = prefs.optJSONObject("map_prefs");
                   if (saved != null){
                       result = saved;
                   }
                } else {
                    result = new JSONObject(sState);
                }
                
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        return result;
    }

		public JSONArray getMissions(ServletContext ctx) {
			String missionDetailsPath = "/WEB-INF/mission-control.json";
			try {
				String missPath = ctx.getRealPath(missionDetailsPath);
            
				try (FileInputStream fis = new FileInputStream(missPath)) {
						JSONArray missions;
						String jsText = IOUtils.toString(fis, StandardCharsets.UTF_8);
						missions = new JSONArray(jsText);
						return missions;
				}
			} catch (Throwable t) {
				log.error(t.getMessage(), t);
			}
			return new JSONArray();
		}

		public HashMap<Integer, String> getMissionTitles() {
			HashMap<Integer, String> missionTitles = new HashMap<Integer, String>();
			if (missions != null) {
				for(int i = 0; i < missions.length(); i++) {
					missionTitles.put(i, missions.getJSONObject(i).getString("mission"));
				}
			}
			return missionTitles;
		}

		public void setSelectedMission(Integer missionId) {
			try {
							
					Subject sbj  = SecurityUtils.getSubject();
					Session sess = sbj.getSession();
							
					JSONObject selectedMission = new JSONObject();
					selectedMission.put("missionId", missionId);
					sess.setAttribute(SELECTED_MISSION, selectedMission.toString());
					
			} catch (Throwable t) {
					log.error(t.getMessage(), t);
			}
		}

		public Integer getSelectedMissionId() {
			JSONObject result = new JSONObject("{}");
			Integer missionId = -1;
        
			try {
					
					Subject sbj  = SecurityUtils.getSubject();
					Session sess = sbj.getSession();
					
					String sMission = (String)sess.getAttribute(SELECTED_MISSION);
					if (!StringUtils.isBlank(sMission)) {
						result = new JSONObject(sMission);
						missionId = result.getInt("missionId");
					}
					
					
			} catch (Throwable t) {
					log.error(t.getMessage(), t);
			}

			return missionId;
		}

		public JSONObject getSelectedMission() {
			JSONObject result = new JSONObject("{}");
        
			try {
					
					Subject sbj  = SecurityUtils.getSubject();
					Session sess = sbj.getSession();
					
					synchronized (sess) {
							String sMission = (String)sess.getAttribute(SELECTED_MISSION);
							if (StringUtils.isBlank(sMission)) {
								JSONObject prefs = getUserPrefs();
								JSONObject  saved = prefs.optJSONObject("mission_prefs");
								if (saved != null){
										result = saved;
								}
						 } else {
								 result = new JSONObject(sMission);
						 }
					}
					
			} catch (Throwable t) {
					log.error(t.getMessage(), t);
			}

			return result;
		}

		private String getMissionDetailPath() {
				String selectedMissionPath = "";
				Integer missionId = getSelectedMissionId();
				try {
						if (missionId > -1) {
							selectedMissionPath = missions.getJSONObject(missionId).getString("mission_description");
						} 
					} catch (Throwable t) {
						log.error(t.getMessage(), t);
				}

				return selectedMissionPath;
		}

		public String getMissionMarkdownHTML(ServletContext ctx) {
			String markdownHTML = "";
			String mdp = getMissionDetailPath();
			
			String missionDetailsPath = "/WEB-INF/" + mdp;

			try {
				String missPath = ctx.getRealPath(missionDetailsPath);
            
				try (FileInputStream fis = new FileInputStream(missPath)) {
						String markdown = IOUtils.toString(fis, StandardCharsets.UTF_8);

						Parser parser = Parser.builder().build();
						Node document = parser.parse(markdown);
						HtmlRenderer renderer = HtmlRenderer.builder().build();
						markdownHTML = renderer.render(document);

						return markdownHTML;
				}
			} catch (Throwable t) {
				log.error(t.getMessage(), t);
			}

			return markdownHTML;
		}

		private String getMissionMapSourcesString() {
			Integer missionId = getSelectedMissionId();
			JSONArray missionMapURLs = new JSONArray();
			String query = "";
			try {
					if (missionId > -1) {
						missionMapURLs = missions.getJSONObject(missionId).getJSONObject("urls").getJSONArray("maps");
						for(int i = 0; i < missionMapURLs.length(); i++) {
							if (i > 0) {
								query += ",";
							}
							JSONObject missionObj = missionMapURLs.getJSONObject(i);
							query += missionObj.getString(missionObj.keys().next());
						}
					} 
				} catch (Throwable t) {
					log.error(t.getMessage(), t);
			}

			return query;
		}

		private JSONObject getMapViewDetails() {
			JSONObject mapDetails = new JSONObject("{}");
			Integer missionId = getSelectedMissionId();

			if (missionId == -1) {
				return mapDetails;
			}

			try {
				mapDetails = missions.getJSONObject(missionId).getJSONObject("map_view");
			} catch (Throwable t) {
				log.error(t.getMessage(), t);
			}
			return mapDetails;
		}

		public String getMissionMapQueryString() {
			JSONObject mapDetails = getMapViewDetails();
			log.info("mapDetails: " + mapDetails.toString());
			String query = "sources=" + getMissionMapSourcesString() +
			 "&lat=" + mapDetails.optDouble("lat", 1) +
			 "&lon=" + mapDetails.optDouble("lon", 1) +
			 "&zoom=" + mapDetails.optDouble("zoom", 0);
			return query;
		};

		public String getMissionScenario() {
			String scenario = "";
			Integer missionId = getSelectedMissionId();

			if (missionId == -1) {
				return scenario;
			}

			try {
				JSONObject urls = missions.getJSONObject(missionId).optJSONObject("urls");
				if (urls != null) {
					scenario = urls.optString("fog_scenario");
					scenario += "/" + missions.getJSONObject(missionId).optString("launch_script");
				}
			} catch (Throwable t) {
				log.error(t.getMessage(), t);
			}
			return scenario;
		}
}
