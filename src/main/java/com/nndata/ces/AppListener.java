/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces;

import java.io.InputStream;
import java.io.FileInputStream;

import java.nio.charset.StandardCharsets;

import java.util.Properties;
import java.util.Base64;

import javax.servlet.ServletContextListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContext;

import org.apache.commons.lang3.StringUtils;

import org.apache.shiro.config.Ini;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nndata.ces.api.RestAPI;

/**
 *
 * @author glgay
 */
public class AppListener implements ServletContextListener {
    
    private static final Logger log = LoggerFactory.getLogger(AppListener.class);
    
    public static final String NV_ADMIN_KEY = "com.nndata.ces.nnvision.admin_rest";
    public static final String NV_URL_KEY   = "com.nndata.ces.nnvision.rest_url";
    
    private Properties cesProps;
    private RestAPI    adminApi;
    private String     nvUrl;
    
    public static Properties loadProperties() {
        Properties props = new Properties();
        try (InputStream is = AppListener.class.getResourceAsStream(UIConfig.PROPERTIES_PATH)) {
            props.load(is);
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return props;
    }
    
    private void loadAdminRest(ServletContext ctx) {
        try {
            
            // We get the CES admin user/pass from shiro.ini
            // to avoid having to poke it again in the properties file
            String iniPath  = ctx.getRealPath("/WEB-INF/shiro.ini");
            Ini    shiroIni = new Ini();
            
            try (FileInputStream fis = new FileInputStream(iniPath)) {
                shiroIni.load(fis);
            }
            
            Ini.Section mainSection = shiroIni.getSection("main");
            String      baseUrl     = mainSection.getOrDefault("nnvisionRealm.url",      "");
            String      username    = mainSection.getOrDefault("nnvisionRealm.username", "");
            String      password    = mainSection.getOrDefault("nnvisionRealm.password", "");
            
            if (!StringUtils.isBlank(baseUrl) &&!StringUtils.isBlank(username) && !StringUtils.isBlank(password)) {
                if (password.startsWith("{") && password.endsWith("}")) {
                    password = password.substring(1, password.length() - 1);
                    byte[] bytes = Base64.getDecoder().decode(password);
                    password = new String(bytes, StandardCharsets.UTF_8);
                }
                
                nvUrl    = baseUrl;
                adminApi = new RestAPI(nvUrl, username, password);
                
                ctx.setAttribute(NV_URL_KEY,   nvUrl);
                ctx.setAttribute(NV_ADMIN_KEY, adminApi);
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
    }
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        
        log.info("Composable UI application initializing");
        
        cesProps = loadProperties();
        
        String         uiHome = cesProps.getProperty(UIConfig.UI_HOME, UIConfig.DEFAULT_HOME);
        ServletContext ctx    = sce.getServletContext();
        
        loadAdminRest(ctx);
        
        ctx.setAttribute(UIConfig.PROPERTIES_KEY, cesProps);
        ctx.setAttribute(UIConfig.UI_HOME,        uiHome);
    }
    
    @Override
    public void contextDestroyed(ServletContextEvent sce)  {
        log.info("Composable UI application shutting down");
    }
    
}
