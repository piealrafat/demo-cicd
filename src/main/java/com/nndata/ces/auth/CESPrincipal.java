/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.auth;

import java.util.Base64;

import java.nio.charset.StandardCharsets;

import java.security.Principal;

/**
 *
 * @author glgay
 */
public class CESPrincipal implements Principal {
    
    // Unfortunately, if a CES user wants to
    // query NNVision, he has to re-authenticate
    // periodically.  That means we have to
    // keep his password.  We'll store it in
    // Base64 so at least it's not plain text
    // in long-term memory
    
    private String username = null;
    private byte[] passwd   = null;
    
    // It's char[] here because that's what Shiro provides
    public CESPrincipal(String user, char[] pass) {
        this.username = user;
        try {
            
            String passwd = new String(pass);
            byte[] bytes = passwd.getBytes(StandardCharsets.UTF_8);
            
            this.passwd = Base64.getEncoder().encode(bytes);
            
        } catch (Throwable t) {
        }
    }
    
    @Override
    public String getName() {
        return username;
    }
    
    @Override
    public String toString() {
        return username;
    }
    
    @Override
    public int hashCode() {
        return username.hashCode();
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof CESPrincipal)) {
            return false;
        }
        CESPrincipal p = (CESPrincipal)o;
        return p.username.equals(username);
    }
    
    public String getPassword() {
        String pwd = null;
        try {
            
            byte[] bytes = Base64.getDecoder().decode(passwd);
            
            pwd = new String(bytes, StandardCharsets.UTF_8);
            
        } catch (Throwable t) {
        }
        return pwd;
    }
    
}
