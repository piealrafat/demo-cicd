/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.auth;

import java.io.IOException;

import java.util.regex.Pattern;

import java.security.Principal;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;

import org.apache.commons.lang3.StringUtils;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.session.Session;
import org.apache.shiro.web.util.WebUtils;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nndata.ces.UIConfig;

/**
 *
 * @author glgay
 */
public class UserMappingFilter extends AuthorizationFilter {
    
    private final Logger log = LoggerFactory.getLogger(getClass());
    
    private String charsToRemove = "";
    
    public String getCharsToRemove() {
        return charsToRemove;
    }
    
    public void setCharsToRemove(String charsToRemove) {
        if (StringUtils.isBlank(charsToRemove)) {
            this.charsToRemove = "";
        } else {
            this.charsToRemove = "[" + Pattern.quote(charsToRemove) + "]";
        }
    }
    
    private void checkTimeZone(HttpServletRequest request, HttpServletResponse response, Session session) {
        try {
            
            String tz = request.getParameter("local_timezone");
            if (!StringUtils.isBlank(tz)) {
                Cookie tzCookie = new Cookie(UIConfig.TIMEZONE_COOKIE, tz);
                tzCookie.setPath("/");
                response.addCookie(tzCookie);
                session.setAttribute(UIConfig.USER_TIME_ZONE, tz);
            } else {
                Cookie[] cookies = request.getCookies();
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals(UIConfig.TIMEZONE_COOKIE)) {
                        tz = cookie.getValue();
                        if (!StringUtils.isBlank(tz)) {
                            session.setAttribute(UIConfig.USER_TIME_ZONE, tz);
                        }
                        break;
                    }
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
    }
    
    @Override
    public boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws IOException {
        try {
            
            Subject             sbj   = getSubject(request, response);
            Session             sess  = sbj.getSession();
            HttpServletRequest  rq    = WebUtils.toHttp(request);
            HttpServletResponse rp    = WebUtils.toHttp(response);
            
            if (!sbj.isAuthenticated()) {
                log.error("UserMapppingFilter called for non-authenticated user");
                sess.removeAttribute(UIConfig.USER_NAME);
                sess.removeAttribute(UIConfig.USER_TIME_ZONE);
                return false;
            }
            
            Object o = sess.getAttribute(UIConfig.USER_NAME);
            if ((o != null) && (o instanceof String)) {
                // If the attribute exists, the session has already been set up
                return true;
            }
            
            String userName;
            
            o = sbj.getPrincipal();
            if (o instanceof String) {
                userName = o.toString();
            } else if (o instanceof Principal) {
                Principal p = (Principal)o;
                userName = p.getName();
                
                log.info("Processing pricipal object (" + o.getClass().getCanonicalName() + "): " + o.toString());
                log.info("Principal name: " + userName);
                
                if (!StringUtils.isBlank(charsToRemove)) {
                    userName = userName.replaceAll(charsToRemove, "");
                    log.debug("Using cleaned name: " + userName);
                }
                
            } else {
                log.error("Unknown principal of type " + o.getClass().getCanonicalName() + ": " + o.toString());
                return false;
            }
            
            sess.setAttribute(UIConfig.USER_NAME, userName);
            checkTimeZone(rq, rp, sess);
            
            return true;
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return false;
    }
    
}
