/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.auth;

import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Base64;

import java.nio.charset.StandardCharsets;

import org.apache.commons.lang3.StringUtils;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;

import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;

import org.apache.shiro.authc.credential.CredentialsMatcher;

import org.apache.shiro.realm.AuthorizingRealm;

import org.apache.shiro.subject.PrincipalCollection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nndata.ces.api.RestAPI;

/**
 *
 * @author glgay
 */
public class NNVisionRealm extends AuthorizingRealm implements CredentialsMatcher  {

    private static final Logger log = LoggerFactory.getLogger(NNVisionRealm.class);
    
    private RestAPI restApi  = null;
    private String  url      = null;
    private String  user     = null;
    private String  pwd      = null;
    
    private void checkApi() {
        if (!StringUtils.isBlank(url) && !StringUtils.isBlank(user) && !StringUtils.isBlank(pwd)) {
            restApi = new RestAPI(url, user, pwd);
        }
    }
    
    public String getUrl() {
        return url;
    }
    
    public void setUrl(String url) {
        
        restApi = null;
        
        if (StringUtils.isBlank(url)) {
            
            this.url = null;
            return;
        }
        if (!url.endsWith("/")) {
            url += "/";
        }
        this.url = url;
        
        checkApi();
    }
    
    public String getUsername() {
        return user;
    }
    
    public void setUsername(String user) {
        restApi = null;
        this.user = user;
        checkApi();
    }
    
    public String getPassword() {
        return pwd;
    }
    
    public void setPassword(String passwd) {
        restApi = null;
        if (StringUtils.isBlank(passwd)) {
            this.pwd = null;
            return;
        }
        if (passwd.startsWith("{") && passwd.endsWith("}")) {
            String b64 = passwd.substring(1, passwd.length() - 1);
            byte[] bytes = Base64.getDecoder().decode(b64);
            this.pwd = new String(bytes, StandardCharsets.UTF_8);
        } else {
            this.pwd = passwd;
        }
        checkApi();
    }
    
    @Override
    public boolean doCredentialsMatch​(AuthenticationToken token, AuthenticationInfo info) {
        
        if (token == null) {
            log.error("Null token received");
            return false;
        }
        if (!(token instanceof UsernamePasswordToken)) {
            log.error("Unsupported token type - " + token.getClass().getCanonicalName());
            return false;
        }
        
        if (StringUtils.isBlank(url)) {
            log.error("REST Server URL not defined");
            return false;
        }
        
        // We just try to request a REST token
        // If we get one, this must be a valid user
        UsernamePasswordToken upToken = (UsernamePasswordToken)token;
        String                passwd  = new String(upToken.getPassword());
        
        RestAPI api       = new RestAPI(url, upToken.getUsername(), passwd);
        String  restToken = api.requestToken();
        
        if (StringUtils.isBlank(restToken)) {
            return false;
        }
        
        return true;
    }
    
    @Override
    public CredentialsMatcher getCredentialsMatcher() {
        return this;
    }
    
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        if (token == null) {
            throw new AuthenticationException("Null authentication token");
        }
        if (!(token instanceof UsernamePasswordToken)) {
            throw new AuthenticationException("Unsupported token class " + token.getClass().getCanonicalName());
        }
        
        // Doing this just in case...
        setCredentialsMatcher(this);
        
        UsernamePasswordToken upToken  = (UsernamePasswordToken)token;
        String                username = upToken.getUsername().trim().toLowerCase();
        CESPrincipal          cesUser  = new CESPrincipal(username, upToken.getPassword());
        
        return new SimpleAuthenticationInfo(cesUser, upToken.getPassword(), getName());
    }
    
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        if (principals == null) {
            throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
        }
        
        // Get roles and perms from the NNVision CES plugin...
        
        Set<String> shRoles = new LinkedHashSet<>();
        Set<String> shPerms = new LinkedHashSet<>();
        
        if (restApi != null) {
            Object o = principals.getPrimaryPrincipal();
            if (o != null) {
                String username = o.toString();
                Set<String> roles = restApi.getUserRoles(username);
                shRoles.addAll(roles);
            }
        }
        
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(shRoles);
        info.setStringPermissions(shPerms);
        return info;
    }
    
}
