/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces;

import java.io.FileInputStream;

import java.nio.charset.StandardCharsets;

import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.ArrayList;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import org.apache.commons.io.IOUtils;

import org.json.JSONObject;
import org.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nndata.ces.controller.ControllerApi;

/**
 *
 * @author glgay
 */
public class Widgets {
    
    public static final String BUILTIN_PATH = "/WEB-INF/builtin-widgets.json";
    
    private static final Logger log = LoggerFactory.getLogger(Widgets.class);
    
    private final Map<String, List<Widget>> allWidgets = new TreeMap<>();
    
    public void addAll(JSONArray arr, boolean builtin) {
        if (arr == null) {
            return;
        }
        try {
            
            int len  = arr.length();
            
            for (int x = 0; x < len; ++x) {
                JSONObject group = arr.optJSONObject(x);
                if (group != null) {
                    String name = group.optString("group");
                    if (StringUtils.isBlank(name)) {
                        continue;
                    }
                    JSONArray vis = group.optJSONArray("visualizations");
                    if (vis == null) {
                        continue;
                    }
                    int nvis = vis.length();
                    if (nvis == 0) {
                        continue;
                    }
                    List<Widget> widgets = allWidgets.get(name);
                    if (widgets == null) {
                        widgets = new ArrayList<>();
                        allWidgets.put(name, widgets);    
                    }
                    for (int y = 0; y < nvis; ++y) {
                        JSONObject jo = vis.optJSONObject(y);
                        Widget w = new Widget(jo, builtin);
                        if (w.isValid()) {
                            widgets.add(w);
                        }
                    }
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
    }
    
    public void merge(Widgets other) {
        try {
            
            for (String grp : other.allWidgets.keySet()) {
                List<Widget> mine = allWidgets.get(grp);
                List<Widget> his = other.allWidgets.get(grp);
                if (mine == null) {
                    mine = new ArrayList<>();
                    allWidgets.put(grp, mine);
                }
                mine.addAll(his);
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
    }
    
    public void addAll(String jsText, boolean builtin) {
        try {
            
            JSONArray arr = new JSONArray(jsText);
            addAll(arr, builtin);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
    }
    
    public Set<String> getGroups() {
        return allWidgets.keySet();
    }
    
    public List<Widget> getWidgetsByGroup(String group) {
        List<Widget> widgets = allWidgets.get(group);
        if (widgets == null) {
            widgets = new ArrayList<>(0);
        }
        return widgets;
    }
    
    public Map<String, List<Widget>> getRaw() {
        return allWidgets;
    }
    
    public List<Widget> getAll() {
        List<Widget> all = new ArrayList<>();
        for (String grp : allWidgets.keySet()) {
            all.addAll(allWidgets.get(grp));
        }
        return all;
    }
    
    public static Widgets loadAll(HttpServletRequest request) {
        Widgets widgets = new Widgets();
        try {
            
            String biPath = request.getServletContext().getRealPath(BUILTIN_PATH);
            
            try (FileInputStream fis = new FileInputStream(biPath)) {
                String jsText = IOUtils.toString(fis, StandardCharsets.UTF_8);
                widgets.addAll(jsText, true);
            }
            
            ControllerApi api = new ControllerApi();
            Widgets w2 = api.getWidgets();
            
            widgets.merge(w2);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return widgets;
    }
    
}
