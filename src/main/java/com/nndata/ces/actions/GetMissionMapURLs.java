/**
 * Copyright 2021, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.actions;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nndata.ces.UIConfig;

/**
 *
 * @author tmiller
 */
public class GetMissionMapURLs extends HttpServlet {
    
    private static class SaveError extends Exception { }

    private static final Logger log = LoggerFactory.getLogger(GetMissionMapURLs.class);

		@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
				ServletContext ctx = request.getServletContext();
				UIConfig  uiConfig = new UIConfig(ctx);
				String query = uiConfig.getMissionMapQueryString();

				response.setHeader("Content-Type", "application/UTF-8");
        response.getWriter().println(query);
		}
}
