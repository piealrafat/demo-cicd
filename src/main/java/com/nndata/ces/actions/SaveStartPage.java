/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.actions;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nndata.ces.UIConfig;

/**
 *
 * @author glgay
 */
public class SaveStartPage extends HttpServlet {
    
    private static class SaveError extends Exception { }

    private static final Logger log = LoggerFactory.getLogger(SaveStartPage.class);
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        
        JSONObject result = new JSONObject();
        
        result.put("status", "fail");
        result.put("msg",    "Server Error");
        
        try {
            
            String startPage = request.getParameter("page");
            if (StringUtils.isBlank(startPage)) {
                result.put("msg", "No page provided");
                throw new SaveError();
            }
            
            UIConfig uiConfig = new UIConfig(request.getServletContext());
            
            JSONObject prefs = uiConfig.getUserPrefs();
            
            prefs.put("start_page", startPage);
            
            if (uiConfig.saveUserPrefs(prefs)) {
                result.put("status", "success");
                result.remove("msg");
                result.put("page", startPage);
            }
            
        } catch (SaveError se) {
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        response.setHeader("Content-Type", "application/json");
        response.getWriter().println(result.toString());
    }
    
}
