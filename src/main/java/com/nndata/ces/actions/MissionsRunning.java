/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.actions;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nndata.ces.controller.ClusterData;

/**
 *
 * @author glgay
 */
public class MissionsRunning extends HttpServlet {
    
    private static final Logger log = LoggerFactory.getLogger(MissionsRunning.class);
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        
        JSONObject result = new JSONObject();
        
        result.put("status", "fail");
        result.put("msg",    "Server Error");
        
        try {
            
            ClusterData cd = new ClusterData();
            
            boolean running = cd.anyMissionsRunning();
            
            result.put("status",  "success");
            result.put("running", running);
            result.remove("msg");
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        response.setHeader("Content-Type", "application/json");
        response.getWriter().println(result.toString());
        
    }
    
}
