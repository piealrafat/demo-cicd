/**
 * Copyright 2021, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.actions;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import org.json.JSONObject;
import org.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nndata.ces.UIConfig;

/**
 *
 * @author glgay
 */
public class SaveMapState extends HttpServlet {
    
    private static class SaveError extends Exception { }

    private static final Logger log = LoggerFactory.getLogger(SaveMapState.class);
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        
        JSONObject result = new JSONObject();
        
        result.put("status", "fail");
        result.put("msg",    "Server Error");
        
        try {
            
            String sCenter  = request.getParameter("center");
            String sZoom    = request.getParameter("zoom");
            String strSession = request.getParameter("session");
            
            // Ignoring session param for now, always saving to session
            boolean isSession = false;
            if (!StringUtils.isBlank(strSession)) {
                isSession = strSession.equalsIgnoreCase("true");
            }
            if (StringUtils.isBlank(sCenter) || StringUtils.isBlank(sZoom)) {
                result.put("msg", "Missing parameters");
                throw new SaveError();
            }
            
            try {
                
                JSONArray center   = new JSONArray(sCenter);
                double    zoom     = Double.parseDouble(sZoom);
                UIConfig  uiConfig = new UIConfig(request.getServletContext());
                
                uiConfig.setMapState(center, zoom);
                if (!isSession){
                    JSONObject prefs = uiConfig.getUserPrefs();
                    JSONObject mapprefs = new JSONObject();
                    mapprefs.put("center", center); 
                    mapprefs.put("zoom", zoom);
                    prefs.put("map_prefs", mapprefs);
                    uiConfig.saveUserPrefs(prefs);
                }
                
                result.put("status", "success");
                result.remove("msg");
                
            } catch (Throwable t) {
                result.put("msg", "Invalid input");
                throw new SaveError();
            }
            
        } catch (SaveError se) {
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        response.setHeader("Content-Type", "application/json");
        response.getWriter().println(result.toString());
    }
    
}
