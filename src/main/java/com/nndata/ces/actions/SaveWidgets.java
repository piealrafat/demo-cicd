/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.actions;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import org.json.JSONObject;
import org.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nndata.ces.UIConfig;

/**
 *
 * @author glgay
 */
public class SaveWidgets extends HttpServlet {
    
    private static class SaveError extends Exception { }

    private static final Logger log = LoggerFactory.getLogger(SaveWidgets.class);
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        
        JSONObject result = new JSONObject();
        
        result.put("status", "fail");
        result.put("msg",    "Server Error");
        
        try {
            
            String    wText      = request.getParameter("widgets");
            String    strSession = request.getParameter("session");
            JSONArray inWidgets  = null;
            
            boolean isSession = false;
            if (!StringUtils.isBlank(strSession)) {
                isSession = strSession.equalsIgnoreCase("true");
            }
            
            try {
                inWidgets = new JSONArray(wText);
            } catch (Throwable t) {
                result.put("msg", "Invalid input");
                throw new SaveError();
            }
            
            // Translate front-end structure to the nrmalized back-end
            int       len        = inWidgets.length();
            JSONArray outWidgets = new JSONArray();
            
            for (int x = 0; x < len; ++x) {
                JSONObject inWidget = inWidgets.optJSONObject(x);
                if (inWidget != null) {
                    String title = inWidget.optString("title");
                    String link  = inWidget.optString("link");
                    if (!StringUtils.isBlank(title) && !StringUtils.isBlank(link)) {
                        JSONObject outWidget = new JSONObject();
                        outWidget.put("display_name", title);
                        outWidget.put("url", link);
                        outWidgets.put(outWidget);
                    }
                }
            }
            
            UIConfig   uiConfig = new UIConfig(request.getServletContext());
            
            if (isSession) {
                uiConfig.setActiveWidgets(outWidgets);
                result.put("status", "success");
                result.remove("msg");
            } else {
                
                JSONObject prefs = uiConfig.getUserPrefs();
                prefs.put("widgets", outWidgets);
                if (uiConfig.saveUserPrefs(prefs)) {
                    result.put("status", "success");
                    result.remove("msg");
                }
            }
            
        } catch (SaveError se) {
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        response.setHeader("Content-Type", "application/json");
        response.getWriter().println(result.toString());
    }
    
}
