/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.controller;

import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.TreeMap;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nndata.ces.AppListener;

/**
 *
 * @author glgay
 */
public class ClusterData {
    
    private static final Logger log              = LoggerFactory.getLogger(ClusterData.class);
    private static final String POLL_INTERVAL    = "controller.poll_interval";
    private static final long   DEFAULT_INTERVAL = 60000;

    
    private final List<NodeInfo>           allNodes = new ArrayList<>();
    private final Map<String, MissionInfo> missions = new TreeMap<>();
    private final List<String>             status   = new ArrayList<>();
    
    public static long getPollInterval() {
        long pollInterval = DEFAULT_INTERVAL;
        try {
            
            Properties props = AppListener.loadProperties();
            String str = props.getProperty(POLL_INTERVAL, Long.toString(DEFAULT_INTERVAL));
            try {
                pollInterval = Long.parseLong(str, 10);
            } catch (Throwable t) {
                log.error("Invalid Cluster Poll interval: " + str + ", using default: " + DEFAULT_INTERVAL);
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return pollInterval;
    }

    
    public ClusterData() {
        try {
            
            ControllerApi api = new ControllerApi();
            allNodes.addAll(api.getAllNodes());
            status.addAll(api.getStatus());
            missions.putAll(api.getMissionStatus());
            
            Set<String> missionNames = api.getMissions();
            
            for (String mn : missionNames) {
                if (!missions.containsKey(mn)) {
                    MissionInfo mi = new MissionInfo(mn);
                    missions.put(mn, mi);
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
    }
    
    public List<NodeInfo> getAllNodes() {
        return allNodes;
    }
    
    public List<MissionInfo> getMissions() {
        
        List<MissionInfo> result = new ArrayList<>();
        
        try {
            
            for (String mn : missions.keySet()) {
                MissionInfo mi = missions.get(mn);
                result.add(mi);
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public boolean anyMissionsRunning() {
        boolean isRunning = false;
        try {
            
            for (Map.Entry<String, MissionInfo> entry : missions.entrySet()) {
                Set<String> running = entry.getValue().getClustersByState(MissionInfo.RUNNING);
                if (!running.isEmpty()) {
                    isRunning = true;
                    break;
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return isRunning;
    }
    
    public Set<String> getClusters() {
        Set<String> result = new TreeSet<>();
        result.add("unnamed");
        try {
            
            for (Map.Entry<String, MissionInfo> entry : missions.entrySet()) {
                result.addAll(entry.getValue().getClusters().keySet());
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public List<String> getStatus() {
        return status;
    }
    
    public List<NodeInfo> getClusteredNodes(String clusterName) {
        List<NodeInfo> result = new ArrayList<>(allNodes.size());
        try {
            
            for (NodeInfo node : allNodes) {
                if (node.getState() == NodeState.CLUSTERED) {
                    if (node.getClusterName().equals(clusterName)) {
                        result.add(node);
                    }
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public List<NodeInfo> getAvailableNodes() {
        List<NodeInfo> result = new ArrayList<>(allNodes.size());
        try {
            
            for (NodeInfo node : allNodes) {
                if ((node.getState() == NodeState.INITIALIZED) ||
                    (node.getState() == NodeState.UNINITIALIZED)) {
                    result.add(node);
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public List<NodeInfo> getExternalNodes() {
        List<NodeInfo> result = new ArrayList<>(allNodes.size());
        try {
            
            for (NodeInfo node : allNodes) {
                if (node.getState() == NodeState.EXTERNAL) {
                    result.add(node);
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
}
