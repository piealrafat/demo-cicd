/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.controller;

import java.io.IOException;

import java.util.List;
import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import org.json.JSONObject;
import org.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author glgay
 */
public class ClusterAction extends HttpServlet {
    
    private static final Logger log = LoggerFactory.getLogger(ClusterAction.class);
    private static class ActionError extends Exception { }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        
        JSONObject result = new JSONObject();
        
        result.put("status", "fail");
        result.put("msg",    "Server Error");
        
        try {
            
            String action  = request.getParameter("action");
            String itemStr = request.getParameter("items");
            String cluster = request.getParameter("cluster");
            
            if (StringUtils.isBlank(action)) {
                result.put("msg", "No action specified");
                throw new ActionError();
            }
            
            if (StringUtils.isBlank(cluster)) {
                result.put("msg", "No cluster specified");
                throw new ActionError();
            }
            
            JSONArray itemJson = new JSONArray(itemStr);
            int       nItems   = itemJson.length();
            
            if (nItems < 1) {
                result.put("msg", "No items specified");
                throw new ActionError();
            }
            
            List<String> items = new ArrayList<>(nItems);
            for (int x = 0; x < nItems; ++x) {
                String str = itemJson.optString(x);
                if (!StringUtils.isBlank(str)) {
                    items.add(str);
                }
            }
            
            if (items.isEmpty()) {
                result.put("msg", "Invalid item list");
                throw new ActionError();
            }
            
            ControllerApi api    = new ControllerApi();
            boolean       worked = false;
            
            switch (action) {
                case "joincluster": {
                    worked = api.joinCluster(cluster, items);
                    break;
                }
                case "evictnodes": {
                    worked = api.evictNodes(cluster, items);
                    break;
                }
                case "deploymission": {
                    worked = api.deployMissions(cluster, items);
                    break;
                }
                case "terminatemission": {
                    worked = api.terminateMissions(cluster, items);
                    break;
                }
                default: {
                    result.put("msg", "Invalid action - " + action);
                    throw new ActionError();
                }
            }
            
            if (worked) {
                result.put("status", "success");
                result.remove("msg");
            } else {
                result.put("msg", "Cluster Action request failed");
            }
            
        } catch (ActionError ae) {
        } catch (Throwable t) {
            result.put("msg", "Server Error - " + t.getMessage());
            log.error(t.getMessage(), t);
        }
        
        response.setHeader("Content-Type", "application/json");
        response.getWriter().println(result.toString());
    }
    
}
