/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.controller;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Collections;

import org.apache.commons.lang3.StringUtils;

import org.json.JSONObject;
import org.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author glgay
 */
public class MissionInfo {
    
    private static final Logger log         = LoggerFactory.getLogger(MissionInfo.class);
    
    public static final String RUNNING     = "Running";
    public static final String IN_PROGRESS = "In Progress";
    
    private final String              name;
    private final Map<String, String> clusters = new TreeMap<>();
    
    public MissionInfo(String name) {
        this.name = name;
    }
    
    public Set<String> getClustersByState(String state) {
        Set<String> result = new TreeSet<>();
        try {
            for (String cluster : clusters.keySet()) {
                String val = clusters.get(cluster);
                if (val.equals(state)) {
                    result.add(cluster);
                }
            }
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public String getClustersByStateAsString(String state) {
        String result = "";
        try {
            
            Set<String> byState = getClustersByState(state);
            if (!byState.isEmpty()) {
                result = String.join(",", byState);
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public String getClustersByStateAsArray(String state) {
        JSONArray result = new JSONArray();
        try {
            
            Set<String> byState = getClustersByState(state);
            for (String bs : byState) {
                result.put(bs);
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result.toString();
    }
    
    public String              getName()               { return name; }
    public Map<String, String> getClusters()           { return Collections.unmodifiableMap(clusters); }
    public Set<String>         getRunning()            { return getClustersByState(RUNNING); }
    public String              getRunningAsString()    { return getClustersByStateAsString(RUNNING); }
    public String              getRunningAsArray()     { return getClustersByStateAsArray(RUNNING); }
    public Set<String>         getInProgress()         { return getClustersByState(IN_PROGRESS); }
    public String              getInProgressAsString() { return getClustersByStateAsString(IN_PROGRESS); }
    public String              getInProgressAsArray()  { return getClustersByStateAsArray(IN_PROGRESS); }
    
    private static MissionInfo parse(JSONObject jo) {
        
        MissionInfo result = null;
        
        try {
            
            if (jo == null) {
                return null;
            }
            
            String name = jo.optString("mission_name");
            if (StringUtils.isBlank(name)) {
                log.error("Received empty mission name");
                return null;
            }
            
            result = new MissionInfo(name);
            
            JSONArray clusters = jo.optJSONArray("clusters");
            if (clusters != null) {
                int len = clusters.length();
                for (int x = 0; x < len; ++x) {
                    JSONObject cluster = clusters.optJSONObject(x);
                    if (cluster != null) {
                        String clName = cluster.optString("cluster");
                        String status = cluster.optString("status");
                        if (!StringUtils.isBlank(clName) && !StringUtils.isBlank(status)) {
                            result.clusters.put(clName, status);
                        }
                    }
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
    public static List<MissionInfo> parse(String json) {
        List<MissionInfo> result = new ArrayList<>();
        try {
            
            JSONArray missions = new JSONArray(json);
            int len = missions.length();
            
            for (int x = 0; x < len; ++x) {
                JSONObject mission = missions.optJSONObject(x);
                if (mission != null) {
                    MissionInfo mi = parse(mission);
                    if (mi != null) {
                        result.add(mi);
                    }
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
}
