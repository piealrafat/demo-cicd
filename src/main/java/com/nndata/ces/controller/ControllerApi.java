/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.controller;

import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.utils.URIBuilder;

import org.apache.http.entity.StringEntity;

import org.apache.http.conn.ssl.NoopHostnameVerifier;

import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.CloseableHttpClient;

import org.apache.http.ssl.SSLContextBuilder;

import org.apache.http.util.EntityUtils;

import org.json.JSONObject;
import org.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nndata.ces.UIConfig;
import com.nndata.ces.Widgets;
import com.nndata.ces.api.TrustAllStrategy;

/**
 *
 * @author glgay
 */
public class ControllerApi {
    
    private static final Logger log      = LoggerFactory.getLogger(ControllerApi.class);
    private static final String REST_URL = "controller.rest.url";
    
    private String restUrl;
    
    public ControllerApi() {
        try {
            
            UIConfig uiConfig = new UIConfig(null);
            String   uiHome   = uiConfig.getUiHome();
            
            restUrl = uiConfig.getProperty(REST_URL, "http://localhost:8080" + uiHome + "/controllerStub");
            
            if (!restUrl.endsWith("/")) {
                restUrl += "/";
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
    }
    
    private static CloseableHttpClient createClient() throws Exception {
        
        RequestConfig.Builder requestBuilder = RequestConfig.custom();
        requestBuilder = requestBuilder.setConnectTimeout(10000);
        requestBuilder = requestBuilder.setConnectionRequestTimeout(10000);

        HttpClientBuilder builder = HttpClientBuilder.create();     
        builder.setDefaultRequestConfig(requestBuilder.build());
        builder.setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE);
        builder.setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, TrustAllStrategy.INSTANCE).build());
        return builder.build();
    }
    
    public List<NodeInfo> getAllNodes() {
        
        List<NodeInfo> result = null;
        
        try (CloseableHttpClient client = createClient()) {
            
            HttpGet getter = new HttpGet(restUrl + "nodestate");
            CloseableHttpResponse rsp = client.execute(getter);
            
            String json = EntityUtils.toString(rsp.getEntity(), "UTF-8");
            result = NodeInfo.parse(json);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        if (result == null) {
            result = new ArrayList<>(0);
        }
        return result;
    }
    
    public int getComposableCount() {
        
        int            count = 0;
        List<NodeInfo> nodes = getAllNodes();
        
        for (NodeInfo node : nodes) {
            if ((node.getState() == NodeState.CLUSTERED) ||
                (node.getState() == NodeState.INITIALIZED) ||
                (node.getState() == NodeState.UNINITIALIZED)) {
                ++count;
            }
        }
        
        return count;
    }
    
    public Map<String, MissionInfo> getMissionStatus() {
        Map<String, MissionInfo> result = new TreeMap<>();
        try (CloseableHttpClient client = createClient()) {
            
            HttpGet getter = new HttpGet(restUrl + "missionstatus2");
            CloseableHttpResponse rsp = client.execute(getter);
            
            String    json = EntityUtils.toString(rsp.getEntity(), "UTF-8");
            
            List<MissionInfo> missions = MissionInfo.parse(json);
            
            for (MissionInfo mi : missions) {
                result.put(mi.getName(), mi);
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public Set<String> getMissions() {
        Set<String> result = new TreeSet<>();
        try (CloseableHttpClient client = createClient()) {
            
            HttpGet getter = new HttpGet(restUrl + "missions");
            CloseableHttpResponse rsp = client.execute(getter);
            
            String    json = EntityUtils.toString(rsp.getEntity(), "UTF-8");
            JSONArray arr  = new JSONArray(json);
            int       len  = arr.length();
            
            for (int x = 0; x < len; ++x) {
                String mission = arr.optString(x);
                if (!StringUtils.isBlank(mission)) {
                    result.add(mission);
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public List<String> getStatus() {
        List<String> result = new ArrayList<>();
        try (CloseableHttpClient client = createClient()) {
            
            HttpGet getter = new HttpGet(restUrl + "status");
            CloseableHttpResponse rsp = client.execute(getter);
            
            String    json = EntityUtils.toString(rsp.getEntity(), "UTF-8");
            JSONArray arr  = new JSONArray(json);
            int       len  = arr.length();
            
            for (int x = 0; x < len; ++x) {
                String msg = arr.optString(x);
                if (!StringUtils.isBlank(msg)) {
                    result.add(msg);
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public String getMaster(String clusterName) {
        String result = "<none>";
        try (CloseableHttpClient client = createClient()) {
            
            if (StringUtils.isBlank(clusterName)) {
                clusterName = "unnamed";
            }
            
            URIBuilder builder = new URIBuilder(restUrl + "getmaster");
            builder.addParameter("clustername", clusterName);
            
            HttpGet getter = new HttpGet(builder.build());
            CloseableHttpResponse rsp = client.execute(getter);
            
            String json = EntityUtils.toString(rsp.getEntity(), "UTF-8");
            JSONObject jo = new JSONObject(json);
            String master = jo.optString("master");
            if (!StringUtils.isBlank(master)) {
                result = master;
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public Widgets getWidgets() {
        
        Widgets widgets = new Widgets();
        
        try (CloseableHttpClient client = createClient()) {
            
            HttpGet getter = new HttpGet(restUrl + "visualizations");
            CloseableHttpResponse rsp = client.execute(getter);
            
            String json = EntityUtils.toString(rsp.getEntity(), "UTF-8");
            widgets.addAll(json, false);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return widgets;
    }
    
    public boolean postAction(JSONObject request) {
        boolean worked = false;
        try (CloseableHttpClient client = createClient()) {
            
            HttpPost     poster = new HttpPost(restUrl + "requestkubeaction");
            StringEntity ent    = new StringEntity(request.toString(), "UTF-8");
            
            poster.setEntity(ent);
            poster.setHeader("Content-Type", "application/json");
            
            CloseableHttpResponse rsp = client.execute(poster);
            
            String     str = EntityUtils.toString(rsp.getEntity());
            JSONObject jo  = new JSONObject(str);
            
            String status = jo.optString("status", "rejected");
            
            worked = status.equals("submitted");
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return worked;
    }
    
    public boolean nodesAction(String action, String clusterName, List<String> nodeList) {
        boolean worked = false;
        try {
            
            JSONObject request = new JSONObject();
            
            request.put("action",      action);
            request.put("clustername", clusterName);
            
            JSONArray nodes = new JSONArray();
            
            for (String node : nodeList) {
                nodes.put(node);
            }
            request.put("nodelist", nodes);
            
            worked = postAction(request);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return worked;
    }
    
    public boolean joinCluster(String clusterName, List<String> nodeList) {
        return nodesAction("joincluster", clusterName, nodeList);
    }
    
    public boolean evictNodes(String clusterName, List<String> nodeList) {
        return nodesAction("evictnodes", clusterName, nodeList);
    }
    
    public boolean deployMission(String clusterName, String missionName) {
        boolean worked = false;
        try {
            
            JSONObject request = new JSONObject();
            
            request.put("action", "deploymission");
            request.put("clustername", clusterName);
            request.put("missionname", missionName);
            
            worked = postAction(request);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return worked;
    }
    
    public boolean terminateMission(String clusterName, String missionName) {
        boolean worked = false;
        try {
            
            JSONObject request = new JSONObject();
            
            request.put("action", "terminatemission");
            request.put("clustername", clusterName);
            request.put("missionname", missionName);
            
            worked = postAction(request);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return worked;
    }
    
    public boolean deployMissions(String clusterName, List<String> missions) {
        boolean worked = true;
        try {
            
            for (String mission : missions) {
                if (!deployMission(clusterName, mission)) {
                    worked = false;
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
            worked = false;
        }
        return worked;
    }
    
    public boolean terminateMissions(String clusterName, List<String> missions) {
        boolean worked = true;
        try {
            
            for (String mission : missions) {
                if (!terminateMission(clusterName, mission)) {
                    worked = false;
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
            worked = false;
        }
        return worked;
    }
    
}
