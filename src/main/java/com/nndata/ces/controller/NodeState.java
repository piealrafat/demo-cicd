/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.controller;

/**
 *
 * @author glgay
 */
public enum NodeState {
    
    EXTERNAL,
    UNINITIALIZED,
    INITIALIZED,
    CLUSTERED,
    UNKNOWN;
    
    public static NodeState fromString(String state) {
        NodeState result = UNKNOWN;
        try {
            
            result = NodeState.valueOf(state.toUpperCase());
            
        } catch (Throwable t) {
        }
        return result;
    }
    
}
