/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.controller;

import java.util.List;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

import org.json.JSONObject;
import org.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author glgay
 */
public class NodeInfo {
    
    private static final Logger log          = LoggerFactory.getLogger(NodeInfo.class);
    private static final String DEFAULT_ROLE = "<none>";
    
    private final String    hostname;
    private final String    ip;
    private final String    description;
    private final NodeState state;
    private final long      updated;

    private String clusterName = "";
    private String role        = DEFAULT_ROLE;
    
    private NodeInfo(String hostname, String ip, String description, NodeState state, long updated) {
        this.hostname    = hostname;
        this.ip          = ip;
        this.description = description;
        this.state       = state;
        this.updated     = updated;
    }
    
    public String    getHostname()    { return hostname; }
    public String    getIp()          { return ip; }
    public String    getDescription() { return description; }
    public NodeState getState()       { return state; }
    public long      getUpdated()     { return updated; }
    public String    getClusterName() { return clusterName; }
    public String    getRole()        { return role; }
    
    public void setClusterName(String clusterName) {
        if (StringUtils.isBlank(clusterName)) {
            this.clusterName = "";
        } else {
            this.clusterName = clusterName;
        }
    }
    
    public void setRole(String role) {
        if (StringUtils.isBlank(role)) {
            this.role = DEFAULT_ROLE;
        } else {
            this.role = role;
        }
    }
    
    private static NodeInfo parse(JSONObject jo) {
        NodeInfo result = null;
        try {
            
            if (jo == null) {
                return null;
            }
            
            String    hostname = jo.optString("hostname");
            String    ip       = jo.optString("ip");
            String    desc     = jo.optString("description", "");
            long      updated  = jo.optLong("updated", 0);
            String    sState   = jo.optString("state");
            NodeState state    = NodeState.fromString(sState);
            
            if (StringUtils.isBlank(hostname)) {
                log.error("Received empty node hostname");
                return null;
            }
            if (StringUtils.isBlank(ip)) {
                log.error("No IP Address for host [" + hostname + "]");
                return null;
            }
            if (state == NodeState.UNKNOWN) {
                log.error("Unrecognized node state [" + sState + "]");
                return null;
            }
            
            result = new NodeInfo(hostname, ip, desc, state, updated);
            
            JSONObject nodeLabels = jo.optJSONObject("nodelabels");
            String     role       = jo.optString("clusterrole");
            String     cluster    = jo.optString("clustername");
            
            if (StringUtils.isBlank(role) && (nodeLabels != null)) {
                role = nodeLabels.optString("clusterrole");
            }
            
            if (StringUtils.isBlank(cluster) && (nodeLabels != null)) {
                cluster = nodeLabels.optString("clustername");
            }
            
            result.setClusterName(cluster);
            result.setRole(role);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public static List<NodeInfo> parse(String json) {
        List<NodeInfo> result = new ArrayList<>();
        try {
            
            JSONArray nodes = new JSONArray(json);
            int len = nodes.length();
            for (int x = 0; x < len; ++x) {
                NodeInfo info = parse(nodes.optJSONObject(x));
                if (info != null) {
                    result.add(info);
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
}
