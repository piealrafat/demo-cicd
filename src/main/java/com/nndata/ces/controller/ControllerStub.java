/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.controller;

import java.io.IOException;
import java.io.FileInputStream;

import java.util.Scanner;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author glgay
 */
public class ControllerStub extends HttpServlet {
    
    private static final Logger log = LoggerFactory.getLogger(ControllerStub.class);
    
    private static String readTextFile(String path) {
        String result = "";
        try (FileInputStream fis = new FileInputStream(path)) {
            
            StringBuilder sb  = new StringBuilder();
            Scanner       scn = new Scanner(fis, "UTF-8");
            
            while (scn.hasNext()) {
                sb.append(scn.nextLine());
            }
            
            result = sb.toString();
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            
            String text = "";
            String uri  = request.getRequestURI();
            
            if (uri.endsWith("/nodestate")) {
                String path = request.getServletContext().getRealPath("/WEB-INF/controller-stubs/nodestate.json");
                text = readTextFile(path);
            } else if (uri.endsWith("/missions")) {
                String path = request.getServletContext().getRealPath("/WEB-INF/controller-stubs/missions.json");
                text = readTextFile(path);
            } else if (uri.endsWith("/getmaster")) {
                String path = request.getServletContext().getRealPath("/WEB-INF/controller-stubs/getmaster.json");
                text = readTextFile(path);
            } else if (uri.endsWith("/status")) {
                String path = request.getServletContext().getRealPath("/WEB-INF/controller-stubs/status.json");
                text = readTextFile(path);
            }  else if (uri.endsWith("/missionstatus")) {
                String path = request.getServletContext().getRealPath("/WEB-INF/controller-stubs/mission-status.json");
                text = readTextFile(path);
            } else if (uri.endsWith("/missionstatus2")) {
                String path = request.getServletContext().getRealPath("/WEB-INF/controller-stubs/missionstatus2.json");
                text = readTextFile(path);
            } else if (uri.endsWith("/visualizations")) {
                String path = request.getServletContext().getRealPath("/WEB-INF/controller-stubs/visualizations.json");
                text = readTextFile(path);
            }
            
            response.setHeader("Content-Type", "application/json");
            response.getWriter().println(text);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String path = request.getServletContext().getRealPath("/WEB-INF/controller-stubs/action-response.json");
        String text = readTextFile(path);
        
        response.setHeader("Content-Type", "application/json");
        response.getWriter().println(text);
    }
    
}
