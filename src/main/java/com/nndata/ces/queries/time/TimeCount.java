/**
 * Copyright 2021, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.queries.time;

import java.util.List;
import java.util.ArrayList;

import org.json.JSONObject;
import org.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author glgay
 */
public class TimeCount {
    
    private static final Logger log = LoggerFactory.getLogger(TimeCount.class);
    
    private long time;
    private long count;
    private String mode;
    
    private TimeCount(long time, long count, String mode) {
        this.time  = time;
        this.count = count;
        this.mode = mode;
    }
    
    public long getTime() {
        return time;
    }
    
    public long getCount() {
        return count;
    }
    
    public String getMode() {
        return mode;
    }
    
    public static List<TimeCount> parse(JSONObject jo, String mode) {
        List<TimeCount> result = new ArrayList<>();
        try {
            
            JSONArray counts = jo.optJSONArray("counts");
            if (counts != null) {
                int len = counts.length();
                for (int x = 0; x < len; ++x) {
                    JSONObject cnt = counts.optJSONObject(x);
                    if (cnt != null) {
                        long count = cnt.optLong("count", -1);
                        long time  = cnt.optLong("time",  -1);
                        if ((count > 0) && (time > 0)) {
                            TimeCount tc = new TimeCount(time, count, mode);
                            result.add(tc);
                        }
                    }
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
}
