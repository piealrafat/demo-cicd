/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.queries.time;

import java.io.IOException;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nndata.ces.UIConfig;

import com.nndata.ces.api.RestAPI;
import com.nndata.ces.api.InvalidPrincipalException;

/**
 *
 * @author glgay/JackH
 */
public class GetModeTimeData  extends HttpServlet {
    
    private static final Logger log       = LoggerFactory.getLogger(GetIntitalTimeData.class);
    private static final long   ONE_DAY   = 1000L * 60 * 60 * 24;
    private static final long   ONE_MONTH = ONE_DAY * 30; // Not quite a month, but...
    private static final long   ONE_YEAR  = ONE_DAY * 365;
    
    private static class TimeError extends Exception { }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        
        JSONObject result = new JSONObject();
        
         
        result.put("status", "fail");
        result.put("msg",    "Server Error");
        result.put("msg2", "fail");
        
        try {
            
            result.put("status", "fail 0");
            UIConfig         uiConfig = new UIConfig(getServletContext());
            String           timeZone = uiConfig.getTimeZone();
            RestAPI          restApi  = new RestAPI(request.getServletContext());
            List<TimeSet>    timeSets = new ArrayList<>();
            JSONObject       jo       = restApi.getCesTimeFields();
            Date datestart = new Date();
            result.put("status", "date1 =" + request.getParameter("datestart"));
            datestart.setTime(Long.parseLong(request.getParameter("datestart")));//Integer.parseInt(datestartstr.substring(0, datestartstr.indexOf('-'))),Integer.parseInt(datestartstr.substring(datestartstr.indexOf('-'), datestartstr.indexOf('-',datestartstr.indexOf('-')+1))),Integer.parseInt(datestartstr.substring(datestartstr.indexOf('-',datestartstr.indexOf('-')+1)+1,datestartstr.indexOf('T')))); // request.getParameter("datestart")
            
            result.put("status", "date2=" + datestart);
            //Date dateend = new Date(request.getParameter("dateend"));
            result.put("status", "zoom=" + request.getParameter("zoomlevel"));
            String zoomlevel = request.getParameter("zoomlevel");
            result.put("status", "group=" + request.getParameter("groupas"));
            int groupas = Integer.parseInt(request.getParameter("groupas"));
            
            
            if (jo == null) {
                throw new TimeError();
            }
            String status = jo.optString("status", "fail");
            if (!status.equals("success")) {
                throw new TimeError();
            }
            
            JSONArray flds = jo.optJSONArray("fields");
            if (flds != null) {
                int len = flds.length();
                for (int x = 0; x < len; ++x) {
                    JSONObject fld = flds.optJSONObject(x);
                    if (fld != null) {
                        String setName = fld.getString("set_name");
                        String setOid  = fld.getString("set_oid");
                        String fName   = fld.getString("field");
                        
                        TimeSet ts = new TimeSet(setName, setOid, fName);
                        timeSets.add(ts);
                    }
                }
            }
            result.put("status", "fail 1");
            
            
            // We want hour counts for the past 24 hours
            // and day counts before that.  Figure out
            // the time 24 hours ago...
            long tick      = System.currentTimeMillis();
            Date now       = new Date(tick);
            Date yesterday = new Date(tick - ONE_DAY);
            Date t30days   = new Date(tick - ONE_DAY - ONE_MONTH);
            Date t1year    = new Date(tick - ONE_MONTH - ONE_YEAR);
            Date dateend   = new Date(Long.parseLong(request.getParameter("datestart")) + (zoomlevel.equals("hour")?ONE_DAY:(zoomlevel.equals("day")?ONE_MONTH:ONE_YEAR)));
            
            result.put("msg2", datestart + " " + dateend + " " + zoomlevel.equals("hour") + " " + zoomlevel.equals("day") + " " + zoomlevel.equals("month") + " " + zoomlevel.equals("year"));
            
            int i = 0;
            for (TimeSet ts : timeSets) {
                if(i == groupas || zoomlevel.equals("year")){
                    JSONObject cnts;
                    if(zoomlevel.equals("hour")){
                        // Hours for the current day...
                        cnts = restApi.getCesTimeCounts(ts.getSetOid(), ts.getField(), "hour", datestart, dateend, timeZone);
                        ts.addTimeCounts(cnts, "hour");
                    } else if(zoomlevel.equals("day")){
                        // Days for the previous month
                        cnts = restApi.getCesTimeCounts(ts.getSetOid(), ts.getField(), "day", datestart, dateend, timeZone);
                        ts.addTimeCounts(cnts, "day");
                    } else if(zoomlevel.equals("month")){                    
                        // Months for the previous year
                        cnts = restApi.getCesTimeCounts(ts.getSetOid(), ts.getField(), "month", datestart, dateend, timeZone);
                        ts.addTimeCounts(cnts, "month");
                    } else if(zoomlevel.equals("year")){
                        // And years prior to that
                        
                        cnts = restApi.getCesTimeCounts(ts.getSetOid(), ts.getField(), "hour", yesterday, now, timeZone);
                        ts.addTimeCounts(cnts, "hour");

                        // Days for the previous month
                        cnts = restApi.getCesTimeCounts(ts.getSetOid(), ts.getField(), "day", t30days, yesterday, timeZone);
                        ts.addTimeCounts(cnts, "day");

                        // Months for the previous year
                        cnts = restApi.getCesTimeCounts(ts.getSetOid(), ts.getField(), "month", t1year, t30days, timeZone);
                        ts.addTimeCounts(cnts, "month");

                        // And years prior to that
                        cnts = restApi.getCesTimeCounts(ts.getSetOid(), ts.getField(), "year", null, t1year, timeZone);
                        ts.addTimeCounts(cnts, "year");
                        //cnts = restApi.getCesTimeCounts(ts.getSetOid(), ts.getField(), "year", datestart, dateend, timeZone);
                        //ts.addTimeCounts(cnts, "year");
                    }
                }
                i++;
            }
            result.put("status", "fail 2");
            
            JSONArray groups = new JSONArray();
            for (TimeSet ts : timeSets) {
                
                JSONObject grp = new JSONObject();
                
                grp.put("id",      ts.getSetOid() + "/" + ts.getField());
                grp.put("content", ts.getSetName() + " - " + ts.getPrettyField());
                grp.put("setid",   ts.getSetOid());
                grp.put("field",   ts.getField());
                
                JSONArray       times  = new JSONArray();
                List<TimeCount> counts = ts.getCounts();
                
                for (TimeCount tc : counts) {
                    JSONObject c = new JSONObject();
                    c.put("time",  tc.getTime());
                    c.put("count", tc.getCount());
                    c.put("mode", tc.getMode());
                    times.put(c);
                }
                
                grp.put("counts", times);
                //grp.put("alttimes", counts.);
                
                groups.put(grp);
            }
            
            result.put("status", "success");
            result.put("groups", groups);
            result.remove("msg");
            
        } catch (TimeError te) {
        } catch (InvalidPrincipalException ipe) {  
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        response.setHeader("Content-Type", "application/json");
        response.getWriter().println(result.toString());
    }
    
}