/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.queries.time;

import java.util.List;
import java.util.ArrayList;

import org.json.JSONObject;

import com.nndata.ces.api.DataField;

/**
 *
 * @author glgay
 */
public class TimeSet {
    
    private final String          setName;
    private final String          setOid;
    private final String          field;
    private final String          prettyField;
    private final List<TimeCount> timeCounts = new ArrayList<>();
    
    public TimeSet(String setName, String setOid, String field, String prettyField) {
        this.setName     = setName;
        this.setOid      = setOid;
        this.field       = field;
        this.prettyField = prettyField;
    }
    
    public TimeSet(String setName, String setOid, String field) {
        this.setName     = setName;
        this.setOid      = setOid;
        this.field       = field;
        this.prettyField = DataField.makePrettyName(field);
    }
    
    public void addTimeCounts(JSONObject jo, String mode) {
        if (jo != null) {
            List<TimeCount> counts = TimeCount.parse(jo, mode);
            timeCounts.addAll(counts);
        }
    }
    
    public String getSetName() {
        return setName;
    }
    
    public String getSetOid() {
        return setOid;
    }
    
    public String getField() {
        return field;
    }
    
    public String getPrettyField() {
        return prettyField;
    }
    
    public List<TimeCount> getCounts() {
        return timeCounts;
    }
    
}
