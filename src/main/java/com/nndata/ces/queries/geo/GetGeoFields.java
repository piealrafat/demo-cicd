/**
 * Copyright 2021, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.queries.geo;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nndata.ces.api.RestAPI;
import com.nndata.ces.api.DataField;

/**
 *
 * @author glgay
 */
public class GetGeoFields extends HttpServlet {
    
    private static final Logger log = LoggerFactory.getLogger(GetGeoFields.class);

    private static class GeoError extends Exception { }
    
    private String prettyFieldName(String fullName) {
        
        String result = fullName;
        
        try {
            
            String[] parts = fullName.split("[.]");
            int      np    = parts.length;
            
            String pn1 = null;
            String pn2 = null;
            
            if (np > 3) {
                pn1 = parts[np - 2];
                pn2 = parts[np - 1];
            } else {
                pn1 = parts[np - 1];
            }
            
            pn1 = DataField.makePrettyName(pn1);
            if (pn2 == null) {
                result = pn1;
            } else {
                pn2    = DataField.makePrettyName(pn2);
                result = pn1 + " " + pn2;
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        
        JSONObject result = new JSONObject();
        
        result.put("status", "fail");
        result.put("msg",    "Server Error");
        
        try {
            
            RestAPI    restApi = new RestAPI(request.getServletContext());
            JSONObject jo      = restApi.getCesGeoFields();
            
            if (jo == null) {
                throw new GeoError();
            }
            
            // Try to make pretty display names for fields
            // If the field name has 4 or more components,
            // use the last 2.  Otherwise, just use the last one
            JSONArray fields = jo.optJSONArray("fields");
            if (fields != null) {
                int len = fields.length();
                for (int x = 0; x < len; ++x) {
                    JSONObject fld = fields.optJSONObject(x);
                    if (fld != null) {
                        String fullName = fld.optString("field", "unknown");
                        String pretty   = prettyFieldName(fullName);
                        
                        fld.put("displayName", pretty);
                    }
                }
            }
            
            // The JSON from the REST server can just be reflected into the UI...
            result = jo;
            
        } catch (GeoError ge) {
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        response.setHeader("Content-Type", "application/json");
        response.getWriter().println(result.toString());
    }
    
}
