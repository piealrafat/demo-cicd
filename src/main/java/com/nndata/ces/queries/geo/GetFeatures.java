/**
 * Copyright 2021, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.queries.geo;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import org.json.JSONObject;
import org.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nndata.ces.api.RestAPI;

/**
 *
 * @author glgay
 */
public class GetFeatures extends HttpServlet {
    
    private static final Logger log = LoggerFactory.getLogger(GetFeatures.class);
    
    private static class GeoError extends Exception { }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        
        JSONObject result = new JSONObject();
        
        result.put("status", "fail");
        result.put("msg",    "Server Error");
        
        try {
            
            String setOid   = request.getParameter("set");
            String field    = request.getParameter("field");
            String sTopX    = request.getParameter("topx");
            String sTopY    = request.getParameter("topy");
            String sBottomX = request.getParameter("bottomx");
            String sBottomY = request.getParameter("bottomy");
            
            if (StringUtils.isBlank(setOid)) {
                result.put("msg", "Missing Data Set");
                throw new GeoError();
            }
            if (StringUtils.isBlank(field)) {
                result.put("msg", "Missing field name");
                throw new GeoError();
            }
            
            double topx = 0, topy = 0, bottomx = 0, bottomy = 0;
            
            try {
                topx = Double.parseDouble(sTopX);
            } catch (Throwable t) {
                result.put("msg", "Invalid Top X: " + sTopX);
            }
            try {
                topy = Double.parseDouble(sTopY);
            } catch (Throwable t) {
                result.put("msg", "Invalid Top Y: " + sTopY);
            }
            try {
                bottomx = Double.parseDouble(sBottomX);
            } catch (Throwable t) {
                result.put("msg", "Invalid Bottom X: " + sBottomX);
            }
            try {
                bottomy = Double.parseDouble(sBottomY);
            } catch (Throwable t) {
                result.put("msg", "Invalid Bottom Y: " + sBottomY);
            }
            
            RestAPI    restApi = new RestAPI(request.getServletContext());
            JSONObject jo      = restApi.getCesGeoCounts(setOid, field, topx, topy, bottomx, bottomy);
            
            String status = jo.optString("status", "fail");
            if (!status.equals("success")) {
                String msg = jo.optString("msg");
                if (!StringUtils.isBlank(msg)) {
                    result.put("msg", msg);
                    throw new GeoError();
                }
            }
            
            JSONObject geoJSON  = new JSONObject();
            JSONObject geoProps = new JSONObject();
            JSONObject crs      = new JSONObject();
            JSONObject crsProps = new JSONObject();
            JSONArray  features = new JSONArray();
            JSONArray  counts   = jo.optJSONArray("counts");
            
            geoProps.put("ces_set_oid",  jo.getString("set_oid"));
            geoProps.put("ces_set_name", jo.getString("set_name"));
            geoProps.put("ces_field",    jo.getString("field"));
            
            crsProps.put("name",  "epsg:4326"); // WGS84
            crs.put("type",       "name");
            crs.put("properties", crsProps);
            
            geoJSON.put("type",       "FeatureCollection");
            geoJSON.put("crs",        crs);
            geoJSON.put("properties", geoProps);
            
            int len = counts.length();
            for (int x = 0; x < len; ++x) {
                JSONObject hit = counts.optJSONObject(x);
                if (hit != null) {
                    long   count = hit.optLong("count", 0);
                    double lat   = hit.optDouble("lat", 0);
                    double lon   = hit.optDouble("lon", 0);
                    String key   = hit.optString("key");
                    
                    if ((count > 0) && !StringUtils.isBlank(key)) {
                        JSONObject geo    = new JSONObject();
                        JSONObject props  = new JSONObject();
                        JSONObject point  = new JSONObject();
                        JSONArray  coords = new JSONArray();
                        
                        props.put("count", count);
                        props.put("hash",  key);
                        
                        coords.put(lon);
                        coords.put(lat);
                        
                        point.put("type",        "Point");
                        point.put("coordinates", coords);
                        
                        geo.put("type",       "Feature");
                        geo.put("properties", props);
                        geo.put("geometry",   point);
                        
                        features.put(geo);
                    }
                }
            }
            
            geoJSON.put("features", features);
            result = geoJSON;
            
        } catch (GeoError ge) {
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        response.setHeader("Content-Type", "application/json");
        response.getWriter().println(result.toString());
    }
    
}
