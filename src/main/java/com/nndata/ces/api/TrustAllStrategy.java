/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.api;

import java.security.cert.X509Certificate;

import org.apache.http.ssl.TrustStrategy;

/**
 *
 * @author glgay
 */
public class TrustAllStrategy implements TrustStrategy {
    
    public static final TrustAllStrategy INSTANCE = new TrustAllStrategy();
    
    @Override
    public boolean isTrusted(X509Certificate[] certs, String authType) {
        return true;
    }
    
}
