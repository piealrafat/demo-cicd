/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.api;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.TreeMap;

import org.json.JSONObject;
import org.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author glgay
 */
public class DataSource {
    
    private static final Logger log = LoggerFactory.getLogger(DataSource.class);
    
    private final String        name;
    private final String        desc;
    private final String        oid;
    private final long          id;
    private final long          userId;
    private final long          createTime;
    private final List<DataSet> dataSets;
    
    private long rowCount = 0;
    
    private DataSource(JSONObject jsSrc) {
        
        // We aren't doing any sanity checks here - assuming we get good stuff
        name       = jsSrc.optString("src.name",        "");
        desc       = jsSrc.optString("src.description", "");
        oid        = jsSrc.optString("src.oid",         "");
        id         = jsSrc.optLong(  "src.id",          0L);
        userId     = jsSrc.optLong(  "src.userId",      0L);
        createTime = jsSrc.optLong(  "src.create_time", 0L);
        
        dataSets   = DataSet.parse(jsSrc);
    }
    
    private DataSource(String srcOid, String srcName) {
        name       = srcName;
        oid        = srcOid;
        desc       = "";
        id         = 0L;
        userId     = 0L;
        createTime = 0L;
        rowCount   = 0L;
        dataSets   = new ArrayList<>();
    }

    public String        getName()        { return name; }
    public String        getDescription() { return desc; }
    public String        getOid()         { return oid; }
    public long          getId()          { return id; }
    public long          getUserId()      { return userId; }
    public long          getCreateTime()  { return createTime; }
    public long          getRowCount()    { return rowCount; }
    public List<DataSet> getDataSets()    { return Collections.unmodifiableList(dataSets); }
    
    public static List<DataSource> parse(JSONObject inObj) {
        List<DataSource> result = new ArrayList<>();
        try {
            
            if (inObj == null) {
                return result;
            }
            
            JSONArray jsSrcs = inObj.optJSONArray("sources");
            if (jsSrcs == null) {
                return result;
            }
            
            int len = jsSrcs.length();
            for (int x = 0; x < len; ++x) {
                JSONObject jsSrc = jsSrcs.optJSONObject(x);
                if (jsSrc != null) {
                    DataSource src = new DataSource(jsSrc);
                    result.add(src);
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public static List<DataSource> fromRowCounts(JSONObject inObj, boolean includeEmpty) {
        List<DataSource> result = new ArrayList<>();
        try {
            
            if (inObj == null) {
                return result;
            }
            
            JSONArray jsSets = inObj.optJSONArray("data_sets");
            Map<String, List<DataSet>> bySrcOid = new TreeMap<>();

            if (jsSets != null) {                
                int len = jsSets.length();
                for (int x = 0; x < len; ++x) {
                    JSONObject jsSet = jsSets.optJSONObject(x);
                    if (jsSet != null) {
                        DataSet ds = DataSet.parseSingle(jsSet);
                        if (ds != null) {
                            String srcOid = ds.getSrcOid();
                            List<DataSet> srcSets = bySrcOid.get(srcOid);
                            if (srcSets == null) {
                                srcSets = new ArrayList<>();
                                bySrcOid.put(srcOid, srcSets);
                            }
                            srcSets.add(ds);
                        }
                    }
                }
            }
            
            // This feels a bit hacky but...
            for (String srcOid : bySrcOid.keySet()) {
                List<DataSet> srcSets = bySrcOid.get(srcOid);
                String srcName = srcSets.get(0).getSrcName();
                DataSource src = new DataSource(srcOid, srcName);
                for (DataSet ds : srcSets) {
                    if (includeEmpty || ds.getRowCount() > 0) {
                        src.dataSets.add(ds);
                        src.rowCount += ds.getRowCount();
                    }
                }
                result.add(src);
            }
            
            if (!includeEmpty) {
                Iterator<DataSource> iter = result.iterator();
                while (iter.hasNext()) {
                    DataSource src = iter.next();
                    if (src.rowCount == 0) {
                        iter.remove();
                    }
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
}
