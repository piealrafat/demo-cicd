/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.api;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

import org.json.JSONObject;
import org.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author glgay
 */
public class DataSet {
    
    private static final Logger log = LoggerFactory.getLogger(DataSet.class);
    
    private final String          name;
    private final String          desc;
    private final String          oid;
    private final long            id;
    private final long            userId;
    private final String          srcOid;
    private final String          srcName;
    private final long            srcId;
    private final String          index;
    private final boolean         publicRead;
    private final boolean         publicWrite;
    private final long            createTime;
    private final long            rowCount;
    private final List<DataField> fields;
    
    private boolean checked  = false;
    
    private DataSet(JSONObject jsSet) {
        
        // We aren't doing any sanity checks here - assuming we get good stuff
        name        = jsSet.optString( "set.name",         "");
        desc        = jsSet.optString( "set.description",  "");
        oid         = jsSet.optString( "set.oid",          "");
        id          = jsSet.optLong(   "set.id",           0L);
        userId      = jsSet.optLong(   "set.userId",       0L);
        srcOid      = jsSet.optString( "set.source_oid",   "");
        srcId       = jsSet.optLong(   "set.source_id",    0L);
        srcName     = jsSet.optString( "src.name",         "");
        index       = jsSet.optString( "set.index",        "");
        publicRead  = jsSet.optBoolean("set.public_read",  false);
        publicWrite = jsSet.optBoolean("set.public_write", false);
        createTime  = jsSet.optLong(   "set.create_time",  0L);
        rowCount    = jsSet.optLong(   "count",            0L);
        fields      = new ArrayList<>();
    }
    
    public void parseSchema(JSONObject inObj) {
        fields.clear();
        fields.addAll(DataField.parse(inObj));
    }

    public String          getName()        { return name; }
    public String          getDescription() { return desc; }
    public String          getOid()         { return oid; }
    public long            getId()          { return id; }
    public long            getUserId()      { return userId; }
    public String          getSrcOid()      { return srcOid; }
    public long            getSrcId()       { return srcId; }
    public String          getSrcName()     { return srcName; }
    public String          getIndex()       { return index; }
    public boolean         isPublicRead()   { return publicRead; }
    public boolean         isPublicWrite()  { return publicWrite; }
    public long            getCreateTime()  { return createTime; }
    public List<DataField> getFields()      { return Collections.unmodifiableList(fields); }
    public boolean         isChecked()      { return checked; }
    public long            getRowCount()    { return rowCount; }
    
    public void setChecked(boolean checked) {
        this.checked = checked;
    }
    
    public static DataSet parseSingle(JSONObject inObj) {
        DataSet result = null;
        try {
            
            if (inObj == null) {
                return result;
            }
            
            result = new DataSet(inObj);
            // Check that we have certain fields
            if (StringUtils.isBlank(result.oid)) {
                return null;
            }
            if (StringUtils.isBlank(result.name)) {
                return null;
            }
            if (StringUtils.isBlank(result.srcOid)) {
                return null;
            }
            if (StringUtils.isBlank(result.srcName)) {
                return null;
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public static List<DataSet> parse(JSONObject inObj) {
        List<DataSet> result = new ArrayList<>();
        try {
            
            if (inObj == null) {
                return result;
            }
            
            JSONArray jsSets = inObj.optJSONArray("data_sets");
            if (jsSets == null) {
                return result;
            }
            
            int len = jsSets.length();
            for (int x = 0; x < len; ++x) {
                JSONObject jsSet = jsSets.optJSONObject(x);
                if (jsSet != null) {
                    DataSet set = new DataSet(jsSet);
                    result.add(set);
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
}
