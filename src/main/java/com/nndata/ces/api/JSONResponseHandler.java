/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.api;

import org.apache.commons.lang3.StringUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;

import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author glgay
 */
public class JSONResponseHandler implements ResponseHandler<JSONObject> {
    
    private static final Logger log = LoggerFactory.getLogger(JSONResponseHandler.class);
    
    private int    lastStatus  = 0;
    private String lastError   = "";
    private String lastMessage = "";
    
    @Override
    public JSONObject handleResponse(final HttpResponse response) {
        JSONObject result = null;
        try {
            HttpEntity entity = response.getEntity();
            
            lastStatus = response.getStatusLine().getStatusCode();
            lastMessage = response.getStatusLine().getReasonPhrase();
            
            if (lastMessage == null) {
                lastMessage = "";
            }
            
            String txt = null;
            if (entity != null) {
                if ((lastStatus >= 200) && (lastStatus < 300)) {
                    txt = EntityUtils.toString(entity);
                } else {
                    String err = EntityUtils.toString(entity);
                    if (!StringUtils.isBlank(err)) {
                        lastError = err;
                        lastMessage += " - " + err;
                    }
                }
                EntityUtils.consume(entity);
            }
            if (txt != null) {
                result = new JSONObject(txt);
            }
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public int getLastStatus() {
        return lastStatus;
    }
    
    public String getLastMessage() {
        if (lastMessage == null) {
            return "";
        } else {
            return lastMessage;
        }
    }
    
    public String getLastError() {
        return lastError;
    }
    
}
