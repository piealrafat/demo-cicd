/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.api;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.Date;

import java.net.URI;

import javax.servlet.ServletContext;

import org.apache.commons.lang3.StringUtils;

import org.apache.http.HttpEntity;

import org.apache.http.client.config.RequestConfig;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpUriRequest;

import org.apache.http.client.utils.URIBuilder;

import org.apache.http.conn.ssl.NoopHostnameVerifier;

import org.apache.http.entity.StringEntity;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import org.apache.http.ssl.SSLContextBuilder;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.json.JSONObject;
import org.json.JSONArray;

import com.nndata.ces.AppListener;
import com.nndata.ces.UIConfig;
import com.nndata.ces.auth.CESPrincipal;

/**
 *
 * @author glgay
 */
public class RestAPI {
    
    private static final Logger log       = LoggerFactory.getLogger(RestAPI.class);
    private static final String TOKEN_KEY = "com.nndata.ces.rest.token";
    
    private static final String CT_HEADER = "Content-Type";
    private static final String MIME_JSON = "application/json";
    
    private String  tokenUrl;
    private String  sourcesUrl;
    private String  setsUrl;
    private String  schemaUrl;
    private String  typesUrl;
    private String  countsUrl;
    private String  cardUrl;
    private String  timeUrl;
    private String  searchUrl;
    private String  bulkUrl;
    
    // CES Plugin End Points
    private String  cesRoleUrl;
    private String  cesPrefsUrl;
    private String  cesTimeUrl;
    private String  cesGeoUrl;
    private boolean hasUserPass;
    
    private String username = null;
    private String password = null;
    
    private String myToken = null;
    
    public RestAPI(String baseUrl, String username, String password) {
        init(baseUrl, username, password);
    }
    
    public RestAPI(String baseUrl) {
        init(baseUrl, null, null);
    }
    
    public RestAPI(ServletContext ctx) throws InvalidPrincipalException {
        
        Subject sbj = SecurityUtils.getSubject();
        Object  o   = sbj.getPrincipal();
        
        if (!(o instanceof CESPrincipal)) {
            log.error("Incorrect principal type: " + o.getClass().getCanonicalName());
            throw new InvalidPrincipalException();
        }
        
        CESPrincipal p = (CESPrincipal)o;
        
        String nvUrl = (String)ctx.getAttribute(AppListener.NV_URL_KEY);
        init(nvUrl, p.getName(), p.getPassword());
    }
    
    private void init(String baseUrl, String username, String password) {
        
        this.username = username;
        this.password = password;
        
        if (!baseUrl.endsWith("/")) {
            baseUrl += "/";
        }
        
        tokenUrl    = baseUrl + "access/token";
        sourcesUrl  = baseUrl + "sources";
        schemaUrl   = baseUrl + "schema";
        typesUrl    = baseUrl + "stats/types";
        countsUrl   = baseUrl + "stats/rows";
        cardUrl     = baseUrl + "stats/fields";
        timeUrl     = baseUrl + "stats/time";
        searchUrl   = baseUrl + "search";
        setsUrl     = baseUrl + "datasets";
        bulkUrl     = baseUrl + "bulk";
        
        cesRoleUrl  = baseUrl + "plugins/ces/user/roles";
        cesPrefsUrl = baseUrl + "plugins/ces/user/prefs";
        cesTimeUrl  = baseUrl + "plugins/ces/time";
        cesGeoUrl   = baseUrl + "plugins/ces/geo";
        
        hasUserPass = !StringUtils.isBlank(username) && !StringUtils.isBlank(password);
    }
    
    private static CloseableHttpClient createClient() throws Exception {
        
        RequestConfig.Builder requestBuilder = RequestConfig.custom();
        
        requestBuilder = requestBuilder.setConnectTimeout(10000);
        requestBuilder = requestBuilder.setConnectionRequestTimeout(10000);
        
        HttpClientBuilder builder = HttpClientBuilder.create();     
        builder.setDefaultRequestConfig(requestBuilder.build());
        builder.setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE);
        builder.setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, TrustAllStrategy.INSTANCE).build());
        
        return builder.build();
    }
    
    public String requestToken() {
        
        String token = "";
        
        try (CloseableHttpClient client = createClient()) {
            
            URIBuilder builder = new URIBuilder(tokenUrl);
            
            if (hasUserPass) {
                
                builder.addParameter("username", username);
                builder.addParameter("password", password);
                
            } else {
                
                Subject sbj = SecurityUtils.getSubject();
                Object  o   = sbj.getPrincipal();
                
                if ((o == null) || !(o instanceof CESPrincipal)) {
                    return token;
                }
                
                CESPrincipal p = (CESPrincipal)o;
                
                builder.addParameter("username", p.getName());
                builder.addParameter("password", p.getPassword());
                
            }
            
            HttpPost poster = new HttpPost(builder.build());
            
            StringResponseHandler srh = new StringResponseHandler();
            String tmpToken = client.execute(poster, srh);
            
            int code = srh.getLastStatus();
            
            if ((code < 200) || (code > 299)) {
                log.error("REST Token Request failed: " + code + ": " + srh.getLastMessage());
            } else {
                token = tmpToken;
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        return token;
    }
    
    private String newToken() {
        
        String token = null;
        
        try {
            
            token = requestToken();
            
            if (hasUserPass) {
                myToken = token;
            } else {
                Session session = SecurityUtils.getSubject().getSession();
                session.setAttribute(TOKEN_KEY, token);
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        return token;
    }
    
    private String checkToken() {
        
        String token = null;
        
        try {
            
            if (hasUserPass) {
                token = myToken;
            } else {
                Session session = SecurityUtils.getSubject().getSession();
                token = (String)session.getAttribute(TOKEN_KEY);
            }
            
            if (StringUtils.isBlank(token)) {
                token = newToken();
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        return token;
    }
    
    public JSONObject secureRequest(HttpUriRequest request) {
        
        JSONObject result = null;
        
        try (CloseableHttpClient client = createClient()) {
            
            String token = checkToken();
            
            request.removeHeaders("Authorization");
            request.addHeader("Authorization", "Bearer " + token);
            
            JSONResponseHandler jrh = new JSONResponseHandler();
            result = client.execute(request, jrh);
            
            int code = jrh.getLastStatus();
            if (code == 401) {
                // Token probably expired, try again...
                token = newToken();
                
                request.removeHeaders("Authorization");
                request.addHeader("Authorization", "Bearer " + token);
                
                result = client.execute(request, jrh);
                code = jrh.getLastStatus();
            }
            
            if ((code < 200) || (code > 299)) {
                String url = request.getURI().toString();
                log.error("REST API request failed (" + url + "), " + code + ": " + jrh.getLastMessage());
                result = null;
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
    public List<DataField> getSchema(String setOid) {
        List<DataField> result = new ArrayList<>();
        try {
            
            HttpGet getter = new HttpGet(schemaUrl + "/" + setOid);
            
            JSONObject jsSchema = secureRequest(getter);
            
            result.addAll(DataField.parse(jsSchema));
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public List<DataSource> getSources() {
        List<DataSource> result = new ArrayList<>();
        try {
            
            HttpGet getter = new HttpGet(sourcesUrl);
            
            JSONObject jsSources = secureRequest(getter);
            
            result.addAll(DataSource.parse(jsSources));
            
            for (DataSource ds : result) {
                List<DataSet> sets = ds.getDataSets();
                for (DataSet set : sets) {
                    
                    String setOid = set.getOid();
                    getter = new HttpGet(schemaUrl + "/" + setOid);
                    JSONObject jsSchema = secureRequest(getter);
                    set.parseSchema(jsSchema);
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public JSONObject genericJSONRequest(JSONObject nvQuery, String url) {
        JSONObject result = null;
        try {
            
            HttpPost   poster = new HttpPost(url);
            HttpEntity ent    = new StringEntity(nvQuery.toString(), "UTF-8");
            
            poster.addHeader(CT_HEADER, MIME_JSON);
            poster.setEntity(ent);
            
            result = secureRequest(poster);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        // Return a blank object on failure
        if (result == null) {
            result = new JSONObject();
        }
        
        return result;
    }
    
    public JSONObject getTypeStats(JSONObject nvQuery) {
        return genericJSONRequest(nvQuery, typesUrl);
    }
    
    public JSONObject getTimeStats(JSONObject nvQuery) {
        return genericJSONRequest(nvQuery, timeUrl);
    }
    
    public long getTotalRows(JSONObject nvQuery) {
        long result = 0;
        try {
            
            JSONObject counts = genericJSONRequest(nvQuery, countsUrl);
            
            result = counts.optLong("count", 0);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
    public JSONObject getRowCounts(JSONObject nvQuery) {
        return genericJSONRequest(nvQuery, countsUrl);
    }
    
    public JSONObject getFieldStats(JSONObject nvQuery) {
        return genericJSONRequest(nvQuery, cardUrl);
    }
    
    public JSONObject getData(JSONObject nvQuery) {
        return genericJSONRequest(nvQuery, searchUrl);
    }
    
    public JSONObject createSource(JSONObject srcConfig) {
        return genericJSONRequest(srcConfig, sourcesUrl);
    }
    
    public JSONObject deleteSource(String oid) {
        JSONObject result = null;
        try {
            
            String delUrl = sourcesUrl + "/" + oid;
            
            HttpDelete deleter = new HttpDelete(delUrl);
            result = secureRequest(deleter);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public JSONObject deleteDataSet(String oid) {
        JSONObject result = null;
        try {
            
            String delUrl = setsUrl + "/" + oid;
            
            HttpDelete deleter = new HttpDelete(delUrl);
            result = secureRequest(deleter);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;        
    }
    
    public JSONObject getBulk(String setOid, long fromTime, boolean includeBinary, int batchSize) {
        JSONObject result = null;
        try {
            
            if (StringUtils.isBlank(setOid)) {
                return null;
            }
            
            URIBuilder builder = new URIBuilder(bulkUrl + "/" + setOid);
            builder.addParameter("binary", Boolean.toString(includeBinary));
            
            if (batchSize > 0) {
                builder.addParameter("size", Integer.toString(batchSize));
            }
            if (fromTime > 0) {
                builder.addParameter("fromTime", Long.toString(fromTime));
            }
            
            HttpGet getter = new HttpGet(builder.build());
            result = secureRequest(getter);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public Set<String> getUserRoles(String username) {
        Set<String> result = new TreeSet<>();
        try {
            
            URI        roleUri = new URIBuilder(cesRoleUrl).addParameter("username", username).build();
            HttpGet    getter  = new HttpGet(roleUri);
            JSONObject nvUser  = secureRequest(getter);
            
            if (nvUser != null) {
                JSONArray nvRoles = nvUser.optJSONArray("roles");
                if (nvRoles != null) {
                    int len = nvRoles.length();
                    for (int x = 0; x < len; ++x) {
                        String str = nvRoles.optString(x);
                        if (!StringUtils.isBlank(str)) {
                            result.add(str);
                        }
                    }
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public JSONObject getUserPrefs(String username) {
        
        JSONObject result = null;
        
        try {
            
            URI        prefUri = new URIBuilder(cesPrefsUrl).addParameter("username", username).build();
            HttpGet    getter  = new HttpGet(prefUri);
            JSONObject nvUser  = secureRequest(getter);
            
            if (nvUser != null) {
                result = nvUser.optJSONObject("prefs");
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        if (result == null) {
            result = new JSONObject();
        }
        
        return result;
    }
    
    public boolean saveUserPrefs(String username, JSONObject prefs) {
        
        boolean worked = false;
        
        try {
            
            URI        prefUri = new URIBuilder(cesPrefsUrl).addParameter("username", username).build();
            HttpPut    putter  = new HttpPut(prefUri);
            HttpEntity ent     = new StringEntity(prefs.toString(), "UTF-8");
            
            putter.setHeader(CT_HEADER, MIME_JSON);
            putter.setEntity(ent);
            
            JSONObject rsp = secureRequest(putter);
            
            if (rsp != null) {
                String status = rsp.optString("status", "");
                worked = status.equals("success");
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        return worked;
    }
    
    public JSONObject getCesTimeFields() {
        JSONObject result = null;
        try {
            
            HttpGet getter = new HttpGet(cesTimeUrl + "/fields");
            result = secureRequest(getter);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    // Mode can currently be one of: year, month, day, hour
    public JSONObject getCesTimeCounts(String setOid, String field, String mode, Date fromTime, Date toTime, String timeZone) {
        JSONObject result = null;
        try {
            
            URIBuilder builder = new URIBuilder(cesTimeUrl + "/counts");
            
            builder.addParameter("set",   setOid);
            builder.addParameter("field", field);
            builder.addParameter("type",   mode);
            
            if (!StringUtils.isBlank(timeZone)) {
                builder.addParameter("tz", timeZone);
            }
            
            if (fromTime != null) {
                builder.addParameter("from", Long.toString(fromTime.getTime()));
            }
            
            if (toTime != null) {
                builder.addParameter("to", Long.toString(toTime.getTime()));
            }
            
            HttpGet getter = new HttpGet(builder.build());
            
            result = secureRequest(getter);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    // Get the user's timezone out of the session
    public JSONObject getCesTimeCounts(String setOid, String field, String mode, Date fromTime, Date toTime) {
        JSONObject result = null;
        try {
            
            Subject sbj  = SecurityUtils.getSubject();
            Session sess = sbj.getSession();
            String  tz   = (String)sess.getAttribute(UIConfig.USER_TIME_ZONE);
            
            result = getCesTimeCounts(setOid, field, mode, fromTime, toTime, tz);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public JSONObject getCesGeoFields() {
        JSONObject result = null;
        try {
            
            HttpGet getter = new HttpGet(cesGeoUrl + "/fields");
            result = secureRequest(getter);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
    public JSONObject getCesGeoCounts(String setOid, String field, double topX, double topY, double bottomX, double bottomY) {
        
        JSONObject result = null;
        
        try {
            
            URIBuilder builder = new URIBuilder(cesGeoUrl + "/counts");
            
            builder.addParameter("set",     setOid);
            builder.addParameter("field",   field);
            builder.addParameter("topx",    Double.toString(topX));
            builder.addParameter("topy",    Double.toString(topY));
            builder.addParameter("bottomx", Double.toString(bottomX));
            builder.addParameter("bottomy", Double.toString(bottomY));
            
            HttpGet getter = new HttpGet(builder.build());
            
            result = secureRequest(getter);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        
        return result;
    }
    
}
