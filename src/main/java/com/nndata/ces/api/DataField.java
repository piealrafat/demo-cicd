/**
 * Copyright 2020, NNData Corporation
 * 
 * This source code may not be copied, modified, distributed, or used in
 * derivative works, in whole or in part, without the express written permission
 * of NNData Corporation.
 */
package com.nndata.ces.api;

import java.util.List;
import java.util.ArrayList;

import org.apache.commons.lang3.text.WordUtils;

import org.json.JSONObject;
import org.json.JSONArray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author glgay
 */
public class DataField {
    
    private static final Logger log = LoggerFactory.getLogger(DataField.class);
    
    private final String fullName;
    private final String prettyName;
    private final String type;
    
    private DataField(JSONObject inObj) {
        fullName   = inObj.optString("name", "");
        prettyName = makePrettyName(fullName);
        type       = inObj.optString("type", "");
    }
    
    public DataField(String compact) {
        int idx    = compact.lastIndexOf("/");
        fullName   = compact.substring(0, idx);
        type       = compact.substring(idx + 1);
        prettyName = makePrettyName(fullName);
    }
    
    public String getFullName()   { return fullName; }
    public String getPrettyName() { return prettyName; }
    public String getType()       { return type; }
    
    public static String makePrettyName(String key) {
        String pretty = key;
        try {
            
            int idx = key.lastIndexOf(".");
            if (idx != -1) {
                key = key.substring(idx + 1);
            }
            
            key = key.replace("_", " ");
            pretty = WordUtils.capitalize(key);
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return pretty;
    }
    
    public static List<DataField> parse(JSONObject inObj) {
        List<DataField> result = new ArrayList<>();
        try {
            
            if (inObj == null) {
                return result;
            }
            
            JSONArray schema = inObj.optJSONArray("schema");
            if (schema == null) {
                return result;
            }
            
            int len = schema.length();
            for (int x = 0; x < len; ++x) {
                JSONObject jsField = schema.optJSONObject(x);
                if (jsField != null) {
                    DataField df = new DataField(jsField);
                    result.add(df);
                }
            }
            
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }
        return result;
    }
    
}
