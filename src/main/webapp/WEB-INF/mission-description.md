# Shadow Kick Vignette

This vignette covers an attack on a defended facility where a group of insurgents will covertly plant an explosive IVO a communications facility and a separate observer will remotely detonate the explosive while broadcasting the event through social media.  The vignette is played out over 11 sequences across six areas of interest:

## Areas Of Interest

- Area-1: a defended area containing a high-value communications facility
- Area-2: a rally point for the Observer
- Area-3: a rally point for the Covert team
- Area-4: the observation point
- Area-5: the explosives staging area
- Area-6: the Covert team egress route
- Area-7: Base security location with operations station manned by security operator; Composability station as well
- Area-8: Security platform operations/staging area for airframes and vehicles
- Area-9: Base AOR as defined region wherein airframe patrols of sensor feeds are gathered


## Sequence

- Seq 1: A facility in Area-1 is patrolled on-foot by one security officer.  The facility uses several radio feeds; the security officer has a cell phone and radio. Airframe(s) patrol the Base AOR.  Airframe(s) collect and store sensor data from patrols to aid in building patterns of life and boundary norms for anomaly determination over time.
- Seq 2: The Observer is staged at Area-2 on-foot with an active cell phone and remote detonator.
- Seq 3: The Observer transitions to Area-4 and begins posting to social media.
- Seq 4: The Covert team is staged at Area-3 in a vehicle with three operatives and an explosive.  The two operatives have cell phones.
- Seq 5: The Covert team transitions to Area-5 by car and unloads the explosive for placement.
- Seq 6: The security officer moves to a position in Area-1 that creates a blindspot for the Covert team.
- Seq 7: Two operatives of the Covert team move on foot from Area-5 into the Area-1 blindspot, place the explosive, and arm it.
- Seq 8: The two operatives redeploy from Area-1 to Area-5 on foot.  An operator signals the Observer that the device is set.
- Seq 9: The Observer remotely detonates the device (via radio), heavily damaging the facility.  Several videos of the explosion are posted and circulate online. Detonation identified by patrol airframes and generates an alert to base station (and initiates automated disaster recovery ISR airframe mission to view damage and identify if entities remain in the damage area for determining disaster recovery actions).
- Seq 10: The Covert team hastily departs Area-5 by vehicle away from the AOR.
- Seq 11: The Observer hastily escapes the area towards Area-2 leaving the detonator at Area-4.

## Analytics:

### SPoTTER
Subject Position Timeseries Tracking and Entity Recognition (SPoTTER) is an analytic module for entity detection from signal characteristics.  Using attribute GLoVE Vector embedding and vector comparison, this analytic identifies and tracks entities and their attributes, deduplicates conflicting signals from fused sensor data, and tracks objects and attributes that may be shared across entities (e.g. if two people are observed with radios on the same frequency) from any sensor platform in the computing cluster.



### ARCHER
Agglomerative hierRCHical Entity clusteRing (ARCHER) is an analytic module for entity clustering according to positional space, movement patterns, or observed attributes of identified entities.  Allows for the establishment of connections between entities and tracking of groups, subgroups within groups, and their behaviors.


### RANGE
Real time Ad-hoc Network Graphing for Entities (RANGE) is an analytic for assembling observed entity data into a network graph for recording entity attributes and relationships between entities.  Combines entity and attribute tracking from SPoTTER and (optionally) ARCHER to track entity attributes/connections.  Primarily used to fuse observations and analytics for ingestion by other analytics.

### TETSU
acTivity and EnTity SUperposition (TETSU) creates heatmaps of entity positions and activity as distributed in observations of entities.  TETSU highlights areas of anomalously high or low activity in current observations (comparing heatmaps of movement vs. heatmaps of positions) relative to entity positions over time.  Tetsu can also be configured to handle custom areas/polygons for heatmap generation to allow users to customize the way that movement distributions are compared.  For example, Tetsu can be configured to compare incidence of attribute observations along particular roads, compare movement patterns between different geographic regions, and much more.


### SOBA
Sensor and OBserver displAy (SOBA) provides updates on location, status, range, and sensor coverage for each sensor on each observation platform in a SENSEI mission.  SOBA can optionally be configured to create polygons that represent the areas observed for each sensor on each platform.  These polygons can either be used in visualizations or passed to other analytics.
