<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page import="com.nndata.ces.UIConfig" %>
<%
    UIConfig uiConfig = new UIConfig(pageContext.getServletContext());
    pageContext.setAttribute("uiHome",   uiConfig.getUiHome());
    
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Global stylesheets -->
        <link href="${uiHome}/assets/css/fonts/Roboto/styles.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="${uiHome}/assets/css/visjs/vis-timeline-graph2d.min.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/bootstrap_ces.min.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/ces_custom.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/components_ces.min.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/misc.css" rel="stylesheet" type="text/css">
        <link rel="apple-touch-icon" sizes="57x57" href="${uiHome}/assets/images/ico/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="${uiHome}/assets/images/ico/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="${uiHome}/assets/images/ico/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="${uiHome}/assets/images/ico/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="${uiHome}/assets/images/ico/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="${uiHome}/assets/images/ico/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="${uiHome}/assets/images/ico/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="${uiHome}/assets/images/ico/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="${uiHome}/assets/images/ico/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="${uiHome}/assets/images/ico/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="${uiHome}/assets/images/ico/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="${uiHome}/assets/images/ico/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="${uiHome}/assets/images/ico/favicon-16x16.png">
        <link rel="manifest" href="${uiHome}/assets/images/ico/manifest.json">
        <meta name="msapplication-TileColor" content="#9c9d9d">
        <meta name="msapplication-TileImage" content="${uiHome}/assets/images/ico/ms-icon-144x144.png">
        <meta name="theme-color" content="#eb6001">
        <!-- /global stylesheets -->
        <!-- Core JS files -->
        <script src="${uiHome}/assets/js/main/jquery.min.js"></script>
        <script src="${uiHome}/assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="${uiHome}/assets/js/plugins/loaders/blockui.min.js"></script>
        <script src="${uiHome}/assets/js/plugins/ui/moment/moment.min.js"></script>
        <script src="${uiHome}/assets/js/plugins/ui/moment/moment_locales.min.js"></script>
        <script src="${uiHome}/assets/js/plugins/visjs/vis-data.min.js"></script>
        <script src="${uiHome}/assets/js/plugins/visjs/vis-timeline-graph2d.min.js"></script>
		<script src="${uiHome}/assets/js/plugins/forms/selects/select2.min.js"></script>
		<script src="${uiHome}/assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
		<script src="${uiHome}/assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script src="${uiHome}/assets/js/includes/timeline.js"></script>
        <!-- /core JS files -->
        <script src="${uiHome}/assets/js/plugins/notifications/sweetalert.min.js"></script>
    </head>
    <body class="appHeight">
        <div id="composable-ui-home" style="display:none;">${uiHome}</div>
        <div class="content-wrapper">
            <!-- Content area -->
            <div class="content p-0">
				  <div class="card-header bg-info header-elements-inline" style="padding:8px;">
                      <h6 class="card-title grid-item-title">Mission Timeline</h6>
                      <!-- <div class="header-elements">
                          <span class="mr-3"><i class="icon-filter4 mr-2"></i> Asset Filter:</span>
						  
						  <form action="#">
                              <div class="wmin-sm-200">
                                  <select class="form-control form-control-multiselect" multiple="multiple" data-fouc>
                                      <optgroup label="Cluster 1">
                                        <option value="cheese">Asset 1</option>
                                        <option value="tomatoes">Asset 2</option>
									  </optgroup>
									  <optgroup label="Cluster 2">
                                        <option value="cheese">Asset 1</option>
                                        <option value="tomatoes">Asset 2</option>
									  </optgroup>
									  <optgroup label="Cluster 3">
                                        <option value="cheese">Asset 1</option>
                                        <option value="tomatoes">Asset 2</option>
									  </optgroup>
                                  </select>
                              </div>
                          </form>
                      </div> -->
                  </div>
                  <div id="visualization"></div>
                </div>
            </div>
            <!-- /content area -->
        </div>
    </body>
</html>