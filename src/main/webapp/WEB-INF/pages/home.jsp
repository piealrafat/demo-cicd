<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.nndata.ces.UIConfig" %>
<%

	UIConfig uiConfig = new UIConfig(pageContext.getServletContext());
	String   startPage = uiConfig.getStartPage();
	
	if (startPage == null) {
	    startPage = "/map";
	} else {
	    startPage = "/" + startPage;
	}
	String redirectURL = uiConfig.getUiHome() + startPage;
	response.sendRedirect(redirectURL);

%>