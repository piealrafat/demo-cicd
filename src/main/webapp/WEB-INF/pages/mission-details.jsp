<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page import="java.util.Properties" %>
<%@ page import="com.nndata.ces.UIConfig" %>
<%
    UIConfig uiConfig = new UIConfig(pageContext.getServletContext());

    pageContext.setAttribute("uiHome",   uiConfig.getUiHome());
    pageContext.setAttribute("userName", uiConfig.getUserName());
		pageContext.setAttribute("markdownHTML", uiConfig.getMissionMarkdownHTML(pageContext.getServletContext()));

%>
<div class="mission-markdown">${markdownHTML}</div>