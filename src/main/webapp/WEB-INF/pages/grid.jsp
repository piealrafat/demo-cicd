<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page trimDirectiveWhitespaces="true" %>
<%@ page import="java.util.Properties" %>
<%@ page import="com.nndata.ces.UIConfig" %>
<%@ page import="com.nndata.ces.Widgets" %>
<%
	UIConfig uiConfig   = new UIConfig(pageContext.getServletContext());
	Widgets  allWidgets = Widgets.loadAll(request);
	Widgets  active     = uiConfig.getActiveWidgets();

	pageContext.setAttribute("uiHome",   uiConfig.getUiHome());
	pageContext.setAttribute("userName", uiConfig.getUserName());
	pageContext.setAttribute("widgets",  allWidgets);
	pageContext.setAttribute("active",   active.getAll());

%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Global stylesheets -->
		<link href="${uiHome}/assets/css/fonts/Roboto/styles.css" rel="stylesheet" type="text/css">
		<link href="${uiHome}/assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
		<link href="${uiHome}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="${uiHome}/assets/css/bootstrap_ces.min.css" rel="stylesheet" type="text/css">
		<link href="${uiHome}/assets/css/ces_custom.css" rel="stylesheet" type="text/css">
		<link href="${uiHome}/assets/css/layout.min.css" rel="stylesheet" type="text/css">
		<link href="${uiHome}/assets/css/components_ces.min.css" rel="stylesheet" type="text/css">
		<link href="${uiHome}/assets/css/colors.min.css" rel="stylesheet" type="text/css">
		<link href="${uiHome}/assets/css/video-js/video-js.min.css" rel="stylesheet" type="text/css">
		<link href="${uiHome}/assets/css/misc.css" rel="stylesheet" type="text/css">
		<link rel="apple-touch-icon" sizes="57x57" href="${uiHome}/assets/images/ico/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="${uiHome}/assets/images/ico/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="${uiHome}/assets/images/ico/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="${uiHome}/assets/images/ico/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="${uiHome}/assets/images/ico/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="${uiHome}/assets/images/ico/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="${uiHome}/assets/images/ico/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="${uiHome}/assets/images/ico/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="${uiHome}/assets/images/ico/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="${uiHome}/assets/images/ico/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="${uiHome}/assets/images/ico/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="${uiHome}/assets/images/ico/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="${uiHome}/assets/images/ico/favicon-16x16.png">
		<link rel="manifest" href="${uiHome}/assets/images/ico/manifest.json">
		<meta name="msapplication-TileColor" content="#9c9d9d">
		<meta name="msapplication-TileImage" content="${uiHome}/assets/images/ico/ms-icon-144x144.png">
		<meta name="theme-color" content="#eb6001">
		<!-- /global stylesheets -->
		<!-- Core JS files -->
		<script src="${uiHome}/assets/js/main/jquery.min.js"></script>
		<script src="${uiHome}/assets/js/main/bootstrap.bundle.min.js"></script>
		<script src="${uiHome}/assets/js/plugins/loaders/blockui.min.js"></script>
		<script src="${uiHome}/assets/js/plugins/video-js/video.min.js"></script>
		<!-- /core JS files -->
		<!-- App JS files -->
		<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>-->
		<script type="text/javascript" src="${uiHome}/assets/js/plugins/forms/styling/uniform.min.js"></script>
		<script src="${uiHome}/assets/js/plugins/notifications/sweetalert.min.js"></script>
		<script type="text/javascript" src="${uiHome}/assets/js/plugins/ui/prism.min.js"></script>
		<script type="text/javascript" src="${uiHome}/assets/js/plugins/split/split.min.js"></script>
		<script type="text/javascript" src="${uiHome}/assets/js/app.js"></script>
		<script type="text/javascript" src="${uiHome}/assets/js/includes/navbar.js"></script>
		<script type="text/javascript" src="${uiHome}/assets/js/includes/ces_sidebar.js"></script>
		<script type="text/javascript" src="${uiHome}/assets/js/includes/grid.js"></script>
		<link href="${uiHome}/assets/css/controller.css" rel="stylesheet" type="text/css">
	<link href="${uiHome}/assets/css/controller-full.css" rel="stylesheet" type="text/css">
	</head>
	<body class="appHeight sidebar-secondary-hidden">
		<!-- Main navbar -->
		<jsp:include page="/WEB-INF/pages/global/navbar.jsp">
		    <jsp:param name="page" value="grid" />
		</jsp:include>
		<!-- /main navbar -->
		<!-- Page content -->
		<div class="page-content">
		    <div id="ui-current-page" style="display:none;">grid</div>
		    <div id="composable-ui-home" style="display:none;">${uiHome}</div>
			<!-- Secondary sidebar -->
			<div class="sidebar sidebar-light sidebar-secondary sidebar-expand-md">
				<!-- Sidebar mobile toggler -->
				<div class="sidebar-mobile-toggler text-center">
					<a href="#" class="sidebar-mobile-secondary-toggle">
					<i class="icon-arrow-left8"></i>
					</a>
					<span class="font-weight-semibold">Grid Selector</span>
					<a href="#" class="sidebar-mobile-expand">
					<i class="icon-screen-full"></i>
					<i class="icon-screen-normal"></i>
					</a>
				</div>
				<!-- /sidebar mobile toggler -->
				<!-- Sidebar content -->
				<div class="sidebar-content">
					<form action="#" class="pl-2 pr-2 pt-2">
                        <div class="form-group-feedback form-group-feedback-right">
                            <input id="grid-filter-widgets" type="search" class="form-control" placeholder="Filter Widgets">
                            <div class="form-control-feedback">
                                <i class="icon-search4 font-size-base text-muted"></i>
                            </div>
                        </div>
                    </form>
					<!-- /widget filter -->
					<c:forEach items="${widgets.groups}" var="group">
						<div class="card mt-2">
							<div class="card-header bg-transparent border-bottom border-bottom-light header-elements-inline">
								<span class="text-uppercase font-size-sm font-weight-semibold"><c:out value="${group}"/></span>
								<div class="header-elements">
									<div class="list-icons">
										<a class="list-icons-item" data-action="collapse"></a>
									</div>
								</div>
							</div>
							<ul class="media-list media-list-linked">
								<c:forEach items="${widgets.getWidgetsByGroup(group)}" var="widget">
									<li class="sidebar-widget-li" style="cursor:pointer;">
									    <a class="grid-add-widget media" data-url="${widget.url}" data-builtin="${widget.builtin}" data-title="<c:out value="${widget.displayName}"/>">
										    <div class="mr-3">
											    <img src="${uiHome}/assets/images/ces_mc_logo_icon_light.png" width="40" height="32" class="rounded-circle" alt="">
										    </div>
										    <div class="media-body mt-1">
											    <span class="media-title font-weight-semibold"><c:out value="${widget.displayName}"/></span>
											    <%-- <div class="font-size-sm text-muted">Description if one exists</div> --%>
										    </div>
										    <%--  If we want bullets...
										    <div class="ml-3 align-self-center">
											    <span class="badge badge-mark bg-success border-success"></span>
										    </div>
										    --%>
										</a>
									</li>
								</c:forEach>

							</ul>
						</div>
					</c:forEach>
				</div>
				<!-- /sidebar content -->
			</div>
			<!-- /secondary sidebar -->
			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Content area -->
				<div class="content p-0">
					<div class="removeMargin">
						<div class="splitMain gridMain" id="main">
							<div id="topData">
								<ul class="row list-unstyled widget-list">
								    <%-- NOTE:  Make sure this matches the template at the bottom of the page --%>
								    <c:forEach items="${active}" var="widget">
								        <li class="col-md-4 grid-list-item-template">
					                        <div class="card">
						                        <div class="card-header bg-teal-400 header-elements-inline" style="padding:8px;">
							                        <h6 class="card-title grid-item-title"><c:out value="${widget.displayName}"/></h6>
							                        <div class="header-elements">
								                        <div class="list-icons">
									                        <a class="list-icons-item" data-action="collapse"></a>
									                        <a class="list-icons-item" data-action="fullscreen"></a>
									                        <a class="list-icons-item close" data-action="remove"></a>
								                        </div>
							                        </div>
						                        </div>
						                        <div class="card-body-div" style="height:315px">
						                            <iframe src="${widget.url}" height="100%" width="100%" class="border-0"></iframe>
						                        </div>
					                        </div>
				                        </li>
								    </c:forEach>
								</ul>
							</div>
							<div class="grid-bottom" id="bottomData" style="overflow:scroll;">
								<iframe src="${uiHome}/timeline" height="100%" width="100%" class="border-0"></iframe>
							</div>
						</div>
					</div>
				</div>
				<!-- /content area -->
			</div>
			<!-- /main content -->
		</div>
		<div id="grid-ui-home" style="display:none;"><c:out value="${uiHome}"/></div>
		<div id="grid-widget-template" style="display:none;">
		    <ul>
				<li class="col-md-4 grid-list-item-template">
					<div class="card">
						<div class="card-header bg-teal-400 header-elements-inline" style="padding:8px;">
							<h6 class="card-title grid-item-title">Custom Widget</h6>
							<div class="header-elements">
								<div class="list-icons">
									<a class="list-icons-item" data-action="collapse"></a>
									<a class="list-icons-item" data-action="fullscreen"></a>
									<a class="list-icons-item close" data-action="remove"></a>
								</div>
							</div>
						</div>
						<div class="card-body-div" style="height:315px;">
						</div>
					</div>
				</li>
			</ul>
		</div>
		<!-- HTML For Video JS
			<li class="col-md-4">
				<div class="card">
					<div class="card-header bg-teal-400 header-elements-inline">
						<h6 class="card-title">Custom Widget</h6>
						<div class="header-elements">
							<div class="list-icons">
								<a class="list-icons-item" data-action="collapse"></a>
								<a class="list-icons-item" data-action="fullscreen"></a>
								<a class="list-icons-item close" data-action="remove"></a>
							</div>
						</div>
					</div>
					<div class="card-body" style="height:315px">
						<video id="my_video_1" class="video-js vjs-default-skin" controls preload="auto" data-setup='{"playbackRates": [0.5, 1, 1.5, 2],"loopbutton": true, "fluid": true, "aspectRatio":"16:9"}'
						>
						    <source src="http://vjs.zencdn.net/v/oceans.mp4" type='video/mp4'>
						</video>
					</div>
					<div class="card-footer bg-light d-flex justify-content-between">
						<span class="opacity-75">Last Update: <strong>Fri Nov 13 12:19 PM</strong></span>
						<div>
							<div class="list-icons">
								<a href="#" class="list-icons-item"><i class="icon-play3"></i></a>
								<a href="#" class="list-icons-item"><i class="icon-pause"></i></a>
								<div class="dropdown">
									<a href="#" class="list-icons-item dropdown-toggle" data-toggle="dropdown"><i class="icon-stop"></i></a>
									<div class="dropdown-menu">
										<a href="#" class="dropdown-item">CES Action</a>
										<a href="#" class="dropdown-item">CES Action 2</a>
										<a href="#" class="dropdown-item">CES Action 3</a>
										<div class="dropdown-divider"></div>
										<a href="#" class="dropdown-item">CES Action 4</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</li>
		-->
		<div id="controller-config-refresh" class="modal fade controller-modal" data-backdrop="false">
				<div class="modal-dialog modal-sm">
						<div class="modal-content">
								<div class="modal-header">
										<span class="modal-title">Choose Refresh Interval</span>
										<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>
								<div class="modal-body">
										<div style="height:130px;overflow:auto;">
												<div class="form-group">
														<div class="form-check">
																<label class="form-check-label">
																		<input type="radio" name="controller-refresh-interval" class="form-input-styled" value="10000">
																		10 seconds
																</label>
														</div>
														<div class="form-check" style="margin-top:10px;">
																<label class="form-check-label">
																		<input type="radio" name="controller-refresh-interval" class="form-input-styled" value="30000">
																		30 seconds
																</label>
														</div>
														<div class="form-check" style="margin-top:10px;">
																<label class="form-check-label">
																		<input type="radio" name="controller-refresh-interval" class="form-input-styled" value="60000">
																		1 minute
																</label>
														</div>
														<div class="form-check" style="margin-top:10px;">
																<label class="form-check-label">
																		<input type="radio" name="controller-refresh-interval" class="form-input-styled" value="0">
																		Manual
																</label>
														</div>
												</div>
										</div>
										<div style="height:50px;padding-top:10px;">
												<div class="header-elements">
										<a class="btn btn-sm btn-outline-success controller-set-interval">OK</a>
										<a class="btn btn-sm btn-outline-warning" data-dismiss="modal">Cancel</a>
									</div>
										</div>
								</div>
						</div>
				</div>
		</div>
	</body>
</html>
