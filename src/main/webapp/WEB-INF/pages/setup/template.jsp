<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page import="com.nndata.ces.UIConfig" %>
<%

    UIConfig    uiConfig     = new UIConfig(pageContext.getServletContext());

    pageContext.setAttribute("uiHome",         uiConfig.getUiHome());
    pageContext.setAttribute("userName",       uiConfig.getUserName());

%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Mission from Template</title>
	<link href="${uiHome}/assets/css/fonts/Roboto/styles.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/bootstrap_ces.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/ces_custom.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/controller-full.css" rel="stylesheet" type="text/css">
	<!-- CES Core JS files -->
	<script src="${uiHome}/assets/js/main/jquery.min.js"></script>
	<script src="${uiHome}/assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="${uiHome}/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /CES core JS files -->

	<!-- CES Mission Controller JS files -->
	<script src="${uiHome}/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script src="${uiHome}/assets/js/plugins/notifications/sweetalert.min.js"></script>
	<script src="${uiHome}/assets/js/plugins/cookie/js.cookie.js"></script>

	<script src="${uiHome}/assets/js/app.js"></script>
	<script type="text/javascript" src="${uiHome}/assets/js/includes/navbar.js"></script>
  <script src="${uiHome}/assets/js/includes/controller.js"></script>
	<!-- /CES Mission Controller JS files -->

</head>

<body>
	<!-- Main navbar -->

	<!-- /main navbar -->

	<div class="page-content">

	    <div id="cluster-widget-mode" style="display:none;">main</div>
	    <div id="cluster-ui-home" style="display:none;"><c:out value="${uiHome}"/></div>
	    <div id="ui-current-page" style="display:none;">controller</div>
	    <div id="composable-ui-home" style="display:none;">${uiHome}</div>

		<!-- Main content -->
    <div class="content">


      <div class=" text-center mission-header">
      <strong>Let's select an existing Template!</strong>
      </div>

			<!-- Content area -->
      <div class="row list-unstyled widget-list">
        <div class="col-md-6 grid-list-item-template" >
          <div class="card mission-card" style="margin: 75px;">
            <div class="card-body-div  scratch-modal" style="height: 315px;padding: 20px 100px;">
              <div class="text-center">
                <div class="Lheader">
                  Start from scratch
                </div>
                <h3 style="padding-top: 20px;">You will answer a few questions about the mission and we will provide a few suggested templates to use.</h3>
              </div>


            </div>
          </div>
        </div>
        <div class="col-md-6 grid-list-item-template" >
          <div class="card mission-card" style="margin: 75px;">
            <div class="card-body-div" style="height: 315px;padding: 20px 100px;">
              <div class="text-center ">
                <div class="Lheader">
                  Choose existing template
                </div>
                <h3 style="padding-top: 20px;">We will provide you a list of a few of the most used templates, to get you started.</h3>
              </div>

            </div>
          </div>
        </div>
      </div>
			<!-- /content area -->

  </div>
		<!-- /main content -->
    <div id="start-from-scratch" class="modal fade scratch-modal" data-backdrop="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="modal-title">Start Mission from Scratch</span>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div style="height:130px;overflow:auto;">
                        <div class="form-group">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="10000">
                                    10 seconds
                                </label>
                            </div>
                            <div class="form-check" style="margin-top:10px;">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="30000">
                                    30 seconds
                                </label>
                            </div>
                            <div class="form-check" style="margin-top:10px;">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="60000">
                                    1 minute
                                </label>
                            </div>
                            <div class="form-check" style="margin-top:10px;">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="0">
                                    Manual
                                </label>
                            </div>
                        </div>
                    </div>
                    <div style="height:50px;padding-top:10px;">
                        <div class="header-elements">
                    <a class="btn btn-sm btn-outline-success controller-set-interval">OK</a>
                    <a class="btn btn-sm btn-outline-warning" data-dismiss="modal">Cancel</a>
                  </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


	</div>
	<!-- /page content -->

</body>
</html>
