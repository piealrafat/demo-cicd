<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page trimDirectiveWhitespaces="true" %>
<%@ page import="java.util.Properties" %>
<%@ page import="com.nndata.ces.UIConfig" %>
<%@ page import="com.nndata.ces.controller.ClusterData" %>

<%

    UIConfig    uiConfig = new UIConfig(pageContext.getServletContext());
    ClusterData cd       = new ClusterData();
    String      myPage   = request.getParameter("page");
    long        poll     = ClusterData.getPollInterval();
    boolean     running  = cd.anyMissionsRunning();

    if (myPage == null) {
        myPage = "";
    }

    pageContext.setAttribute("uiHome",         uiConfig.getUiHome());
    pageContext.setAttribute("userName",       uiConfig.getUserName());
    pageContext.setAttribute("myPage",         myPage);
    pageContext.setAttribute("pollInterval",   poll);
    pageContext.setAttribute("missionRunning", running);

%>

<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-light bg-body navbar-static">

    <div id="nb-params" style="display:none;">
        <div id="nb-poll-interval">${pollInterval}</div>
    </div>

	<div class="navbar-header sidebar-light d-none d-md-flex align-items-md-center">
		<div class="navbar-brand navbar-brand-md p-0">
			<a href="${uiHome}" class="d-inline-block">
				<img src="${uiHome}/assets/images/Sensei_Logo_Light.png" style="height:31px" alt="">
			</a>
		</div>
	</div>

	<div class="d-flex flex-1 d-md-none">
		<div class="navbar-brand mr-auto">
			<a class="d-inline-block"></a>
		</div>

		<button class="navbar-toggler sidebar-mobile-secondary-toggle" type="button">
			<i class="icon-more"></i>
		</button>
	</div>

	<!-- Navbar content -->
	<div class="collapse navbar-collapse" id="navbar-mobile">
		<ul class="navbar-nav">

			<li class="nav-item">
                <c:if test="${myPage == 'map'}">
                    <a href="#" class="navbar-nav-link sidebar-control sidebar-secondary-toggle d-none d-md-block" title="Map Controls" data-placement="bottom" data-container="body" data-trigger="hover">
                        <i class="icon-transmission"></i>
                    </a>
                </c:if>
                <c:if test="${myPage == 'grid'}">
                    <a href="#" class="navbar-nav-link sidebar-control sidebar-secondary-toggle d-none d-md-block" title="Grid Controls" data-placement="bottom" data-container="body" data-trigger="hover">
                        <i class="icon-transmission"></i>
                    </a>
                </c:if>
            <!--  commenting out until we decide what we're doing
            <c:if test="${myPage == 'mission'}">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-secondary-toggle d-none d-md-block" title="Mission Controls" data-placement="bottom" data-container="body" data-trigger="hover">
                    <i class="icon-transmission"></i>
                </a>
            </c:if>
            -->
			</li>
			<li class="nav-item dropdown">
                <a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
                    <i class="icon-alert"></i>
                    <span class="d-md-none ml-2">CES Alerts</span>
                    <span class="badge badge-pill bg-pink-700 ml-auto ml-md-0">5</span>
                </a>

                <div class="dropdown-menu dropdown-content wmin-md-350">
                    <div class="dropdown-content-header">
                        <span class="font-weight-semibold">CES Alerts</span>
                        <a href="#" class="text-default"><i class="icon-sync"></i></a>
                    </div>

                    <div class="dropdown-content-body dropdown-scrollable">
                        <ul class="media-list">
                            <li class="media">
                                <div class="mr-3">
                                    <a href="#" class="btn bg-transparent border-blue-300 text-blue-300 rounded-round border-2 btn-icon"><i class="icon-alert"></i></a>
                                </div>

                                <div class="media-body">
                                    <span class="font-weight-semibold bg-dark py-1 px-2 rounded text-info">@source</span> initiated a <span class="font-weight-semibold bg-dark py-1 px-2 rounded text-info">@type</span> alert at priority level <span class="font-weight-semibold bg-dark py-1 px-2 rounded text-info">@number</span> that reads: <code>@message lorem ipsum dolor set amit lorem IPSUM</code>
									<br><a href="#">access payload</a>
                                    <div class="text-muted font-size-sm">4 minutes ago</div>
                                </div>
                            </li>

                            <li class="media">
                                <div class="mr-3">
                                    <a href="#" class="btn bg-transparent border-orange-300 text-orange-300 rounded-round border-2 btn-icon"><i class="icon-alert"></i></a>
                                </div>

                                <div class="media-body">
                                    <span class="font-weight-semibold bg-dark py-1 px-2 rounded text-info">@source</span> initiated a <span class="font-weight-semibold bg-dark py-1 px-2 rounded text-info">@type</span> alert at priority level <span class="font-weight-semibold bg-dark py-1 px-2 rounded text-info">@number</span> that reads: <code>@message lorem ipsum dolor set amit lorem IPSUM</code>
									<br><a href="#">access payload</a>
                                    <div class="text-muted font-size-sm">6 minutes ago</div>
                                </div>
                            </li>

                            <li class="media">
                                <div class="mr-3">
                                    <a href="#" class="btn bg-transparent border-green text-green rounded-round border-2 btn-icon"><i class="icon-alert"></i></a>
                                </div>

                               <div class="media-body">
                                    <span class="font-weight-semibold bg-dark py-1 px-2 rounded text-info">@source</span> initiated a <span class="font-weight-semibold bg-dark py-1 px-2 rounded text-info">@type</span> alert at priority level <span class="font-weight-semibold bg-dark py-1 px-2 rounded text-info">@number</span> that reads: <code>@message lorem ipsum dolor set amit lorem IPSUM</code>
									<br><a href="#">access payload</a>
                                    <div class="text-muted font-size-sm">45 minutes ago</div>
                                </div>
                            </li>

                            <li class="media">
                                <div class="mr-3">
                                    <a href="#" class="btn bg-transparent border-pink-700 text-pink-700 rounded-round border-2 btn-icon"><i class="icon-alert"></i></a>
                                </div>

                                <div class="media-body">
                                    <span class="font-weight-semibold bg-dark py-1 px-2 rounded text-info">@source</span> initiated a <span class="font-weight-semibold bg-dark py-1 px-2 rounded text-info">@type</span> alert at priority level <span class="font-weight-semibold bg-dark py-1 px-2 rounded text-info">@number</span> that reads: <code>@message lorem ipsum dolor set amit lorem IPSUM</code>
									<br><a href="#">access payload</a>
                                    <div class="text-muted font-size-sm">Mon Nov 23 10:39am</div>
                                </div>
                            </li>

                            <li class="media">
                                <div class="mr-3">
                                    <a href="#" class="btn bg-transparent border-blue-300 text-blue-300 rounded-round border-2 btn-icon"><i class="icon-alert"></i></a>
                                </div>

                                <div class="media-body">
                                    <span class="font-weight-semibold bg-dark py-1 px-2 rounded text-info">@source</span> initiated a <span class="font-weight-semibold bg-dark py-1 px-2 rounded text-info">@type</span> alert at priority level <span class="font-weight-semibold bg-dark py-1 px-2 rounded text-info">@number</span> that reads: <code>@message lorem ipsum dolor set amit lorem IPSUM</code>
									<br><a href="#">access payload</a>
                                    <div class="text-muted font-size-sm">Mon Nov 23 10:59am</div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="dropdown-content-footer">
                        <a href="#" class="text-white mr-auto">+ Add Alert Widget to Grid</a>
                        <div>
                            <a href="#" class="text-white" data-popup="tooltip" title="Mark All Alerts as Read"><i class="icon-radio-unchecked"></i></a>
                        </div>
                    </div>
                </div>
            </li>
		</ul>
		<span id="nb-mission-online" class="badge bg-success my-3 my-md-0 ml-md-3 mr-md-auto" <c:if test="${!missionRunning}">style="display:none;"</c:if>>MISSION ONLINE</span>
		<span id="nb-mission-offline" class="badge bg-warning my-3 my-md-0 ml-md-3 mr-md-auto" <c:if test="${missionRunning}">style="display:none;"</c:if>>MISSION OFFLINE</span>
		<ul class="navbar-nav navbar-right">

      <li class="nav-item">
                <a href="${uiHome}/mission" class="navbar-nav-link ${fn:endsWith(pageContext.request.requestURI, '/mission') ? 'active' : ''}"><i class="icon-strategy mr-2"></i>Mission</a>
            </li>

      <li class="nav-item">
                <a href="${uiHome}/monitor" class="navbar-nav-link ${fn:endsWith(pageContext.request.requestURI, '/monitor') ? 'active' : ''}"><i class="icon-pulse2 mr-2"></i>Monitor</a>
            </li>

      <li class="nav-item">
                <a href="${uiHome}/track" class="navbar-nav-link ${fn:endsWith(pageContext.request.requestURI, '/track') ? 'active' : ''}"><i class="icon-target2 mr-2"></i>Track</a>
            </li>

      <li class="nav-item">
                <a href="${uiHome}/analyze" class="navbar-nav-link ${fn:endsWith(pageContext.request.requestURI, '/analyze') ? 'active' : ''}"><i class="icon-graph mr-2"></i>Analyze</a>
            </li>

      <li class="nav-item">
                <a href="${uiHome}/c2" class="navbar-nav-link ${fn:endsWith(pageContext.request.requestURI, '/c2') ? 'active' : ''}"><i class="icon-laptop mr-2"></i>C2</a>
            </li>

      <li class="nav-item">
                <a href="${uiHome}/grid" class="navbar-nav-link ${fn:endsWith(pageContext.request.requestURI, '/grid') ? 'active' : ''}"><i class="icon-grid6 mr-2"></i> Composable Grid</a>
            </li>

			<li class="nav-item">
                <a href="${uiHome}/controller" class="navbar-nav-link ${fn:endsWith(pageContext.request.requestURI, '/controller') ? 'active' : ''}"><i class="icon-strategy mr-2"></i>CES Setup</a>
            </li>

			<li class="nav-item">
                <a href="${uiHome}/map" class="navbar-nav-link ${fn:endsWith(pageContext.request.requestURI, '/map') ? 'active' : ''}"><i class="icon-map mr-2"></i> Mission Map</a>
            </li>

			<li class="nav-item dropdown dropdown-user">
				<a class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
					<span><c:out value="${userName}"/></span>
				</a>

				<div class="dropdown-menu dropdown-menu-right">
					<a class="dropdown-item" href="${uiHome}/settings"> <i class="icon-gear"></i> Account Settings</a>
					<a class="dropdown-item" id="navbar-save-page"> <i class="icon-gear"></i> Set Start Page</a>
					<c:if test="${myPage == 'grid'}">
					    <a class="dropdown-item" id="navbar-save-widgets"> <i class="icon-gear"></i> Save Widget Layout</a>
					</c:if>
                    <c:if test="${myPage == 'map'}">
                        <a class="dropdown-item" id="navbar-save-map"> <i class="icon-gear"></i></i> Save Map Layout</a>
                    </c:if>
					<a class="dropdown-item logoutA" href="${uiHome}/logout"><i class="icon-switch"></i> Logout</a>
				</div>
			</li>
		</ul>
	</div>
	<!-- /navbar content -->
</div>
<!-- /main navbar -->
