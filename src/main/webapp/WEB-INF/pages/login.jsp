<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page import="java.util.Properties" %>
<%@ page import="com.nndata.ces.UIConfig" %>

<%

    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", 0);

    UIConfig uiConfig = new UIConfig(pageContext.getServletContext());

    pageContext.setAttribute("uiHome",  uiConfig.getUiHome());

%>

<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>LogIn to CES</title>

		<!-- Global stylesheets -->
		<link href="${uiHome}/assets/css/fonts/Roboto/styles.css" rel="stylesheet" type="text/css">
		<link href="${uiHome}/assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/bootstrap_ces.min.css" rel="stylesheet" type="text/css">
		<link href="${uiHome}/assets/css/ces_custom.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/components_ces.min.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/colors.min.css" rel="stylesheet" type="text/css">
		<link rel="apple-touch-icon" sizes="57x57" href="${uiHome}/assets/images/ico/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="${uiHome}/assets/images/ico/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="${uiHome}/assets/images/ico/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="${uiHome}/assets/images/ico/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="${uiHome}/assets/images/ico/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="${uiHome}/assets/images/ico/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="${uiHome}/assets/images/ico/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="${uiHome}/assets/images/ico/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="${uiHome}/assets/images/ico/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="${uiHome}/assets/images/ico/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="${uiHome}/assets/images/ico/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="${uiHome}/assets/images/ico/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="${uiHome}/assets/images/ico/favicon-16x16.png">
		<link rel="manifest" href="${uiHome}/assets/images/ico/manifest.json">
		<meta name="msapplication-TileColor" content="#9c9d9d">
		<meta name="msapplication-TileImage" content="${uiHome}/assets/images/ico/ms-icon-144x144.png">
		<meta name="theme-color" content="#eb6001">
		<!-- /global stylesheets -->

		<!-- Core JS files -->
        <script src="${uiHome}/assets/js/main/jquery.min.js"></script>
        <script src="${uiHome}/assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="${uiHome}/assets/js/plugins/loaders/blockui.min.js"></script>
	    <script type="text/javascript" src="${uiHome}/assets/js/plugins/time/jstz.min.js"></script>
	    <script type="text/javascript" src="${uiHome}/assets/js/plugins/cookie/js.cookie.js"></script>
        <!-- /core JS files -->

		<!--App JS Files -->
        <script type="text/javascript" src="${uiHome}/assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script type="text/javascript" src="${uiHome}/assets/js/plugins/ui/prism.min.js"></script>

		<script type="text/javascript" src="${uiHome}/assets/js/app.js"></script>
	    <!-- App JS Files -->

		<script>
			$(function() {
				var localTZ = jstz.determine().name();
				$('#local_timezone').val(localTZ);
				Cookies.set('user_time_zone', localTZ, { path: '/'} );
			});
			window.name = 'composable-ui';
		</script>

</head>

<body class="login-container login-cover">
		<div class="page-content">
			<!--<div class="content-wrapper">
				<form action="login" method="POST">
					<div class="panel panel-body login-form">
						<div class="text-center">
							<h5 class="content-group-lg">CES UI <small class="display-block">Login to Your Account</small></h5>
						</div>
						<div class="form-group form-group-material">
							<label class="control-label text-semibold text-size-small animate">USERNAME</label>
							<input type="text" name="username" class="form-control" placeholder="username">
						</div>
						<div class="form-group form-group-material pb-20">
							<label class="control-label text-semibold text-size-small animate">PASSWORD</label>
							<input type="password" name="password" class="form-control" placeholder="password">
						</div>
						<input type="hidden" name="local_timezone" id="local_timezone" value="" />
						<div class="form-group">
							<button type="submit" class="btn bg-purple-600 btn-block" value="ACCESS NNVISION">LogIn <i class="icon-circle-right2 position-right"></i></button>
						</div>
					</div>
				</form>
			</div>-->


			<!-- Content area -->
			<div class="content d-flex justify-content-center align-items-center pt-0">

				<!-- Login form -->
				<form action="login" method="POST">
					<div class="card mb-0">
						<div class="card-body">
							<div class="text-center mb-3">
								<img src="${uiHome}/assets/images/Sensei_Logo_Light.png" style="height:60px;" alt="">
								<span class="d-block text-muted">Login to Your Account</span>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="text" name="username" class="form-control" placeholder="username">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="password" name="password" class="form-control" placeholder="password">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>
							<input type="hidden" name="local_timezone" id="local_timezone" value="" />
							<div class="form-group">
								<button type="submit" class="btn bg-teal btn-block" value="ACCESS NNVISION">LogIn <i class="icon-circle-right2 position-right"></i></button>
							</div>
						</div>
					</div>
				</form>
				<!-- /login form -->

			</div>
			<!-- /content area -->

		</div>
</body>
</html>
