<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page import="java.util.Properties" %>
<%@ page import="com.nndata.ces.UIConfig" %>
<%
    UIConfig uiConfig = new UIConfig(pageContext.getServletContext());

    pageContext.setAttribute("uiHome",   uiConfig.getUiHome());
    pageContext.setAttribute("userName", uiConfig.getUserName());
    pageContext.setAttribute("mapState", uiConfig.getMapState().toString());

%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Global stylesheets -->
        <link href="${uiHome}/assets/css/fonts/Roboto/styles.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/bootstrap_ces.min.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/ces_custom.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/components_ces.min.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/openlayer/ol.css" rel="stylesheet" type="text/css">
        <link href="${uiHome}/assets/css/misc.css" rel="stylesheet" type="text/css">
        <link rel="apple-touch-icon" sizes="57x57" href="${uiHome}/assets/images/ico/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="${uiHome}/assets/images/ico/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="${uiHome}/assets/images/ico/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="${uiHome}/assets/images/ico/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="${uiHome}/assets/images/ico/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="${uiHome}/assets/images/ico/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="${uiHome}/assets/images/ico/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="${uiHome}/assets/images/ico/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="${uiHome}/assets/images/ico/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="${uiHome}/assets/images/ico/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="${uiHome}/assets/images/ico/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="${uiHome}/assets/images/ico/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="${uiHome}/assets/images/ico/favicon-16x16.png">
        <link rel="manifest" href="${uiHome}/assets/images/ico/manifest.json">
          <link href="${uiHome}/assets/css/controller.css" rel="stylesheet" type="text/css">

        <meta name="msapplication-TileColor" content="#9c9d9d">
        <meta name="msapplication-TileImage" content="${uiHome}/assets/images/ico/ms-icon-144x144.png">
        <meta name="theme-color" content="#eb6001">
        <!-- /global stylesheets -->
        <!-- Core JS files -->
        <script src="${uiHome}/assets/js/main/jquery.min.js"></script>
        <script src="${uiHome}/assets/js/main/bootstrap.bundle.min.js"></script>
        <script src="${uiHome}/assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->
        <!-- App JS files -->
        <script type="text/javascript" src="${uiHome}/assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script src="${uiHome}/assets/js/plugins/notifications/sweetalert.min.js"></script>
        <script type="text/javascript" src="${uiHome}/assets/js/plugins/ui/prism.min.js"></script>
        <script type="text/javascript" src="${uiHome}/assets/js/plugins/split/split.min.js"></script>
        <script type="text/javascript" src="${uiHome}/assets/js/plugins/openlayer/ol.js"></script>
        <script type="text/javascript" src="${uiHome}/assets/js/app.js"></script>
        <script type="text/javascript" src="${uiHome}/assets/js/includes/navbar.js"></script>
        <script type="text/javascript" src="${uiHome}/assets/js/includes/ces_sidebar.js"></script>
        <script type="text/javascript" src="${uiHome}/assets/js/includes/map.js"></script>
        <link href="${uiHome}/assets/css/controller.css" rel="stylesheet" type="text/css">
      <link href="${uiHome}/assets/css/controller-full.css" rel="stylesheet" type="text/css">
    </head>
    <body class="appHeight sidebar-secondary-hidden">
        <!-- Main navbar -->
        <jsp:include page="/WEB-INF/pages/global/navbar.jsp">
            <jsp:param name="page" value="map" />
        </jsp:include>
        <!-- /main navbar -->
        <!-- Page content -->



        <div class="page-content p-0">
            <div id="ui-current-page" style="display:none;">map</div>
            <div id="composable-ui-home" style="display:none;">${uiHome}</div>
            <div id="ces-map-state" style="display:none;">${mapState}</div>
            <!-- Secondary sidebar -->
            <div class="sidebar sidebar-light sidebar-secondary sidebar-expand-md">
                <!-- Sidebar mobile toggler -->
                <div class="sidebar-mobile-toggler text-center">
                    <a href="#" class="sidebar-mobile-secondary-toggle">
                    <i class="icon-arrow-left8"></i>
                    </a>
                    <span class="font-weight-semibold">Secondary sidebar</span>
                    <a href="#" class="sidebar-mobile-expand">
                    <i class="icon-screen-full"></i>
                    <i class="icon-screen-normal"></i>
                    </a>
                </div>
                <!-- /sidebar mobile toggler -->
                <!-- Sidebar content -->
                <div class="sidebar-content">
                    <!-- Sidebar search -->
                    <div class="card">
                        <div class="card-header bg-transparent border-bottom border-bottom-light header-elements-inline">
                            <span class="text-uppercase font-size-sm font-weight-semibold">Map Controls</span>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">

                        </div>
                        <div class="card-body" id="ces-map-layer-list">
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header bg-transparent border-bottom border-bottom-light header-elements-inline">
                            <span class="text-uppercase font-size-sm font-weight-semibold">UI Watcher</span>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">

                        </div>
                        <div class="card-body" >

                          <div class="form-check">
                            <label class="form-check-label">
                            <div class="uniform-checker">
                              <span class="checked">

                              </span>
                            </div>
                              Getaway Car
                              </label>
                          </div>
                        </div>
                    </div>
                    <!-- /sidebar search -->
                </div>
                <!-- /sidebar content -->
            </div>
            <!-- /secondary sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">
                <!-- Content area -->
                <div class="content">
                    <div class="removeMargin">
                        <div class="splitMain" id="main">
                            <div id="topMap" class="map"></div>
                            <div id="bottomData">
                                <iframe src="${uiHome}/timeline" height="100%" width="100%" class="border-0"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /content area -->
            </div>
            <!-- /main content -->




        </div>
        <!-- /page content -->
        <!-- //SOMETHING JERRY DID <div class="page-container no-padding-bottom">
            <div class="page-content">
                <c:set var="col" value="0"/>
                <c:forEach var="entry" items="${modules}">
                    <c:set var="module" value="${entry.value}"/>
                    <c:if test="${col == 0}">
                        <div class="row">
                    </c:if>
                    <c:set var="col" value="${col + 1}"/>
                    <div class="col-md-4" style="border:1px solid black;">
                        <jsp:include page="modules/${module.url}">
                            <jsp:param name="id" value="${entry.key}"/>
                        </jsp:include>
                    </div>
                    <c:if test="${col == 3}">
                        </div>
                        <c:set var="col" value="0"/>
                    </c:if>
                </c:forEach>
                <c:if test="${col != 0}">
                    </div>
                </c:if>
            </div>
            </div>-->
    </body>
</html>
