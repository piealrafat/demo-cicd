<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page import="com.nndata.ces.UIConfig" %>
<%@ page import="com.nndata.ces.controller.ClusterData" %>
<%

    long        pollInterval = ClusterData.getPollInterval();
    UIConfig    uiConfig     = new UIConfig(pageContext.getServletContext());
    ClusterData clusterData  = new ClusterData();

    String piStr = request.getParameter("pollInterval");
    try {
        pollInterval = Long.parseLong(piStr, 10);
    } catch (Throwable t) {
    }

    if (pollInterval < 0) {
        pollInterval = 0;
    } else if (pollInterval > 60000) {
        pollInterval = 60000;
    }

    pageContext.setAttribute("baseUrl",        uiConfig.getUiHome() + "/controller/status");
    pageContext.setAttribute("pollInterval",   pollInterval);
    pageContext.setAttribute("uiHome",         uiConfig.getUiHome());
    pageContext.setAttribute("userName",       uiConfig.getUserName());
    pageContext.setAttribute("messages",       clusterData.getStatus());

%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Watchlist Generator</title>
  <link href="${uiHome}/assets/css/fonts/Roboto/styles.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/controller.css" rel="stylesheet" type="text/css">

	<!-- CES Core JS files -->
	<script src="${uiHome}/assets/js/main/jquery.min.js"></script>
	<script src="${uiHome}/assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="${uiHome}/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /CES core JS files -->

	<!-- CES Mission Controller JS files -->
	<script src="${uiHome}/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script src="${uiHome}/assets/js/plugins/notifications/sweetalert.min.js"></script>
	<script src="${uiHome}/assets/js/plugins/cookie/js.cookie.js"></script>

	<script src="${uiHome}/assets/js/app.js"></script>
	<script src="${uiHome}/assets/js/includes/controller.js"></script>
	<!-- /CES Mission Controller JS files -->

</head>

<body style="background-color: #666;">
<div id="content" style="background-color: #666;">



      <div class="search-container">
        <form action="">
          <input class="w100 sbar" type="text" placeholder="Search for attributes or existing watchlist items" name="search">
        </form>
      </div>

      <div class="results-container">
        <textarea class="w100" name="name" rows="3" cols="80" placeholder="Results shown here..."></textarea>
      </div>
      <div class="attributes-container">
      <label><h2>Attributes</h2></label>
      <div  class="pb3">
        <button class="accordion">Person</button>
        <div class="panel" >
          <div><h3>Clothing</h3></div>
          <div class="row">
            <div class="col-sm-6 col-md-4 col-xs-6">
              <label class="pl10"><h4>Tops:</h4></label>
              <select class="watchlist-dropdown f-right" name="Clothing-Tops">
                <option value="Any">Any (default)</option>
                <option value="Jacket">Jacket</option>
                <option value="t-shirt">T-shirt</option>
                <option value="Hoodie">Hoodie</option>
                <option value="Dress Shirt">Dress Shirt</option>
                <option value="Tank top">Tank Top</option>
              </select>
            </div>
            <div class="col-sm-6 col-md-4 col-xs-6">
            <label><h4>Color:</h4></label>
            <select class="watchlist-dropdown" name="Color-types">
              <option value="Any">Any (default)</option>
              <option value="White">White</option>
              <option value="Red">Red</option>
              <option value="Blue">Blue</option>
              <option value="Black">Black</option>
              <option value="Grey">Grey</option>
              <option value="Denim">Denim</option>
              <option value="Yellow">Yellow</option>
            </select>
              </div>
          </div>
          <div class="row">
            <div class="col-sm-6 col-md-4">
            <label class="pl10"><h4>Bottoms:</h4></label>
            <select class="watchlist-dropdown f-right" name="Clothing-Bottoms">
              <option value="Any">Any (default)</option>
              <option value="Jeans">Jeans</option>
              <option value="Cargo Pants">Cargo Pants</option>
              <option value="Dress">Dress</option>
              <option value="Shorts">Shorts</option>
              <option value="Slacks">Slacks</option>
            </select>
            </div>
            <div class="col-sm-6 col-md-4">
            <label><h4>Color:</h4></label>
            <select class="watchlist-dropdown" name="Color-types">
              <option value="Any">Any (default)</option>
              <option value="White">White</option>
              <option value="Red">Red</option>
              <option value="Blue">Blue</option>
              <option value="Black">Black</option>
              <option value="Grey">Grey</option>
              <option value="Denim">Denim</option>
              <option value="Yellow">Yellow</option>
            </select>
              </div>
          </div>
          <div class="row">
            <div class="col-sm-6 col-md-4">
            <label class="pl10"><h4>Headwear:</h4></label>
            <select class="watchlist-dropdown f-right" name="Clothing-Bottoms">
              <option value="Any">Any (default)</option>
              <option value="Ballcap">Ballcap</option>
              <option value="Beret">Beret</option>
              <option value="Beanie">Beanie</option>
              <option value="Scarf">Scarf</option>
            </select>
            </div>
            <div class="col-sm-6 col-md-4">
            <label><h4>Color:</h4></label>
            <select class="watchlist-dropdown" name="Color-types">
              <option value="Any">Any (default)</option>
              <option value="White">White</option>
              <option value="Red">Red</option>
              <option value="Blue">Blue</option>
              <option value="Black">Black</option>
              <option value="Grey">Grey</option>
              <option value="Denim">Denim</option>
              <option value="Yellow">Yellow</option>
            </select>
              </div>
          </div>
          <div class="row">
            <div class="col-sm-6 col-md-4">
            <label class="pl10"><h4>Footwear:</h4></label>
            <select class="watchlist-dropdown f-right" name="Clothing-Bottoms">
              <option value="Any">Any (default)</option>
              <option value="Boots">Boots</option>
              <option value="Sneakers">Sneakers</option>
              <option value="Dress Shoes">Dress Shoes</option>
              <option value="Heels">Heels</option>
            </select>
            </div>
            <div class="col-sm-6 col-md-4">
            <label><h4>Color:</h4></label>
            <select class="watchlist-dropdown" name="Color-types">
              <option value="Any">Any (default)</option>
              <option value="White">White</option>
              <option value="Red">Red</option>
              <option value="Blue">Blue</option>
              <option value="Black">Black</option>
              <option value="Grey">Grey</option>
              <option value="Denim">Denim</option>
              <option value="Yellow">Yellow</option>
            </select>
              </div>
          </div>
          <div>
            <label><h3>Height (inches):</h3></label>
            <label><h5>from</h5></label>
            <input style="line-height: 1.5; font-size: 18px;" type="number" name="height-min" min="48" max="96">
            <label><h5>to</h5></label>
            <input style="line-height: 1.5; font-size: 18px;" type="number" name="height-max" min="48" max="96">
          </div>
          <div>
            <label><h3>Weight (pounds):</h3></label>
            <label><h5>from</h5></label>
            <input style="line-height: 1.5; font-size: 18px;" type="number" name="height-min" min="85" max="300">
            <label><h5>to</h5></label>
            <input style="line-height: 1.5; font-size: 18px;" type="number" name="height-max" min="85" max="300">
          </div>
          <div>
            <label><h3>Temperatures (℉):</h3></label>
            <label><h5>from</h5></label>
            <input style="line-height: 1.5; font-size: 18px;" type="number" name="height-min" min="0" max="300">
            <label><h5>to</h5></label>
            <input style="line-height: 1.5; font-size: 18px;" type="number" name="height-max" min="0" max="300">
          </div>
          <div>
            <label><h3>Speed (mph):</h3></label>
            <label><h5>from</h5></label>
            <input style="line-height: 1.5; font-size: 18px;" type="number" name="height-min" min="0" max="180">
            <label><h5>to</h5></label>
            <input style="line-height: 1.5; font-size: 18px;" type="number" name="height-max" min="0" max="180">
          </div>
          <div>
            <label><h3>Status:</h3></label>
            <select class="watchlist-dropdown" name="Status-type">
              <option value="Unknown">Unknown (default)</option>
              <option value="Friendly">Friendly</option>
              <option value="Hostile">Hostile</option>
            </select>
          </div>
        </div>
      </div>
      <div class="pb3">
        <button class="accordion">Automobile</button>
        <div class="panel" >
          <p>lorem ipsum .....</p>
        </div>
      </div>
      <div class="pb3">
        <button class="accordion">Radio</button>
        <div class="panel">
          <p>lorem ipsum .....</p>
        </div>
      </div>
      <div class="pb10">
        <button class="accordion">Cellphone</button>
        <div class="panel">
          <p>lorem ipsum .....</p>
        </div>
      </div>
      <button class="watchlist-btn f-right" type="button" name="button">Add to watchlist</button>
      </div>
      <div class="watchlist-container">
        <label>
          <h2>Watchlist Information</h2>
        </label>
        <div>
          <label>
            <h4>Name:</h4>
          </label>
          <input class="nbar" type="text" name="watchlist name" placeholder="Name the watchlist item">
        </div>
        <div>
          <label class="mb0">
              <h4 class="mb0">Attributes:</h4>
            </label>
            <div class="results-container pb20">
              <textarea class="w100" name="name" rows="3" cols="80" placeholder="Attributes selected will be listed here"></textarea>
            </div>
        </div>
        <div class="pb10">
          <label><h4>Priority:</h4></label>
          <select class="watchlist-dropdown nbar" name="priorityLevel">
            <option value="Low">Low (default)</option>
            <option value="Medium">Medium</option>
            <option value="High">High</option>
          </select>
        </div>
        <button class="watchlist-btn" type="button" name="button">Create Watchlist Item</button>
      </div>
      <div class="wItems-container">
        <label>
          <h2>Watchlist Items</h2>
        </label>
        <div class="getawayCar">
          <button class="watchlist-btn pill " type="button" name="button">Getaway Car</button>
        </div>
      </div>

  <div id= "Watchlist-Item" class="modal fade getawayCar" data-backdrop="false">
      <div class="modal-dialog modal-sm">
          <div class="modal-content">
              <div class="modal-header">
                  <span class="modal-title"><h2>Watchlist Item</h2></span>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <div class="pb10">
                  <label><h2>Getaway Car:</h2></label>
                </div>
                <div class="pb10">
                  <label><h4>Attributes:</h4></label>
                </div>

              </div>
          </div>
      </div>
  </div>

</div>
 <footer class="controller-footer">
 <div class="container">
   <div class="header-elements">
       <a class="btn btn-sm btn-outline-info controller-config"><i class="icon-gear"></i></a>
       <a class="btn btn-sm btn-outline-primary controller-reload"><i class="icon-loop3"></i></a>
       <a class="btn btn-sm btn-outline-success">Save</a>
       </div>
   </div>
 </div>
</footer>



</body>
</html>
