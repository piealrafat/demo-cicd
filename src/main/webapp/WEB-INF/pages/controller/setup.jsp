<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page import="com.nndata.ces.UIConfig" %>
<%

    UIConfig    uiConfig     = new UIConfig(pageContext.getServletContext());

    pageContext.setAttribute("uiHome",         uiConfig.getUiHome());
    pageContext.setAttribute("userName",       uiConfig.getUserName());

%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>CES Setup</title>
	<link href="${uiHome}/assets/css/fonts/Roboto/styles.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/bootstrap_ces.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/ces_custom.css" rel="stylesheet" type="text/css">
      <link href="${uiHome}/assets/css/controller.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/controller-full.css" rel="stylesheet" type="text/css">
	<!-- CES Core JS files -->
	<script src="${uiHome}/assets/js/main/jquery.min.js"></script>
	<script src="${uiHome}/assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="${uiHome}/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /CES core JS files -->

	<!-- CES Mission Controller JS files -->
	<script src="${uiHome}/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script src="${uiHome}/assets/js/plugins/notifications/sweetalert.min.js"></script>
	<script src="${uiHome}/assets/js/plugins/cookie/js.cookie.js"></script>

	<script src="${uiHome}/assets/js/app.js"></script>
	<script type="text/javascript" src="${uiHome}/assets/js/includes/navbar.js"></script>
  <script src="${uiHome}/assets/js/includes/controller.js"></script>
	<!-- /CES Mission Controller JS files -->

</head>

<body>
	<!-- Main navbar -->
	<jsp:include page="/WEB-INF/pages/global/navbar.jsp" />
	<!-- /main navbar -->

	<div class="page-content">

	    <div id="cluster-widget-mode" style="display:none;">main</div>
	    <div id="cluster-ui-home" style="display:none;"><c:out value="${uiHome}"/></div>
	    <div id="ui-current-page" style="display:none;">planner</div>
	    <div id="composable-ui-home" style="display:none;">${uiHome}</div>

		<!-- Main content -->
    <div class="content">


      <div class="text-center mission-header" style="margin: 0 75px;">

          <strong>Let's make a mission possible!</strong>
        </div>


			<!-- Content area -->
      <div class="row list-unstyled widget-list">
        <div class="col grid-list-item-template" >
          <div class="card template-card" style="margin: 10px 20px; margin: auto;">
            <div class="card-body-div " style="padding: 0 50px;">
              <div class="Lheader">
                  Basic Template
                  <div class="f-right">
                    <a href="${uiHome}/controller\scratch.jsp" class="btn btn-lg mc-btn scratch-btn " style="margin-right: 10px;">Start from Scratch</a>
                    <a class="btn btn-lg mc-btn ">Existing Template</a>
                  </div>
                  <p>
                    <h4>
                      This template will get you started on a basic mission. You can modify mission parameters (e.g., Sensors, Analytics) based on your mission needs.
                    </h4>
                  </p>

              </div>
              <hr>
                <div class="row">


                  <div class="col-md-6">
                    <div class="card-header text-center">
                      <h1 class="mb0">Sensors</h1>
                    </div>
                    <div class="card-body-div temp-card">
                       <a class="btn btn-sm btn-outline-success add-btn f-right asset-modal">+</a>
                      <span class="badge rounded-pill bg-primary mb20 tag">Radar
                        <button type="button" class="close x-btn" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </span>
                      <br>
                      <span class="badge rounded-pill bg-primary tag">SDR
                        <button type="button" class="close x-btn" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </span>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="card-header text-center">
                      <i class="fas fa-info-circle f-right analytics-modal"></i>
                      <h1 class="mb0">Analytic(s)</h1>
                    </div>
                    <div class="card-body-div temp-card">
                       <a class="btn btn-sm btn-outline-success add-btn f-right analytic-add-modal">+</a>
                      <span class="badge rounded-pill bg-primary mb20 tag">ARCHER-1
                        <button type="button" class="close x-btn" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </span>
                      <br>
                      <span class="badge rounded-pill bg-primary mb20 tag">ARCHER-2
                        <button type="button" class="close x-btn" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </span>
                      <br>
                      <span class="badge rounded-pill bg-primary tag">WATCHER
                        <button type="button" class="close x-btn" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </span>
                    </div>
                  </div>

                </div>

            </div>
          </div>
        </div>

      </div>

        <div class="wrap text-center">
            <button class="buttonX">Launch Mission</button>
        </div>
			<!-- /content area -->

  </div>
		<!-- /main content -->
    <div id="platform-add" class="modal fade platform-modal" data-backdrop="false">
        <div class="modal-dialog modal-sm" style="margin-top: 100px;">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="modal-title">Add a Platform</span>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div style="height:130px;overflow:auto;">
                        <div class="form-group">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="10000">
                                    UAV
                                </label>
                            </div>
                            <div class="form-check" style="margin-top:10px;">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="30000">
                                    Ground Vehicle
                                </label>
                            </div>
                            <div class="form-check" style="margin-top:10px;">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="60000">
                                    Ground Unit
                                </label>
                            </div>
                            <div class="form-check" style="margin-top:10px;">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="0">
                                    Spec-Ops
                                </label>
                            </div>
                        </div>
                    </div>
                    <div style="height:50px;padding-top:10px;">
                        <div class="header-elements">
                    <a class="btn btn-sm btn-outline-success controller-set-interval">OK</a>
                    <a class="btn btn-sm btn-outline-warning" data-dismiss="modal">Cancel</a>
                  </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="asset-add" class="modal fade asset-modal" data-backdrop="false">
        <div class="modal-dialog modal-sm" style="margin-top: 100px;">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="modal-title">Add an Assett</span>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div style="height:130px;overflow:auto;">
                        <div class="form-group">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="10000">
                                    UAV
                                </label>
                            </div>
                            <div class="form-check" style="margin-top:10px;">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="30000">
                                    Ground Vehicle
                                </label>
                            </div>
                            <div class="form-check" style="margin-top:10px;">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="60000">
                                    Ground Unit
                                </label>
                            </div>
                            <div class="form-check" style="margin-top:10px;">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="0">
                                    Spec-Ops
                                </label>
                            </div>
                        </div>
                    </div>
                    <div style="height:50px;padding-top:10px;">
                        <div class="header-elements">
                    <a class="btn btn-sm btn-outline-success controller-set-interval">OK</a>
                    <a class="btn btn-sm btn-outline-warning" data-dismiss="modal">Cancel</a>
                  </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="analytic-add" class="modal fade analytic-add-modal" data-backdrop="false">
        <div class="modal-dialog modal-sm" style="margin-top: 100px;">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="modal-title">Add an Analytic</span>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div style="overflow:auto;">
                        <div class="form-group">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="10000">
                                    FOG
                                </label>
                            </div>
                            <div class="form-check" style="margin-top:10px;">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="30000">
                                    SPoTTER
                                </label>
                            </div>
                            <div class="form-check" style="margin-top:10px;">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="60000">
                                    ARCHER
                                </label>
                            </div>
                            <div class="form-check" style="margin-top:10px;">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="0">
                                    RANGE
                                </label>
                            </div>
                            <div class="form-check" style="margin-top:10px;">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="0">
                                    WATCHER
                                </label>
                            </div>
                            <div class="form-check" style="margin-top:10px;">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="0">
                                    NAGE
                                </label>
                            </div>
                            <div class="form-check" style="margin-top:10px;">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="0">
                                    SAKE
                                </label>
                            </div>
                            <div class="form-check" style="margin-top:10px;">
                                <label class="form-check-label">
                                    <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="0">
                                    GeoDe
                                </label>
                            </div>
                        </div>
                    </div>
                    <div style="height:50px;padding-top:10px;">
                        <div class="header-elements">
                    <a class="btn btn-sm btn-outline-success controller-set-interval">OK</a>
                    <a class="btn btn-sm btn-outline-warning" data-dismiss="modal">Cancel</a>
                  </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="analytics-info" class="modal fade analytics-modal controller-config" data-backdrop="false">
        <div class="modal-dialog modal-xl" style="margin-top: 100px;">
            <div class="modal-content">
                <div class=" text-center">
                    <div class="Mheader bold">
                      Analytics Information
                      <button type="button" class="close mr24" data-dismiss="modal">&times;</button>
                    </div>
                </div>
                  <div class="modal-body" style="overflow:auto; max-height: 700px;">
                    <div >
                        <div class="">
                          <div>
                            <h3>FOG</h3>
                            <ul>
                              <li>
                                <h5>
                                  Fused Observation Generator (FOG) provides a live-virtual-constructive (LVC) simulation framework for fusing sensor-fusion data (aka observations) for higher-level analytical processing.
                                </h5>
                              </li>
                              <li style="list-style: none;">
                                <h4>Dependencies:</h4>
                              </li>
                                <ul>
                                  <li>none</li>
                                </ul>
                            </ul>
                          </div>

                          <hr style="width: 80%;">

                          <div>
                            <h3>SPoTTER</h3>
                            <ul>
                              <li>
                                <h5>
                                  Subject Position Timeseries Tracking and Entity Recognition (SPoTTER) is an analytic module for entity detection from signal/obseravation characteristics.
                                </h5>
                              </li>
                              <li style="list-style: none;">
                                <h4>Inputs:</h4>
                              </li>
                                <ul>
                                  <li>SPoTTER is designed to take in observation inputs from FOG</li>
                                </ul>
                                <li style="list-style: none;">
                                  <h4>Usage:</h4>
                                  <p>This module can be configured using environment variables.</p>
                                </li>
                                  <ul>
                                    <li>`REDIS_HOST` (default `localhost`)</li>
                                    <li>`REDIS_PORT` (default `6379`)</li>
                                    <li>`OBSERVER` (default `all`)</li>
                                    <li>`ZONE` (default `all`)</li>
                                  </ul>
                                <li style="list-style: none;">
                                  <h4>Dependencies:</h4>
                                </li>
                                  <ul>
                                    <li>FOG</li>
                                  </ul>
                            </ul>
                          </div>

                          <hr style="width: 80%;">

                            <div>
                              <h3>ARCHER</h3>
                              <ul>
                                <li>
                                  <h5>
                                    Agglomerative hierRCHical Entity clusteRing (ARCHER) is an analytic module for positional clustering from identified entities.
                                  </h5>
                                </li>
                                  <li style="list-style: none;">
                                    <h4>Usage:</h4>
                                    <p>Can be specified in the docker-compose.yaml as environmental variables if desired. Values will use default values if no environmental values are specified.</p>
                                  </li>
                                    <ul>
                                      <li> `observer` specifies an observer to take observations from and process</li>
                                      <p>Expects the name of an observer or `all` as an argument.  Specifying `all` will process observations from all observers in all messages received </p>
                                      <ul>
                                        Default: 'all'
                                      </ul>
                                      <li>`REDIS_PORT` (default `6379`)</li>
                                      <li>`OBSERVER` (default `all`)</li>
                                      <li>`ZONE` (default `all`)</li>
                                    </ul>
                                  <li style="list-style: none;">
                                    <h4>Dependencies:</h4>
                                  </li>
                                    <ul>
                                      <li>FOG</li>
                                    </ul>
                              </ul>
                            </div>

                        </div>
                    </div>
                    <div style="height:50px;padding-top:10px;">
                        <div class="header-elements">
                    <a class="btn btn-md btn-outline-success controller-set-interval" data-dismiss="modal">OK</a>
                  </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	</div>
	<!-- /page content -->

</body>
</html>
