<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page import="com.nndata.ces.UIConfig" %>
<%@ page import="com.nndata.ces.controller.ClusterData" %>
<%

    long        pollInterval = ClusterData.getPollInterval();
    UIConfig    uiConfig     = new UIConfig(pageContext.getServletContext());
    ClusterData clusterData  = new ClusterData();

    String piStr = request.getParameter("pollInterval");
    try {
        pollInterval = Long.parseLong(piStr, 10);
    } catch (Throwable t) {
    }

    if (pollInterval < 0) {
        pollInterval = 0;
    } else if (pollInterval > 60000) {
        pollInterval = 60000;
    }

    pageContext.setAttribute("baseUrl",        uiConfig.getUiHome() + "/controller/status");
    pageContext.setAttribute("pollInterval",   pollInterval);
    pageContext.setAttribute("uiHome",         uiConfig.getUiHome());
    pageContext.setAttribute("userName",       uiConfig.getUserName());
    pageContext.setAttribute("messages",       clusterData.getStatus());

%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>UI Watcher</title>
  <link href="${uiHome}/assets/css/fonts/Roboto/styles.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/controller.css" rel="stylesheet" type="text/css">

	<!-- CES Core JS files -->
	<script src="${uiHome}/assets/js/main/jquery.min.js"></script>
	<script src="${uiHome}/assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="${uiHome}/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /CES core JS files -->

	<!-- CES Mission Controller JS files -->
	<script src="${uiHome}/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script src="${uiHome}/assets/js/plugins/notifications/sweetalert.min.js"></script>
	<script src="${uiHome}/assets/js/plugins/cookie/js.cookie.js"></script>

	<script src="${uiHome}/assets/js/app.js"></script>
	<script src="${uiHome}/assets/js/includes/controller.js"></script>
	<!-- /CES Mission Controller JS files -->

</head>

<body>
  <div id="content">

    <table class="table text-nowrap">
      <thead>
        <tr>
          <th class="w-100">Watchlist Items</th>
          <th>Status</th>
          <th><i class="btn btn-sm btn-outline-success add-btn f-right analytic-add-modal add-watchlist-item">+</i></th>

        </tr>
      </thead>
      <tbody>

          <tr>
            <td>
              <div class="d-flex align-items-center">
                <div class="mr-1">
                  <div class="form-check">
                    <label class="form-check-label">
                      <input type="checkbox" class="form-check-input-styled-info ces-option" >
                    </label>
                  </div>
                </div>
                <div>
                  <span class="text-default font-weight-semibold">Getaway Car</span>
                </div>
              </div>
            </td>
            <td>Active</td>
            <td><i class="far fa-edit"></i><i class="fas fa-info-circle icon info-getaway"></i></td>



          </tr>

      </tbody>
    </table>

  </div>



  <div id= "Watchlist-Item" class="modal fade controller-modal getawayCar" data-backdrop="false">
      <div class="modal-dialog modal-sm">
          <div class="modal-content">
              <div class="modal-header">
                  <span class="modal-title">Watchlist Item</span>

              </div>
              <div class="modal-body ">
                <div class="pb10 text-center">
                  <label><strong>Getaway Car</strong></label>
                </div>
                <div class="pb10 ">
                  <label>Attributes:</label>
                  <p>Automobile
                  <br>Make/Model: Pontiac Tempest
                    <br>Color: Metallic mint green
                </p>
                </div>
                <div style="height:50px;padding-top:10px;">
                    <div class="header-elements text-center">
                <a class="btn btn-sm btn-outline-success controller-set-interval">OK</a>

                </div>
                </div>
              </div>
          </div>
      </div>
  </div>

  <div id= "Watchlist-Generator" class="modal fade controller-modal getawayCar" data-backdrop="false">
      <div class="modal-dialog modal-md">
          <div class="modal-content">
              <div class="modal-header " style="background-color: #008080; font-size: 1rem;">
                  <span class="modal-title">Watchlist Generator</span>

              </div>
              <iframe src="./controller/generator" height="230px" width="600px" class="border-0"></iframe>


          </div>
      </div>
  </div>

  <div id="start-from-scratch" class="modal fade scratch-modal" data-backdrop="false">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <span class="modal-title">Start Mission from Scratch</span>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                  <div style="height:130px;overflow:auto;">
                      <div class="form-group">
                          <div class="form-check">
                              <label class="form-check-label">
                                  <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="10000">
                                  10 seconds
                              </label>
                          </div>
                          <div class="form-check" style="margin-top:10px;">
                              <label class="form-check-label">
                                  <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="30000">
                                  30 seconds
                              </label>
                          </div>
                          <div class="form-check" style="margin-top:10px;">
                              <label class="form-check-label">
                                  <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="60000">
                                  1 minute
                              </label>
                          </div>
                          <div class="form-check" style="margin-top:10px;">
                              <label class="form-check-label">
                                  <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="0">
                                  Manual
                              </label>
                          </div>
                      </div>
                  </div>
                  <div style="height:50px;padding-top:10px;">
                      <div class="header-elements">
                  <a class="btn btn-sm btn-outline-success controller-set-interval">OK</a>
                  <a class="btn btn-sm btn-outline-warning" data-dismiss="modal">Cancel</a>
                </div>
                  </div>
              </div>
          </div>
      </div>
  </div>




   <footer class="controller-footer">
   <div class="container">
     <div class="header-elements">
         <a class="btn btn-sm btn-outline-info scratch-modal"><i class="icon-gear"></i></a>
         <a class="btn btn-sm btn-outline-primary controller-reload"><i class="icon-loop3"></i></a>
         <a class="btn btn-sm btn-outline-success ">Load Watchlist</a>
 				<a class="btn btn-sm btn-outline-warning ">Terminate Watchlist</a>
     </div>
   </div>
  </footer>



</body>
</html>
