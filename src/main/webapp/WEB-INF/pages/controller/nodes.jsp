<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page import="com.nndata.ces.UIConfig" %>
<%@ page import="com.nndata.ces.controller.ClusterData" %>
<%
    
    long        pollInterval = ClusterData.getPollInterval();
    UIConfig    uiConfig     = new UIConfig(pageContext.getServletContext());
    ClusterData clusterData  = new ClusterData();
    
    String piStr = request.getParameter("pollInterval");
    try {
        pollInterval = Long.parseLong(piStr, 10);
    } catch (Throwable t) {
    }
    
    if (pollInterval < 0) {
        pollInterval = 0;
    } else if (pollInterval > 60000) {
        pollInterval = 60000;
    }

    pageContext.setAttribute("baseUrl",        uiConfig.getUiHome() + "/controller/nodes");
    pageContext.setAttribute("pollInterval",   pollInterval);
    pageContext.setAttribute("uiHome",         uiConfig.getUiHome());
    pageContext.setAttribute("userName",       uiConfig.getUserName());
    pageContext.setAttribute("clusters",       clusterData.getClusters());
    pageContext.setAttribute("availableNodes", clusterData.getAvailableNodes());

%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>CES Mission Controller</title>
	<link href="${uiHome}/assets/css/fonts/Roboto/styles.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="${uiHome}/assets/css/controller.css" rel="stylesheet" type="text/css">
	<!-- CES Core JS files -->
	<script src="${uiHome}/assets/js/main/jquery.min.js"></script>
	<script src="${uiHome}/assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="${uiHome}/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /CES core JS files -->

	<!-- CES Mission Controller JS files -->
	<script src="${uiHome}/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script src="${uiHome}/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script src="${uiHome}/assets/js/plugins/notifications/sweetalert.min.js"></script>
	<script src="${uiHome}/assets/js/plugins/cookie/js.cookie.js"></script>

	<script src="${uiHome}/assets/js/app.js"></script>
	<script src="${uiHome}/assets/js/includes/controller.js"></script>
	<!-- /CES Mission Controller JS files -->

</head>

<body>

    <div id="controller-base-url" style="display:none;">${baseUrl}</div>
    <div id="cluster-poll-interval" style="display:none;">${pollInterval}</div>
	<div id="cluster-widget-mode" style="display:none;">nodes</div>
	<div id="cluster-ui-home" style="display:none;"><c:out value="${uiHome}"/></div>
	<div id="ui-current-page" style="display:none;">controller</div>
	<div id="composable-ui-home" style="display:none;">${uiHome}</div>
	
	<div id="content">
	
	    <div class="mission-controller-widget mission-controller-available">
			
			<table class="table text-nowrap">
				<thead>
					<tr>
						<th>Asset</th>
						<th class="w-100">Description</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${availableNodes}" var="node">
						<tr>
							<td>
								<div class="d-flex align-items-center">
									<div class="mr-1">
										<div class="form-check">
											<label class="form-check-label">
												<input type="checkbox" class="form-check-input-styled-info ces-option" data-id="<c:out value="${node.hostname}"/>">
											</label>
										</div>
									</div>
									<div>
										<span class="text-default font-weight-semibold"><c:out value="${node.hostname}"/></span>
									</div>
								</div>
							</td>
							<td><span class="text-default font-weight-semibold"><c:out value="${node.description}"/></span></td>
							<td><span class="text-default font-weight-semibold"><c:out value="${node.state}"/></span></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		
		</div>
		
		<div id="cluster-picker" class="modal fade controller-modal" data-backdrop="false">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <span class="modal-title">Choose Cluster</span>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div style="height:180px;">
                            <div style="height:100px;">
                                <div style="height:100%;overflow:auto;">
                                    <c:forEach items="${clusters}" var="cluster">
                                        <a class="dropdown-item nodes-pick-cluster" style="cursor:pointer;"><c:out value="${cluster}"/></a>
                                    </c:forEach>
                                </div>
                            </div>
                            <div style="height:80px;padding-top:10px;">
                                <label>
                                    <input class="form-control nodes-cluster-name" type="text" placeholder="New Cluster Name">
                                </label>
                                <div class="header-elements">
				                    <a class="btn btn-sm btn-outline-success nodes-named-cluster">OK</a>
				                    <a class="btn btn-sm btn-outline-warning" data-dismiss="modal">Cancel</a>
			                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="controller-config-refresh" class="modal fade controller-modal" data-backdrop="false">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <span class="modal-title">Choose Refresh Interval</span>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div style="height:130px;overflow:auto;">
                            <div class="form-group">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="10000">
                                        10 seconds
                                    </label>
                                </div>
                                <div class="form-check" style="margin-top:10px;">
                                    <label class="form-check-label">
                                        <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="30000">
                                        30 seconds
                                    </label>
                                </div>
                                <div class="form-check" style="margin-top:10px;">
                                    <label class="form-check-label">
                                        <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="60000">
                                        1 minute
                                    </label>
                                </div>
                                <div class="form-check" style="margin-top:10px;">
                                    <label class="form-check-label">
                                        <input type="radio" name="controller-refresh-interval" class="form-input-styled" value="0">
                                        Manual
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div style="height:50px;padding-top:10px;">
                            <div class="header-elements">
				                <a class="btn btn-sm btn-outline-success controller-set-interval">OK</a>
				                <a class="btn btn-sm btn-outline-warning" data-dismiss="modal">Cancel</a>
			                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	
	</div>
	
	<footer class="controller-footer">
		<div class="container">
			<div class="header-elements">
			    <a class="btn btn-sm btn-outline-info controller-config" title="Configure Refresh"><i class="icon-gear"></i></a>
			    <a class="btn btn-sm btn-outline-primary controller-reload"><i class="icon-loop3" title="Refresh Now"></i></a>
				<a class="btn btn-sm btn-outline-success join-cluster">Join Cluster</a>
			</div>
		</div>
	</footer>

</body>
</html>


