<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%

    String mode = "main";
    String uri = request.getRequestURI();

    if (uri.endsWith("/controller/clustered")) {
        mode = "clustered";
    } else if (uri.endsWith("/controller/nodes")) {
        mode = "nodes";
    } else if (uri.endsWith("/controller/missions")) {
        mode = "missions";
    } else if (uri.endsWith("/controller/external")) {
        mode = "external";
    } else if (uri.endsWith("/controller/status")) {
        mode = "status";
    } else if (uri.endsWith("/controller/generator")) {
        mode = "generator";
    } else if (uri.endsWith("/controller/watcher")) {
        mode = "watcher";
    }

    pageContext.setAttribute("mode", mode);

%>
<c:choose>
    <c:when test="${mode == 'clustered'}">
        <jsp:include page="/WEB-INF/pages/controller/clustered.jsp"/>
    </c:when>
    <c:when test="${mode == 'nodes'}">
        <jsp:include page="/WEB-INF/pages/controller/nodes.jsp"/>
    </c:when>
    <c:when test="${mode == 'missions'}">
        <jsp:include page="/WEB-INF/pages/controller/missions.jsp"/>
    </c:when>
    <c:when test="${mode == 'external'}">
        <jsp:include page="/WEB-INF/pages/controller/external.jsp"/>
    </c:when>
    <c:when test="${mode == 'status'}">
        <jsp:include page="/WEB-INF/pages/controller/status.jsp"/>
    </c:when>
    <c:when test="${mode == 'generator'}">
        <jsp:include page="/WEB-INF/pages/controller/generator.jsp"/>
    </c:when>
    <c:when test="${mode == 'watcher'}">
        <jsp:include page="/WEB-INF/pages/controller/watcher.jsp"/>
    </c:when>
    <c:otherwise>
        <jsp:include page="/WEB-INF/pages/controller/setup.jsp"/>
    </c:otherwise>
</c:choose>
