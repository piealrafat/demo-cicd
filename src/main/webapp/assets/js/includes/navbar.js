$(function() {

    var uiHome       = $('#composable-ui-home').text();
    var pollInterval = $('#nb-poll-interval').text();
    
    function savePageReturn(data) {
        if (data.status == 'success') {
            swal({
                title: 'Page Saved',
                icon: 'success'
            });
        } else {
            swal({
                title: 'Save Failed - ' + data.msg,
                icon: 'error'
            });
        }
    }

    function saveStartPage() {
        var curPage = $('#ui-current-page').text();
        $.ajax({
            url: uiHome + '/actions/saveStartPage',
            type: 'POST',
            data: {
                page: curPage
            }
        }).done(savePageReturn);
    }
    
    function setMissionStatus(running) {
        if (running) {
            $('#nb-mission-offline').hide();
            $('#nb-mission-online').show();
        } else {
            $('#nb-mission-online').hide();
            $('#nb-mission-offline').show();
        }
    }
    
    function missionStatReturn(data) {
        if (data.status === 'success') {
            setMissionStatus(data.running);
        }
    }
    
    function checkMissionStatus() {
        $.ajax({
            url: uiHome + '/actions/missionsRunning',
            type: 'GET'
        }).done(missionStatReturn);
    }

    $('#navbar-save-page').click(saveStartPage);
    
    if (pollInterval > 0) {
        setInterval(checkMissionStatus, pollInterval);
    }

});

