$(function() {

    var theMap      = null;
	var zoomPending = false;
	var currentZoom = 0;
	var uiHome      = $('#composable-ui-home').text();
	var myLayers    = [];

	var pointStyles = [{
	    fill:   new ol.style.Fill({color: 'rgba(255, 0, 0, 0.1)'}),
        stroke: new ol.style.Stroke({color: 'red', width: 1})
	},{
	    fill:   new ol.style.Fill({color: 'rgba(0, 0, 255, 0.1)'}),
        stroke: new ol.style.Stroke({color: 'blue', width: 1})
	},{
	    fill:   new ol.style.Fill({color: 'rgba(0, 255, 0, 0.1)'}),
        stroke: new ol.style.Stroke({color: 'green', width: 1})
	}]

	// Initialize split.js
	var split = Split(['#topMap', '#bottomData'], {
        direction: 'vertical',
		sizes: [80, 20]

    });

	// Checkboxes, radios
    $(".styled, .multiselect-container input").uniform({
        radioClass: 'choice'
    });

    function checkZoom() {
        zoomPending = false;
        var newZoom = Math.floor(theMap.getView().getZoom());
        if (newZoom != currentZoom) {
            currentZoom = newZoom;
            $.each(myLayers, function(idx, layer) {
                layer.getSource().refresh();
            })
        }
    }

    function onZoom() {
        if (!zoomPending) {
            zoomPending = true;
            setTimeout(checkZoom, 300);
        }
    }

    function featureStyleFunc(feature, styleIdx) {
        // Circles from 5 to 25
        // Cap the count at 500
        var cnt = Math.min(feature.get('count'), 500);
        var rad = Math.floor((cnt / 500) * 20) + 5;
        var s = new ol.style.Style({
            image: new ol.style.Circle({
                radius: rad,
                fill: pointStyles[styleIdx].fill,
                stroke: pointStyles[styleIdx].stroke
            }),
            text: new ol.style.Text({
                text: feature.get('count') + ''
            })
        });

        return s;
    }

     function getActiveMap() {
        var myMap = [];

            return myMap;
    }
    
    function saveMapReturn(data) {
        if (data.status == 'success') {
            swal({
                title: 'Map Layout Saved',
                icon: 'success'
            });
        } else {
            swal({
                title: 'Save Layout Failed - ' + data.msg
            });
        }
    }

     function saveMapState(localSession) {

        var view = theMap.getView();
        var ctr  = view.getCenter();
        var zm   = view.getZoom();

        var isSession  = false;
        var returnFunc = saveMapReturn;

        if (localSession) {
            isSession = true;
            returnFunc = function() { }
        }

        $.ajax({
            url: uiHome + '/actions/saveMapState',
            type: 'POST',
            data: {
                center: JSON.stringify(ctr),
                zoom: zm,
                session: isSession
            }
        
        }).done(returnFunc);
    }

    function afterMapMove() {
        saveMapState(true);
    }
    
    function toggleLayer() {
        var ck = $(this);
        var visible = ck.prop('checked');
        var idx = ck.attr('data-index');
        var theLayer = myLayers[idx];
        theLayer.setVisible(visible);
        ck.uniform('update');
    }

    function initLayers(data) {
        if (data.status === 'success') {

            var sbLayers = $('#ces-map-layer-list');
            sbLayers.empty();

            myLayers = [];

            $.each(data.fields, function(idx, fld) {

                var styleIdx = idx % pointStyles.length;

                var src = new ol.source.Vector({
                    format: new ol.format.GeoJSON(),
                    wrapX: true,
                    strategy: ol.loadingstrategy.bbox,
                    loader: function(extent, resolution, projection) {
                        var e = theMap.getView().calculateExtent();
                        var url = uiHome + '/queries/getFeatures?set=' + fld.set_oid + '&field=' + fld.field +
                                  '&topx=' + e[0] + '&topy=' + e[3] + '&bottomx=' + e[2] + '&bottomy=' + e[1];
                        var xhr = new XMLHttpRequest();
                        xhr.open('GET', url);
                        var onError = function() { src.removeLoadedExtent(extent); }
                        xhr.onerror = onError;
                        xhr.onload = function() {
                            if (xhr.status == 200) {
                                src.addFeatures(src.getFormat().readFeatures(xhr.responseText));
                                src.forEachFeature(function(feat) {
                                    var s = featureStyleFunc(feat, styleIdx);
                                    feat.setStyle(s);
                                });
                            } else {
                                onError();
                            }
                        }
                        xhr.send();
                    }
                });
                var layer = new ol.layer.Vector({
                    source: src
                });
                theMap.addLayer(layer);
                myLayers.push(layer);
                
                var sbDIV = '<div class="form-check"><label class="form-check-label"><input type="checkbox" checked class="ces-layer-toggle form-check-input-styled" data-index="' + idx + '"  > ' + fld.set_name + ' - ' + fld.displayName + '</label></div>';               
                sbLayers.append(sbDIV);
            });
            sbLayers.find('.ces-layer-toggle').uniform().on('change', toggleLayer);
        }
    }

    // Load maps
    theMap = new ol.Map({
        layers: [
            new ol.layer.Tile({
                // This illustrates a custom tiles source but for using
                // official OpenStreetMap server new ol.source.OSM()
                // instead of new ol.source.XYZ(...) is enough
                source: new ol.source.XYZ({
                    attributions: [ ol.source.OSM.ATTRIBUTION ],
                    url: 'http://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
                })
            })

        ],
        controls: ol.control.defaults({
            // Set to display OSM attributions on the bottom right control
            attributionOptions:  {
                collapsed: false
            }
        }).extend([
            new ol.control.ScaleLine() // Add scale line to the defaults controls
        ]),
        target: 'topMap',
        view: new ol.View({
            projection: 'EPSG:4326',
            center: ol.proj.fromLonLat([0, 0]),
            zoom: 2
        })
    });

    currentZoom = Math.floor(theMap.getView().getZoom());

    theMap.getView().on('change:resolution', onZoom);
    theMap.on('moveend', afterMapMove);
    
    var sMapState = $('#ces-map-state').text();
    if (sMapState != '') {
        var mapState = JSON.parse(sMapState);
        if (mapState.center) {
            theMap.getView().setCenter(mapState.center);
            theMap.getView().setZoom(mapState.zoom);
        }
    }
    
    $('.form-check-input-styled').uniform();

    $.ajax({
        url: uiHome + '/queries/getGeoFields',
        type: 'GET'
    }).done(initLayers);
    $('#navbar-save-map').click(function() { saveMapState(); });
});

