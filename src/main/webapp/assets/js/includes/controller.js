/* ------------------------------------------------------------------------------
 *
 *  # Mission Controller JS
 *
 *  Long Live the Fighters
 *
 * ---------------------------------------------------------------------------- */


$(function() {

    var checkBoxColors = {
        '.form-check-input-styled-primary': 'border-primary text-primary',
        '.form-check-input-styled-danger':  'border-danger text-danger',
        '.form-check-input-styled-success': 'border-success text-success',
        '.form-check-input-styled-warning': 'border-warning text-warning',
        '.form-check-input-styled-info':    'border-info text-info',
        '.form-check-input-styled-custom':  'border-indigo-400 text-indigo-400'
    };

    var clusterSelected = [];
    var availSelected   = [];
    var missionSelected = [];
    var pageBusy        = false;
    var pollInterval    = 30000;
    var uiHome          = $('#cluster-ui-home').text();
    var widgetMode      = $('#cluster-widget-mode').text();
    var cookieName      = widgetMode + '-cluster-selected-items';

    function generalError(reason) {
        pageBusy = true;
        swal({
            title: "Oops...",
            text: reason,
            icon: "error",
            className: "swal-size-sm"
        }).then((value) => { pageBusy = false;});
    }

    function updateUniform(el) {
        if (el == null) {
            $('.form-input-styled').uniform();
            $('.form-check-input-styled').uniform();
            $.each(checkBoxColors, function(key, val) {
                $(key).uniform({wrapperClass: val});
            });
        } else {
            el.find('.form-check-input-styled').uniform();
            $.each(checkBoxColors, function(key, val) {
                el.find(key).uniform({wrapperClass: val});
            });
        }
    }

    function getSelectedOptions() {

        var opts = [];

        $('.mission-controller-widget').find('.ces-option').each(function() {
            var $cb = $(this);
            if ($cb.prop('checked')) {
                opts.push($cb.attr('data-id'));
            }
        });

        return opts;
    }

    function getSelectedClusters() {

        var opts = [];

        $('.mission-controller-widget').find('.ces-option').each(function() {
            var $cb = $(this);
            if ($cb.prop('checked')) {
                var $fc = $cb.closest('div.form-check');
                var rs = $fc.find('.missions-running').text();
                var is = $fc.find('.missions-in-progress').text();
                try {
                    var ra = JSON.parse(rs);
                    opts = opts.concat(ra);
                } catch (e) {
                }
                try {
                    var ra = JSON.parse(is);
                    opts = opts.concat(ra);
                } catch (e) {
                }
            }
        });

        return opts;

    }

    function setSelectedOptions(opts) {
        $.each(opts, function(idx, val) {
            $('.mission-controller-widget').find('.ces-option').each(function() {
                var $cb = $(this);
                var id = $cb.attr('data-id');
                if (id == val) {
                    $cb.prop('checked', true).uniform('refresh');
                }
            });
        });
    }

    function reloadPage() {
        if (pageBusy) {
            return;
        }
        var opts = getSelectedOptions();
        if (opts.length > 0) {
            Cookies.set(cookieName, JSON.stringify(opts));
        }
        window.location.reload();
    }

    function actionReturn(data) {

        if (data.status == 'success') {
            $('.mission-controller-widget').find('.ces-option').each(function() {
                $(this).prop('checked', false);
            })
            window.location.reload();
        } else {
            generalError(data.msg);
        }
    }

    function doPickCluster() {
        $('#cluster-picker').modal('show');
    }

    function doJoinCluster() {
        var opts = getSelectedOptions();
        if (opts.length < 1) {
            generalError('Nothing selected');
            return;
        }
        $('.nodes-cluster-name').val('');
        $('#cluster-picker').modal('show');
    }

    function clusteredChangeCluster() {
        var clName = $(this).text();
        var safeName = encodeURIComponent(clName);
        var myURL = uiHome + '/controller/clustered?cluster=' + safeName;
        myURL += "&pollInterval=" + pollInterval;
        window.location.href =  myURL;
    }

    function clusteredEvictNodes() {
        var items = getSelectedOptions();
        if (items.length < 1) {
            generalError('Nothing selected');
            return;
        }
        var clName = $('#cluster-current').text();
        $.ajax({
            url: uiHome + '/actions/cluster',
            type: 'POST',
            data: {
                action: 'evictnodes',
                cluster: clName,
                items: JSON.stringify(items)
            }
        }).done(actionReturn);

    }

    function nodesPickCluster() {
        var clName = $(this).text();
        var items  = getSelectedOptions();
        $.ajax({
            url: uiHome + '/actions/cluster',
            type: 'POST',
            data: {
                action: 'joincluster',
                cluster: clName,
                items: JSON.stringify(items)
            }
        }).done(actionReturn);
    }

    function nodesNamedCluster() {
        var clName = $('.nodes-cluster-name').val();
        if (clName === '') {
            generalError('Please enter a name');
            return;
        }
        var items  = getSelectedOptions();
        $.ajax({
            url: uiHome + '/actions/cluster',
            type: 'POST',
            data: {
                action: 'joincluster',
                cluster: clName,
                items: JSON.stringify(items)
            }
        }).done(actionReturn);

    }

    function deployMission() {
        var items = getSelectedOptions();
        if (items.length == 0) {
            generalError('Nothing selected');
            return;
        } else if (items.length > 1) {
            generalError('Please select one at a time');
            return;
        }
        $('.missions-cluster-name').val('');
        $('#cluster-picker').modal('show');
    }

    function terminateMission() {
        var items = getSelectedOptions();
        if (items.length == 0) {
            generalError('Nothing selected');
            return;
        } else if (items.length > 1) {
            generalError('Please select one at a time');
            return;
        }

        var clusters = getSelectedClusters();
        if (clusters.length < 1) {
            generalError('Mission is not running');
            return;
        }
        if (clusters.length == 1) {
            $.ajax({
                url: uiHome + '/actions/cluster',
                type: 'POST',
                data: {
                    action: 'terminatemission',
                    cluster: clusters[0],
                    items: JSON.stringify(items)
                }
            }).done(actionReturn);
        }

        $('#existing-cluster-container').empty();
        var $a = $('#existing-cluster-template').find('a');
        $.each(clusters, function(idx, cluster) {
            var $item = $a.clone();
            $item.text(cluster);
            $('#existing-cluster-container').append($item);
        });

        $('#existing-cluster-picker').modal('show');
    }

    function terminatePicked() {
            var clName = $(this).text();
            var items  = getSelectedOptions();
            $.ajax({
            url: uiHome + '/actions/cluster',
            type: 'POST',
            data: {
                action: 'terminatemission',
                cluster: clName,
                items: JSON.stringify(items)
            }
        }).done(actionReturn);
        }

    function deployPicked() {
        var clName = $(this).text();
        var items = getSelectedOptions();
        $.ajax({
            url: uiHome + '/actions/cluster',
            type: 'POST',
            data: {
                action: 'deploymission',
                cluster: clName,
                items: JSON.stringify(items)
            }
        }).done(actionReturn);
    }

    function deployNamed() {
        var clName = $('.missions-cluster-name').val();
        if (clName === '') {
            generalError('Please enter a name');
            return;
        }
        var items = getSelectedOptions();
        $.ajax({
            url: uiHome + '/actions/cluster',
            type: 'POST',
            data: {
                action: 'deploymission',
                cluster: clName,
                items: JSON.stringify(items)
            }
        }).done(actionReturn);
    }

    function setPollInterval() {

        var pi = 0;
        $('#controller-config-refresh').find('input:radio').each(function(idx, el) {
            var $rb = $(this);
            if ($rb.prop('checked')) {
                pi = $rb.val();
                return false;
            }
        });

        var myURL = $('#controller-base-url').text();
        myURL += "?pollInterval=" + pi;

        if (widgetMode === 'clustered') {
            var clName = $('#cluster-current').text();
            myURL += "&cluster=" + encodeURIComponent(clName);
        }

        location.href = myURL;

    }

    try {
        pollInterval = parseInt($('#cluster-poll-interval').text());
    } catch (err) {
    }

    $('#controller-config-refresh').find('input:radio').each(function(idx, el) {
        var $rb = $(this);
        var pi = $rb.val();
        if (pi == pollInterval) {
            $rb.prop('checked', true);
        } else {
            $rb.prop('checked', false);
        }
    });

    $('.ces-option').prop('checked', false);
    $('.join-cluster').click(doJoinCluster);
    $('.pick-cluster').click(doPickCluster);
    $('.clustered-change-cluster').click(clusteredChangeCluster);
    $('.clustered-evict').click(clusteredEvictNodes);
    $('.nodes-pick-cluster').click(nodesPickCluster);
    $('.nodes-named-cluster').click(nodesNamedCluster);
    $('.missions-deploy').click(deployMission);
    $('.missions-terminate').click(terminateMission);
    $('.missions-pick-cluster').click(deployPicked);
    $('.missions-named-cluster').click(deployNamed);
    $('.controller-config').click(function() { $('#controller-config-refresh').modal('show');});
    $('.analytics-modal').click(function() { $('#analytics-info').modal('show');});
    $('.platform-modal').click(function() { $('#platform-add').modal('show');});
    $('.asset-modal').click(function() { $('#asset-add').modal('show');});
    $('.analytic-add-modal').click(function() { $('#analytic-add').modal('show');});
    $('.scratch-modal').click(function() { $('#start-from-scratch').modal('show');});
    $('.info-getaway').click(function() { $('#Watchlist-Item').modal('show');});
    $('.add-watchlist-item').click(function() { $('#Watchlist-Generator').modal('show');});
    $('.watchlist-btn').click(function() { $('.getawayCar').button('show');});
    $('.controller-reload').click(reloadPage);
    $('.controller-set-interval').click(setPollInterval);

    $('#existing-cluster-picker').on('click', '.missions-pick-terminate', terminatePicked);

    $('#content').on('shown.bs.modal', function() { pageBusy = true;});
    $('#content').on('hidden.bs.modal', function() { pageBusy = false;});

    updateUniform();

    var selItemsText = Cookies.get(cookieName);
    if (selItemsText) {
        var selItems = JSON.parse(selItemsText);
        setSelectedOptions(selItems);
        Cookies.remove(cookieName);
    }

    if (pollInterval > 0) {
        setInterval(reloadPage, pollInterval);
    }

    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function() {
        /* Toggle between adding and removing the "active" class,
        to highlight the button that controls the panel */
        this.classList.toggle("active");

        /* Toggle between hiding and showing the active panel */
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
          panel.style.display = "none";
        } else {
          panel.style.display = "block";
        }
      });
    }

});
