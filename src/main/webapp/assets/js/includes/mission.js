$(function() {
	let uiHome = $('#composable-ui-home').text();

	function saveSelectedMission(missionId) {

		$.ajax({
				url: uiHome + '/composable/actions/saveSelectedMission',
				type: 'POST',
				data: {
						missionId
				}
		
		}).done(function (data) {
			if (!data.status == 'success') {
					swal({
						title: 'Save Selected Mission Failed - ' + data.msg
					});
			} else {
				getMissionDescription();
				getMissionMapURLs();
				getScenario();
			}
		});
	}

	$('#mission-select').on('change', function() {
		let missionId = this.value;
		saveSelectedMission(missionId);
	});

	function getMissionDescription() {
		$.ajax({
			url: uiHome + '/composable/actions/saveSelectedMission',
			type: 'GET'
		}).done(function (data) {
			if (!data.status == 'success') {
				swal({
					title: 'Get Save Selected Mission Failed - ' + data.msg
				});
			} else {
				$('.mission-markdown').html(data);
			}
		});
	}

	function getMissionMapURLs() {
		$.ajax({
			url: uiHome + '/composable/actions/getMissionMapURLs',
			type: 'GET'
		}).done(function (data) {
			if (!data.status == 'success') {
				swal({
					title: 'GetMissionMapURLs Failed - ' + data.msg
				});
			} else {
				$('#mission-map').attr('src', `http://localhost:1234?${data}` );
			}
		});	
	}
	
	function getScenario() {
		$.ajax({
			url: uiHome + '/composable/actions/getScenario',
			type: 'GET'
		}).done(function (data) {
			if (!data.status == 'success') {
				swal({
					title: 'getScenario Failed - ' + data.msg
				});
			} else {
				$('.launch-mission-button').attr('data-scenario', data );
			}
		});
	}

	function launchMission() {
		let scenarioURL = $(this).attr('data-scenario');

		$.ajax({
			url: scenarioURL,
			type: 'POST'
		}).done(data => {
			if (data.status == 'success') {
				swal({
					title: 'Scenario Started' + data.msg
				});
			} else {
				swal({
					title: 'getScenario Failed - ' + data.msg
				});
			}
		});
	}

	$('.launch-mission-button').on('click', launchMission)
});