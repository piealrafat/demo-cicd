$(function() {

    var uiHome      = $('#composable-ui-home').text();
    var tlColors    = [ 'bg-info-400', 'bg-success-400', 'bg-danger' ];
    var theTimeline = null;
    
    var options = {
        stack: true,
        horizontalScroll: true,
        verticalScroll: true,
        zoomKey: 'ctrlKey',
        maxHeight: 400,
        editable: false,
        margin: {
            item: 10, // minimal margin between items
            axis: 5   // minimal margin between items and the axis
        },
        orientation: 'top',
        zoomMin : 10 
    };
    
    function initTimeline(data) {
        if (data.status === 'success') {
        
            var grps    = new vis.DataSet();
            var items   = new vis.DataSet();
            var nColors = tlColors.length;
        
            $.each(data.groups, function(idx, grp) {
                grps.add(grp);
                $.each(grp.counts, function(ix, count) {
                    var dt = new Date(count.time);
                    var txt = ' Events';
                    if (count.count == 1) {
                        txt = ' Event';
                    }
                    items.add({
                        group: grp.id,
                        start: dt,
                        content: count.count + txt,
                        count: count.count,
                        time: count.time,
                        mode: count.mode,
                        className: tlColors[idx % nColors]
                    });
                });
            });
            
            var tlDiv = $('#visualization').get(0);
            theTimeline = new vis.Timeline(tlDiv);
            theTimeline.setOptions(options);
            theTimeline.setGroups(grps);
            theTimeline.setItems(items);
            theTimeline.on('click', function(properties){
                if(properties.what == 'item'){
                            //alert('event' + properties.what + " " + (properties.what == 'item'?true:false));
                            //theTimeline.zoomMin(90000000);
                            //alert('' + grps.length);
                    /*if(theTimeline.timeAxis.step.scale == 'year'){
                        theTimeline.setOptions(optionsZoomMonth);
                    } else if (theTimeline.timeAxis.step.scale == 'month'){
                        theTimeline.setOptions(optionsZoomDay);
                    } else {
                        theTimeline.setOptions(optionsZoomHour);
                    }*/
                    var itm = items.get(properties.item);
                    var grp = grps.get(itm.group);
                            /*var itm2time;
                            var tempbool = false;
                            $.each(grp.counts, function(ix, count){
                                if(tempbool){
                                    itm2time = count.time;
                                    tempbool = false;
                                } else {
                                    tempbool = (count.time == itm.time?true:false);
                                }
                            });*/
                            //var itm2time = (itm2 == null?null:itm2.time);
                    /*var zoommodein = (itm.mode == "year"?"month":(itm.mode == "month"?"day":"hour"));
                    theTimeline.focus(properties.item);
                    setTimeout(function(){ theTimeline.setOptions(options); }, 610); //timing issue when double clicked, might go away with context popup.
                    var grpid = 0;
                    for(var inc = 0; inc < data.groups.length; inc++){
                        var tempgrp = data.groups[inc].id;
                        if(grp.id == tempgrp.id)grpid = inc;
                        //alert(tempgrp);
                    }
                    theTimeline.off('click');
                    */
                    contextMenu(theTimeline, itm, data, grp);
                    /*setTimeout(function(){
                        $.ajax({
                            url: uiHome + '/queries/getModeTimeData', //find this in web XML!
                            type: 'GET',
                            data: {
                                datestart: itm.time,
                                zoomlevel: zoommodein,
                                groupas: grpid
                            }
                        }).done(zoomTImeline);
                    },610);
                    */
                            //var k =[];
                            //for(var v = 0; v < grps.length; v++)k.push(grps[v]); //grps.get(items.get(properties.item).group).content//only gets a list of numbers, not ids or anything, same for each group
                            //alert(JSON.stringify(itm.start));//theTimeline.timeAxis.step.scale + " " + theTimeline.timeAxis.step.scale); //
                            //alert(JSON.stringify(itm2time));//theTimeline.timeAxis.step.scale + " " + theTimeline.timeAxis.step.scale); //
                            //alert(JSON.stringify(grp));//theTimeline.timeAxis.step.scale + " " + theTimeline.timeAxis.step.scale); //
                
                } else {
                }
            });
        }
    }
    
    function zoomTImeline(data){
        if (data.status === 'success') {
        
            var grps    = new vis.DataSet();
            var items   = new vis.DataSet();
            var nColors = tlColors.length;
        
            $.each(data.groups, function(idx, grp) {
                grps.add(grp);
                $.each(grp.counts, function(ix, count) {
                    var dt = new Date(count.time);
                    var txt = ' Events';
                    if (count.count == 1) {
                        txt = ' Event';
                    }
                    items.add({
                        group: grp.id,
                        start: dt,
                        content: count.count + txt,
                        count: count.count,
                        time: count.time,
                        mode: count.mode,
                        className: tlColors[idx % nColors]
                    });
                });
            });
            //alert(data.msg2);
            //theTimeline.setOptions(options);
            //theTimeline.setGroups(grps);
            //theTimeline.setItems(null);
            theTimeline.setItems(items);
            theTimeline.on('click', function(properties){
                if(properties.what == 'item'){
                    var itm = items.get(properties.item);
                    var grp = grps.get(itm.group);
                    contextMenu(theTimeline, itm, data, grp);
                } else {
                }
            });
        } else {
            alert(data.status);
        }
    }
    
    function contextMenu(theTimeline, itm, data, grp){
        swal({
            buttons:{
                ZoomIn: {
                    text:"Zoom In",
                    value:"zoomin",
                },
                ZoomOut: {
                    text:"Zoom Out",
                    value:"zoomout",
                }
            }
        }).then((value) => {
            switch(value){
                case "zoomin":
                    zoomIn(theTimeline,itm,data,grp);
                    break;
                case "zoomout":
                    zoomOut(theTimeline,itm,data,grp);
                    break;
            }
        });
    }
    
    function zoomIn(theTimeline, itm, data, grp){
        if(theTimeline.timeAxis.step.scale == 'year'){
            theTimeline.setOptions(optionsZoomMonth);
        } else if (theTimeline.timeAxis.step.scale == 'month'){
            theTimeline.setOptions(optionsZoomDay);
        } else {
            theTimeline.setOptions(optionsZoomHour);
        }
        var zoommodein = (itm.mode == "year"?"month":(itm.mode == "month"?"day":"hour"));
        theTimeline.focus(itm.id);
        setTimeout(function(){ theTimeline.setOptions(options); }, 610); //timing issue when double clicked, might go away with context popup.
        var grpid = 0;
        for(var inc = 0; inc < data.groups.length; inc++){
            var tempgrp = data.groups[inc].id;
            if(grp.id == tempgrp.id)grpid = inc;
        }
        theTimeline.off('click');
        setTimeout(function(){
            $.ajax({
                url: uiHome + '/queries/getModeTimeData', //find this in web XML!
                type: 'GET',
                data: {
                    datestart: itm.time,
                    zoomlevel: zoommodein,
                    groupas: grpid
                }
            }).done(zoomTImeline);
        },610);
        
    }
    
    function zoomOut(theTimeline, itm, data, grp){
        if(theTimeline.timeAxis.step.scale == 'hour'){
            theTimeline.setOptions(optionsZoomDay);
        } else if (theTimeline.timeAxis.step.scale == 'day'){
            theTimeline.setOptions(optionsZoomMonth);
        } else {
            theTimeline.setOptions(optionsZoomYear);
        }
        var zoommodein = (itm.mode == "hour"?"day":(itm.mode == "day"?"month":"year"));
        theTimeline.focus(itm.id);
        setTimeout(function(){ theTimeline.setOptions(options); }, 610); //timing issue when double clicked, might go away with context popup.
        var grpid = 0;
        for(var inc = 0; inc < data.groups.length; inc++){
            var tempgrp = data.groups[inc].id;
            if(grp.id == tempgrp.id)grpid = inc;
        }
        theTimeline.off('click');
        setTimeout(function(){
            $.ajax({
                url: uiHome + '/queries/getModeTimeData', //find this in web XML!
                type: 'GET',
                data: {
                    datestart: itm.time,
                    zoomlevel: zoommodein,
                    groupas: grpid
                }
            }).done(zoomTImeline);
        },610);
        
    }
    
    var optionsZoomHour = {
        stack: true,
        horizontalScroll: true,
        verticalScroll: true,
        zoomKey: 'ctrlKey',
        maxHeight: 400,
        editable: false,
        margin: {
            item: 10, // minimal margin between items
            axis: 5   // minimal margin between items and the axis
        },
        orientation: 'top',
        zoomMin : 90000000 
    };
    
    var optionsZoomDay = {
        stack: true,
        horizontalScroll: true,
        verticalScroll: true,
        zoomKey: 'ctrlKey',
        maxHeight: 400,
        editable: false,
        margin: {
            item: 10, // minimal margin between items
            axis: 5   // minimal margin between items and the axis
        },
        orientation: 'top',
        zoomMin : 2160000000 
    };
    
    var optionsZoomMonth = {
        stack: true,
        horizontalScroll: true,
        verticalScroll: true,
        zoomKey: 'ctrlKey',
        maxHeight: 400,
        editable: false,
        margin: {
            item: 10, // minimal margin between items
            axis: 5   // minimal margin between items and the axis
        },
        orientation: 'top',
        zoomMin : 64800000000 
    };
    
    var optionsZoomYear = {
        stack: true,
        horizontalScroll: true,
        verticalScroll: true,
        zoomKey: 'ctrlKey',
        maxHeight: 400,
        editable: false,
        margin: {
            item: 10, // minimal margin between items
            axis: 5   // minimal margin between items and the axis
        },
        orientation: 'top',
        zoomMin : 777600000000 
    };
    
    $('.form-control-multiselect').multiselect();
    
    $.ajax({
        url: uiHome + '/queries/getInitialTime', //find this in web XML!
        type: 'GET'
    }).done(initTimeline);
    
    

});
