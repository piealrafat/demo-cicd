/* ------------------------------------------------------------------------------
 *
 *  # Grid Functions
 */
$(function() {

    var uiHome = $('#grid-ui-home').text();

    // Initialize split.js
    var split = Split(['#topData', '#bottomData'], {
        direction: 'vertical',
        sizes: [80, 20]
    });

    function getActiveWidgets() {
        var myWidgets = [];

        $('#topData').find('div.card').each(function(idx, el) {
            var $card = $(el);
            var obj   = {};
            obj.title = $card.find('h6.card-title').text();
            obj.link  = $card.find('iframe').attr('src');

            myWidgets.push(obj);
        });

        return myWidgets;
    }

    function saveWidgetReturn(data) {
        if (data.status == 'success') {
            swal({
                title: 'Widget Layout Saved',
                icon: 'success'
            });
        } else {
            swal({
                title: 'Save Layout Failed - ' + data.msg
            });
        }
    }

    function saveWidgets(localSession) {

        var myWidgets = getActiveWidgets();

        var isSession  = false;
        var returnFunc = saveWidgetReturn;

        if (localSession) {
            isSession = true;
            returnFunc = function() { }
        }

        $.ajax({
            url: uiHome + '/actions/saveWidgets',
            type: 'POST',
            data: {
                widgets: JSON.stringify(myWidgets),
                session: isSession
            }
        }).done(returnFunc);
    }

    function addWidget() {
        var btn = $(this);
        var title = btn.attr('data-title');
        var myUrl = btn.attr('data-url');
        var builtin = btn.attr('data-builtin');

        if (builtin === 'true') {
            myUrl = uiHome + myUrl;
        }
        var card = $('#grid-widget-template').find('li.grid-list-item-template').clone();
        card.find('.grid-item-title').text(title);

        var frame = '<iframe src="' + myUrl + '" height="100%" width="100%" class="border-0"></iframe>';
        card.find('.card-body-div').html(frame);

        $('#topData').find('ul.row').append(card);

        saveWidgets(true);
    }

    function doFilterWidgets(evt) {
        var filterText = $('#grid-filter-widgets').val();
        if (filterText.length > 1) {
            filterText = filterText.toLowerCase();
            $('.sidebar-content').find('li.sidebar-widget-li').each(function() {
                var $li = $(this);
                var widgetName = $li.find('span.media-title').text();
                if (widgetName.toLowerCase().includes(filterText)) {
                    $li.show();
                } else {
                    $li.hide();
                }
            });
        } else {
            $('.sidebar-content').find('li.sidebar-widget-li').show();
        }
    }

    function checkFullScreen() {
        $('#main').find('li.grid-list-item-template').each(function(idx, el) {
            var $card = $(this).find('div.card');
            if ($card.hasClass('fixed-top')) {
                var bodyHeight = Math.round($card.height()) - Math.round($card.find('.card-header').height());
                $card.find('.card-body-div').height(bodyHeight - 70);
            }
        });
    }

    $('#topData').on('click', 'a.list-icons-item', function(e) {

        e.preventDefault();

        var $a     = $(this);
        var $li    = $a.parents('li');
        var $card  = $a.parents('div.card');
        var action = $a.attr('data-action');

        if (action === 'remove') {
            $li.hide('slow', function() { $li.remove(); saveWidgets(true); });
        } else if (action === 'collapse') {
            $card.toggleClass('card-collapsed');
            $a.toggleClass('rotate-180');
            $card.children('.card-header').nextAll().slideToggle(150);
        } else if (action === 'fullscreen') {
            $card.toggleClass('fixed-top h-100 rounded-0');
            if (!$card.hasClass('fixed-top')) {
                $li.removeAttr('data-fullscreen');
                $card.children('.collapsed-in-fullscreen').removeClass('show');
                $('body').removeClass('overflow-hidden');
                $li.siblings('[data-action=move], [data-action=remove], [data-action=collapse]').removeClass('d-none');
                $card.find('.card-body-div').height(315);
            } else {
                $li.attr('data-fullscreen', 'active');
                $card.removeAttr('style').children('.collapse:not(.show)').addClass('show collapsed-in-fullscreen');
                $('body').addClass('overflow-hidden');
                $li.siblings('[data-action=move], [data-action=remove], [data-action=collapse]').addClass('d-none');
                var bodyHeight = Math.round($card.height()) - Math.round($card.find('.card-header').height());
                $card.find('.card-body-div').height(bodyHeight - 70);
            }
        }
    });

    $('.UI-Watcher').click(function() { $('#controller-config-refresh').modal('show');});

    $(window).resize(checkFullScreen);

    $('#grid-filter-widgets').val('');

    $('.grid-add-widget').click(addWidget);
    $('#navbar-save-widgets').click(function() { saveWidgets(); });
    $('#grid-filter-widgets').keyup(doFilterWidgets);

    // var options = { "playbackRates": [0.5, 1, 1.5, 2], "loopbutton": true  };

    //   videojs('my_video_1', options, function () {
    //     // Player (this) is initialized and ready.
    //   });

});
