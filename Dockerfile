FROM docker.io/bitnami/tomcat:9.0.45

LABEL maintainer=”rrahman@nndata.com”

ADD target/*.war /usr/local/tomcat/app/

EXPOSE 8080

CMD [“catalina.sh”, “run”]

